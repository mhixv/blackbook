<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        $tahun_ajaran_id = DB::table('pengaturan')->value('tahun_aktif');
        $tahun_ajaran_aktif = DB::table('tahun_ajaran')->where('id',$tahun_ajaran_id)->value('nama_tahun');
        view()->share('tahun_ajaran_id', $tahun_ajaran_id);
        view()->share('tahun_ajaran_aktif', $tahun_ajaran_aktif);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
