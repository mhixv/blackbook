<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'guru';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function siswa_asuh()
  {
    return $this->hasMany('App\Siswa','guru_asuh_id','id');
  }

}
