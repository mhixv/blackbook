<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'kelas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function siswa()
  {
    return $this->hasMany('App\Siswa','kelas_id','id');
  }
}
