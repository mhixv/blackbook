<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'tahun_ajaran';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];
}
