<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = $request->user();

        if (Auth::guard($guard)->check()) {
            if ($user->isAdmin())
      			{
      				return redirect('/admin');
      			}

            if ($user->isOperator())
      			{
      				return redirect('/operator');
      			}

            return redirect('/');
            // return new RedirectResponse(url('/'));
        }

        return $next($request);
    }
}
