<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sangsi;
use App\Http\Requests\SangsiRequest;

class SangsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $row= 1;
        $sangsi = Sangsi::all();
        return view('sangsi.index', compact('sangsi','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sangsi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SangsiRequest $request)
    {
        Sangsi::create($request->all());

        return redirect('admin/sangsi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sangsi = Sangsi::findOrFail($id);
        // dd($dep);
        return view('sangsi.edit', compact('sangsi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SangsiRequest $request, $id)
    {
        $sangsi = Sangsi::findOrFail($id);
        $sangsi->update($request->all());
        return redirect('admin/sangsi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sangsi::destroy($id);

        return redirect('admin/sangsi');
    }
}
