<?php namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Auth;
use Hash;

class ChangePasswordsController extends Controller {

  public function __construct()
  {
    // $this->middleware('admin_pusat');
  }

	public function edit() {
        return view('admin/changepassword');
  }

  public function update(Request $request){
    $validator = \Validator::make($request->all(),array(
			'Password_Lama'	=> 'required',
      'password' 		=> 'required|min:6|confirmed'
			// 'Konfirmasi_Password'=> 'required|confirmed'
		));

  	if($validator->fails()) {
  		return redirect()->back()->withErrors($validator);
  	} else {
      // Grab the current user
  		$user = User::find(Auth::user()->id);

  		// Get passwords from the user's input
  		$old_password 	= $request->Password_Lama;
  		$password 		= $request->password;

  		// test input password against the existing one
  		if(Hash::check($old_password, $user->getAuthPassword())){
  			$user->password = Hash::make($password);

  			// save the new password
  			if($user->save()) {
  				return redirect('admin/pengguna')
  						->with('pesan', [
          			"level"=>"success",
          			"message"=>"Password ". $user->nama_pengguna." berhasil diganti."
          		]);
  			}
  		} else {
  			return redirect()->back()
  				->with('pesan', [
      			"level"=>"danger",
      			"message"=>"Password saat ini tidak sesuai."
      		]);
  		}
  	}

  }

  public function edit2($id) {
    $pengguna = User::findOrFail($id);
    return view('changepassworduser', compact('pengguna'));
  }

  public function update2(Request $request, $id){
    $validator = \Validator::make($request->all(),array(
      'password' 		=> 'required|confirmed'
			// 'password_confirmation'=> 'required'
		));

  	if($validator->fails()) {
  		return redirect()->back()->withErrors($validator);
  	} else {
        $user = User::findOrFail($id);

        $password = $request->password;

  			$user->password = Hash::make($password);

  			// save the new password
  			if($user->save()) {
  				return redirect('/admin/pengguna')
  						->with('pesan', [
          			"level"=>"success",
          			"message"=>"Password $user->nama_pengguna berhasil diganti."
          		]);

  		} else {
  			return redirect()->back()
  				->with('pesan',  [
      			"level"=>"danger",
      			"message"=>"Password saat ini tidak sesuai."
      		]);
  		}
  	}

  }
}
