<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aturan;
use App\Http\Requests\AturanRequest;

class AturanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $row= 1;
        $aturan = Aturan::all();
        return view('aturan.index', compact('aturan','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aturan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AturanRequest $request)
    {
        Aturan::create($request->all());

        return redirect('admin/aturan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aturan = Aturan::findOrFail($id);
        // dd($dep);
        return view('aturan.edit', compact('aturan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AturanRequest $request, $id)
    {
        $aturan = Aturan::findOrFail($id);
        $aturan->update($request->all());
        return redirect('admin/aturan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Aturan::destroy($id);

        return redirect('admin/aturan');
    }
}
