<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;use App\TahunAnggaran;
use App\Siswa;use App\InformasiUmum;
use App\Guru;use DB;
use App\Kelas;use PDF;
use App\Kamar;
use App\Pengaturan;
use App\TahunAjaran;
use Carbon\Carbon;
use App\Pelanggaran;
use App\Hukuman;

class AdminController extends Controller
{

    // private $tahun_anggaran;
    private $semester_aktif;

    public function __construct()
    {
        // $this->middleware('admin');
        $this->tahun_aktif = DB::table('pengaturan')->value('tahun_aktif');
        // $this->tahun_anggaran = DB::table('m_tahun_anggaran')->where('id',$this->tahun_anggaran_id)->value('tahun_anggaran');
        // dd($tahun_anggaran);
    }

    public function index()
    {
        // jumlah pelanggaran yang dilakukan siswa
        $jml_pel_siswa = Pelanggaran::where('tahun_ajaran_id', $this->tahun_aktif)->count();

        // jumlah siswa yang melakukan pelanggaran
        $jml_siswa_pel = Pelanggaran::where('tahun_ajaran_id', $this->tahun_aktif)
                        ->distinct('siswa_id')->count('siswa_id');

        // jumlah aturan
        $jml_aturan = DB::table('aturan')->count();

        // jumlah hukuman
        $jml_hukuman = Hukuman::where('tahun_ajaran_id', $this->tahun_aktif)->count();

        //barchart pelanggaran perbulan
        $pelanggaran_chart = DB::table('pelanggaran')
                            ->select(DB::raw('count(id) as jumlah, MONTH(tanggal) as month, YEAR(tanggal) as year'))
                            ->groupBy(DB::raw('YEAR(tanggal) ASC, MONTH(tanggal) ASC'))
                            ->where('tahun_ajaran_id', $this->tahun_aktif)
                            ->get();

        $jumlah_pelanggaran = [];
        $bulan = [];
        foreach($pelanggaran_chart as $entry) {
            array_push($jumlah_pelanggaran, $entry->jumlah);
            array_push($bulan, $this->bulan($entry->month));
        }
        // dd($barchart_pelanggaran);

        //barchart pelanggaran per tahun
        $pelanggaran_chart_1 = DB::table('pelanggaran')
                            ->select(DB::raw('count(id) as jumlah, YEAR(tanggal) as year'))
                            ->groupBy(DB::raw('YEAR(tanggal) ASC'))
                            ->where('tahun_ajaran_id', $this->tahun_aktif)
                            ->get();

        $jumlah_pelanggaran_tahun = [];
        $tahun = [];
        foreach($pelanggaran_chart_1 as $entry) {
            array_push($jumlah_pelanggaran_tahun, $entry->jumlah);
            array_push($tahun, $entry->year);
        }


        // tabel pelanggaran top ten
        // $top_ten_pelanggar = DB::table('pelanggaran')
        //                       ->select(DB::raw('COUNT(siswa_id) as jumlah'),'siswa.nama_siswa','tanggal')
        //                       ->join('siswa','siswa.id','=','pelanggaran.siswa_id')
        //                       ->where('tahun_ajaran_id', $this->tahun_aktif)
        //                       // ->orderBy('jumlah')
        //                       ->get(10);

        $row=1;
        $top_ten_pelanggar =  DB::select('SELECT siswa.nama_siswa, count(siswa_id) as jumlah FROM pelanggaran
                              JOIN siswa ON siswa.id=pelanggaran.siswa_id 
                              WHERE pelanggaran.tahun_ajaran_id = '.$this->tahun_aktif.'
                              GROUP BY siswa.nama_siswa ORDER BY jumlah DESC LIMIT 10');

                              // dd($top_ten_pelanggar);

        $row1=1;
        $top_ten_aturan =  DB::select('SELECT aturan.nama_aturan, count(aturan_id) as jumlah FROM pelanggaran
                              JOIN aturan ON aturan.id=pelanggaran.aturan_id
                              WHERE pelanggaran.tahun_ajaran_id = '.$this->tahun_aktif.'
                              GROUP BY aturan.nama_aturan ORDER BY jumlah DESC LIMIT 10');


        // $row2=1;
        // $top_ten_nilai =  DB::select('SELECT siswa.nama_siswa, count(aturan_id) as jumlah_pelanggaran ,sum(aturan.poin) as jumlah_poin FROM pelanggaran
        //                       JOIN aturan ON aturan.id=pelanggaran.aturan_id
        //                       JOIN siswa ON siswa.id=pelanggaran.siswa_id
        //                       -- LEFT JOIN hukuman ON hukuman.siswa_id=pelanggaran.siswa_id
        //                       -- WHERE IF(hukuman.tanggal=null,pelanggaran.tanggal=null,pelanggaran.tanggal > hukuman.tanggal)
        //                       GROUP BY siswa.nama_siswa ORDER BY jumlah_poin DESC LIMIT 10');


        // foreach ($top_ten_nilai as $nilai) {
        //   if($nilai->tanggal < )
        // }

        // dd($top_ten_nilai);

        // return view('admin.dashboard', compact('provinsi','counter','counter1','total','total_penerima','tahun','total_k','total_p','total_p_all'));
        return view('admin.dashboard',compact('jml_pel_siswa','jml_siswa_pel','jml_aturan','jml_hukuman','jumlah_pelanggaran','bulan','jumlah_pelanggaran_tahun','tahun','top_ten_pelanggar','row','top_ten_aturan','row1'));
    }

    public function pelanggaran()
    {
        $row1=1;
        $top_ten_nilai =  DB::select('SELECT siswa.id, siswa.nama_siswa, count(aturan_id) as jumlah_pelanggaran ,sum(aturan.poin) as jumlah_poin FROM pelanggaran
                              JOIN aturan ON aturan.id=pelanggaran.aturan_id
                              JOIN siswa ON siswa.id=pelanggaran.siswa_id
                              LEFT JOIN hukuman ON hukuman.siswa_id=pelanggaran.siswa_id
                              -- WHERE IF(hukuman.tanggal=null,pelanggaran.tanggal=null,pelanggaran.tanggal > hukuman.tanggal)
                              WHERE pelanggaran.tahun_ajaran_id = '. $this->tahun_aktif .'
                              GROUP BY siswa.nama_siswa,siswa.id ORDER BY jumlah_poin DESC LIMIT 10');

        $counter = 1;
        $pelanggaran = DB::table('pelanggaran')
                        ->select('aturan.nama_aturan','aturan.poin','siswa.nama_siswa','tanggal')
                        ->leftJoin('aturan','aturan.id','=','pelanggaran.aturan_id')
                        ->leftJoin('siswa','siswa.id','=','pelanggaran.siswa_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->get();

        return view('admin.pelanggaran',compact('pelanggaran','counter','top_ten_nilai','row1'));
    }

    public function hukuman()
    {
        $counter = 1;
        $hukuman = DB::table('hukuman')
                        ->select('sangsi.nama_sangsi','sangsi.kompensasi','siswa.nama_siswa','tanggal')
                        ->leftJoin('sangsi','sangsi.id','=','hukuman.sangsi_id')
                        ->leftJoin('siswa','siswa.id','=','hukuman.siswa_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->get();

        return view('admin.hukuman',compact('hukuman','counter'));
    }

    /*
    *  CONTROLLER TAHUN AKTIF
    *
    */
    public function tahun_aktif()
    {
        $pengaturan = Pengaturan::find(1);
        // dd($konfigurasi);
        $counter = 1;

        $tahunajaran = TahunAjaran::pluck('nama_tahun','id')->toArray();

        return view('admin.tahun_aktif',compact('pengaturan','tahunajaran','counter'));
    }


    function cetak_laporan()
    {
       $siswa = Siswa::pluck('nama_siswa','id')->toArray();

       return view('admin.cetak_laporan',compact('siswa'));
    }


    function cetak_laporan2(Request $request)
    { 
      // dd($request);
      // $id = Input::get('kelas_id');

      // $kelas = Kelas::pluck('nama_kelas','id')->toArray();
      // $guru = Guru::pluck('nama_guru','id')->all();
      $siswa = Siswa::findOrFail($request->siswa_id);
      // $guru_asuh = Guru::where('id',$nama_siswa->guru_asuh_id)->value('nama_guru');
      // $wali_kelas = Guru::where('id',$nama_siswa->kelas_id)

      $row = 1;
      $pelanggaran = DB::table('pelanggaran')
                      ->select('pelanggaran.tanggal','aturan.nama_aturan','aturan.poin','pelanggaran.keterangan')
                      ->leftJoin('aturan','aturan.id','=','pelanggaran.aturan_id')
                      ->leftJoin('siswa','siswa.id','=','pelanggaran.siswa_id')
                      ->where('tahun_ajaran_id', $this->tahun_aktif)
                      ->where('tanggal','>=',$request->tanggal1)
                      ->where('tanggal','<=',$request->tanggal2)
                      ->where('siswa_id',$request->siswa_id)
                      ->get();

      foreach ($pelanggaran as $info) {
          $info->tanggal = $this->tanggal_indo($info->tanggal,true);
      }

      $tanggal = date("Y-m-d");
      $tanggal = $this->tanggal_indo($tanggal,false);

                      // dd($pelanggaran);

        // return view('templatesk',compact('pelanggaran','row','siswa','tanggal'));
      $pdf = PDF::loadView('templatesk', compact('pelanggaran','row','siswa','tanggal'))->setPaper('A4');
      return $pdf->download('Laporan Pelanggaran - '.$siswa->nama_siswa.'.pdf');
    }



    public function tahun_aktif_update(Request $request, $id)
    {
        $pengaturan = Pengaturan::find(1);

        $this->validate($request, [
          'tahun_aktif' => 'required|not_in:0',
        ]);

        $pengaturan->tahun_aktif = $request->tahun_aktif;
        $pengaturan->save();

        \Session::flash("pesan", [
          "level"=>"success",
          "message"=>"Tahun Ajaran Aktif telah berhasil diperbaharui."
        ]);

  			return redirect('admin/tahun_aktif');
    }
    // -------------------------------------------------------------------------


    public function lihat_detil($id){
      $pelanggaran = Pelanggaran::where('siswa_id',$id)->where('tahun_ajaran_id',$this->tahun_aktif)->where('status',0)->orderBy('tanggal','asc')->get();
      $hukuman = Hukuman::where('siswa_id',$id)->where('tahun_ajaran_id',$this->tahun_aktif)->orderBy('tanggal','desc')->get();

      foreach($pelanggaran as $pel){
        $pel->tanggal = $this->tanggal_indo($pel->tanggal,true);
      }

      foreach($hukuman as $pel1){
        $pel1->tanggal = $this->tanggal_indo($pel1->tanggal,true);
      }

      $row =1;
      $row1=1;

      return view('lihat_detil3',compact('pelanggaran','row','hukuman','row1'));
    }

    function tanggal_indo($tanggal, $cetak_hari)
    {
      $hari = array ( 1 => 'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Ahad');

      $bulan = array (1 => 'Jan','Feb','Mar','Apr','Mei','Jun','Jul',
            'Agu','Sep','Okt','Nov','Des');

      // $jam = date('H:i:s', strtotime($tanggal));
      $tanggal = date('Y-m-d', strtotime($tanggal));

      // $split   = explode(' ', $row['tanggal_terbit']);
      // $tanggal = $split[0];
      $split 	  = explode('-', $tanggal);
      $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ]. ' ' . $split[0];

      if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo; //.' '.$jam;
      }
      return $tgl_indo;
    }

    function bulan($bulan){
      Switch ($bulan){
          case 1 : $bulan="Januari";
              Break;
          case 2 : $bulan="Februari";
              Break;
          case 3 : $bulan="Maret";
              Break;
          case 4 : $bulan="April";
              Break;
          case 5 : $bulan="Mei";
              Break;
          case 6 : $bulan="Juni";
              Break;
          case 7 : $bulan="Juli";
              Break;
          case 8 : $bulan="Agustus";
              Break;
          case 9 : $bulan="September";
              Break;
          case 10 : $bulan="Oktober";
              Break;
          case 11 : $bulan="November";
              Break;
          case 12 : $bulan="Desember";
              Break;
          }
        return $bulan;
    }
}
