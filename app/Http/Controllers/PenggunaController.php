<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\PenggunaRequest;
use App\Http\Requests\PenggunaUpdateRequest;
use Auth;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class PenggunaController extends Controller {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pengguna = User::all();
		$counter = 0;

		return view('pengguna.index', compact('pengguna','counter'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('pengguna.create',compact('list'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PenggunaRequest $request)
	{

		User::create([
			'nama' => $request['nama'],
			'email' => $request['email'],
			'password' => bcrypt($request['password']),
			'hak_akses' => $request['hak_akses']
		]);
	    // dd($request->all());

		//\Session::flash('flash_text','Pengguna berhasil dibuat!');
		return redirect('admin/pengguna');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// $list = array();
		$pengguna = User::findOrFail($id);//dd($pengguna);
		return view('pengguna.edit', compact('pengguna','list'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, PenggunaUpdateRequest $request)
	{
		// todo agung: update password is not encripted yet
		// if password is filled then update password else update information only
		// dd(is_null($request['password']));
			$pengguna = User::findOrFail($id);
			$pengguna->update($request->all());
			return redirect('admin/pengguna');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);
		return redirect('admin/pengguna');
	}


	// public function importPengguna()
	// {
	// 	$ada_error = 0;
	// 	return view('admin.import',compact('ada_error'));
	// }
	//
	// public function import(Request $request)
	// {
	// 	// validasi untuk memastikan file yang diupload adalah excel
	// 	$this->validate($request, [ 'excel' => 'required|mimes:xlsx,xls' ]);
	// 	// ambil file yang baru diupload
	// 	$excel = $request->file('excel');
	//
	// 	$excelsPengguna = Excel::selectSheets('pengguna')->load($excel, function($reader) {
	// 	// options, jika ada
	// 	})->get();
	// 	// dd($excelsPesantren);
	//
	// 	// rule untuk validasi setiap row pada file excel
	// 	$rowRulesPengguna = [
	// 		'nama_lengkap' => 'required',
	// 		'nomor_handphone' => 'required',
	// 		'alamat_email' => 'required|email|unique:users',
	// 		'level_akses' => 'required',
	// 		// 'akses_id' => 'required',
	// 		// 'ka_kanwil' => 'required',
	// 	];
	//
	// 	$pengguna_gagal = array();
	// 	// $santri_gagal = array();
	// 	// $tenaga_pengajar_gagal = array();
	//
	// 	// Catat data yang gagal dimasukan
	// 	$gagal = 0;
	// 	// looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
	// 	foreach ($excelsPesngguna as $row) {
	// 		// dd($row);
	// 		// Membuat validasi untuk row di excel
	// 		// Disini kita ubah baris yang sedang di proses menjadi array
	// 		$validator = Validator::make($row->toArray(), $rowRulesPengguna);
	//
	// 		if ($validator->fails()){
	// 			$pengguna_gagal[$gagal] = array_merge($row->toArray(),
	// 															// ['id_pesantren' =>$validator->messages()->first('id_pesantren')],
	// 															// ['error_nspp' => $validator->messages()->first('nspp')],
	// 															['error_email' => $validator->messages()->first('alamat_email')]);
	// 			// $pesantren_id_gagal[$gagal] = $row->id_pesantren;
	// 			$gagal++;
	// 			// dd($validator->messages()->first('nspp'));
	// 			// Skip baris ini jika tidak valid, langsung ke baris selanjutnya
	// 			continue;
	// 		}
	// 		// Syntax dibawah dieksekusi jika baris excel ini valid
	//
	// 		// buat buku baru
	// 		$pengguna = User::create([
	// 			'fullname'=> $row['nama_lengkap'],
	// 			'hp'=> $row['nomor_handphone'],
	// 			'email'=> $row['alamat_email'],
	// 			'password'=> bcrypt('password'),
	// 			'role'=> $row['level_akses'],
	// 			'akses_id'=> $row['hak_akses'],
	// 			'ka_kanwil'=> $row['nama_ka_kanwil'],
	// 			'nik' => $row['nip_ka_kanwil']
	// 		]);
	// 	}
	//
	// 	$level = "success";
	// 	$pesan = "Semua Data Pesantren berhasil di import.";
	// 	$ada_error = 0;
	// 	if($gagal != 0){
	// 			$this->importGagalXLS($pengguna_gagal);
	// 			$ada_error = 1;
	// 			$level = "warning";
	// 			$pesan = "Data Pesantren berhasil di import, dengan Data Pengguna Error sebanyak ".$gagal." data.";
	// 	}
	//
	// 	\Session::flash("pesan", [
	// 		"level"=>$level,
	// 		"message"=>$pesan
	// 	]);
	//
	// 	return redirect('admin/users');
	// }
	//
	// public function generateExcelTemplate()
	// {
	// 	Excel::create('Template Import Pengguna', function($excel) {
	// 		// Set the properties
	// 		$excel->setTitle('Template Import Pengguna')
	// 		->setCreator('Kemenag RI')
	// 		->setCompany('Kemenag RI')
	// 		->setDescription('Template Import Pengguna');
	//
	// 		$excel->sheet('pengguna', function($sheet) {
	// 			$row = 1;
	// 			$sheet->row($row, [
	// 				'nama_lengkap',
	// 				'nomor_handphone',
	// 				'alamat_email',
	// 				'level_akses',
	// 				'hak_akses',
	// 				'nama_ka_kanwil',
	// 				'nip_ka_kanwil'
	// 			]);
	// 		});
	// 	})->export('xls');
	// }
	//
	// public function importGagalXLS($pengguna_gagal)
	// {
	// 	// dd($pesantren_gagal);
	// 	$error = Excel::create('Error Import Data Pengguna', function($excel) use ($pengguna_gagal){
	// 		// Set the properties
	// 		$excel->setTitle('Import Error Data Pengguna')
	// 		->setCreator('Kemenag RI')
	// 		->setCompany('Kemenag RI')
	// 		->setDescription('Error Import Data Pengguna');
	//
	// 		$excel->sheet('pengguna', function($sheet) use ($pengguna_gagal) {
	// 			$row = 1;
	// 			$sheet->row($row, [
	// 				'nama_lengkap',
	// 				'nomor_handphone',
	// 				'alamat_email',
	// 				'level_akses',
	// 				'hak_akses',
	// 				'nama_ka_kanwil',
	// 				'nip_ka_kanwil'
	// 			]);
	// 			foreach ($pengguna_gagal as $gagal) {
	// 				// dd($santri);
	// 					$sheet->row(++$row,[
	// 									$gagal['nama_lengkap'],
	// 									$gagal['nomor_handphone'],
	// 									$gagal['alamat_email'],
	// 									$gagal['level_akses'],
	// 									$gagal['hak_akses'],
	// 									$gagal['nama_ka_kanwil'],
	// 									$gagal['nip_ka_kanwil']
	// 					]);
	// 			}
	// 		});
	// 	})->export('xlsx');//->store('xlsx', false, true);
	//
	// 	// return $error;
	// }
}
