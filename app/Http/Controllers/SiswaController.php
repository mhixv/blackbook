<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Kelas;
use App\Guru;
use App\Http\Requests\SiswaRequest;
use Maatwebsite\Excel\Facades\Excel;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $row= 1;
        $siswa = Siswa::all();
        return view('siswa.index', compact('siswa','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::pluck('nama_kelas','id')->all();
        $guru = Guru::pluck('nama_guru','id')->all();

        return view('siswa.create',compact('kelas','guru'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiswaRequest $request)
    {
        Siswa::create($request->all());

        return redirect('admin/siswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::pluck('nama_kelas','id')->all();
        $guru = Guru::pluck('nama_guru','id')->all();

        $siswa = Siswa::findOrFail($id);
        // dd($dep);
        return view('siswa.edit', compact('siswa','kelas','guru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiswaRequest $request, $id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->update($request->all());
        return redirect('admin/siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Siswa::destroy($id);

        return redirect('admin/siswa');
    }

    public function import()
  	{
  		$ada_error = 0;
  		return view('admin.import',compact('ada_error'));
  	}

    public function importSiswa(Request $request)
  	{
  		// validasi untuk memastikan file yang diupload adalah excel
  		$this->validate($request, [ 'excel' => 'required|mimes:xlsx,xls' ]);
  		// ambil file yang baru diupload
  		$excel = $request->file('excel');

  		$excelsSiswa = Excel::selectSheets('siswa')->load($excel, function($reader) {
  		// options, jika ada
  		})->get();
      // dd($excelsSiswa);
  		// rule untuk validasi setiap row pada file excel
  		// $rowRulesPengguna = [
  		// 	'nama_lengkap' => 'required',
  		// 	'nomor_handphone' => 'required',
  		// 	'alamat_email' => 'required|email|unique:users',
  		// 	'level_akses' => 'required',
  		// ];

  		// $pengguna_gagal = array();

  		// Catat data yang gagal dimasukan
  		$gagal = 0;
  		// looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
  		foreach ($excelsSiswa as $row) {
  			// dd($row);
  			// Membuat validasi untuk row di excel
  			// Disini kita ubah baris yang sedang di proses menjadi array
  			// $validator = Validator::make($row->toArray(), $rowRulesPengguna);

  			// if ($validator->fails()){
  			// 	$pengguna_gagal[$gagal] = array_merge($row->toArray(),
  			// 													// ['id_pesantren' =>$validator->messages()->first('id_pesantren')],
  			// 													// ['error_nspp' => $validator->messages()->first('nspp')],
  			// 													['error_email' => $validator->messages()->first('alamat_email')]);
  			// 	// $pesantren_id_gagal[$gagal] = $row->id_pesantren;
  			// 	$gagal++;
  			// 	// dd($validator->messages()->first('nspp'));
  			// 	// Skip baris ini jika tidak valid, langsung ke baris selanjutnya
  			// 	continue;
  			// }
  			// Syntax dibawah dieksekusi jika baris excel ini valid

  			// insert
        if($row['nama_siswa'] != ''){
    			$pengguna = Siswa::create([
    				'nama_siswa'=> $row['nama_siswa'],
    				'nis'=> $row['nis'],
    				'nisn'=> $row['nisn'],
    				'kelas_id'=> $row['kelas'],
    				'guru_asuh_id'=> $row['guru_asuh'],
            'asrama_kamar'=> $row['asrama_kamar']
    			]);
        }
  		}

  		$level = "success";
  		$pesan = "Semua Data Siswa berhasil di import.";
  		$ada_error = 0;
  		if($gagal != 0){
  				$this->importGagalXLS($pengguna_gagal);
  				$ada_error = 1;
  				$level = "warning";
  				$pesan = "Data Siswa berhasil di import, dengan Data Pengguna Error sebanyak ".$gagal." data.";
  		}

  		\Session::flash("pesan", [
  			"level"=>$level,
  			"message"=>$pesan
  		]);

  		return redirect('admin/siswa');
  	}

    public function download_template() {
      $file_path = public_path('file/templateimportsiswa.xlsx');
      return response()->download($file_path);
    }
}
