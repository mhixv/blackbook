<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\PelanggaranRequest;
use App\Http\Requests\HukumanRequest;

use App\Http\Requests;
use App\TahunAnggaran;
use App\Siswa;use Auth;
use App\Guru;use App\Hukuman;
use DB;use App\Pelanggaran;
use App\Kelas;use Input;
use App\Kamar;
use App\Sangsi;
use App\Aturan;

class OperatorController extends Controller
{

    private $tahun_aktif;

    public function __construct()
    {
        // $this->middleware('admin');
        $this->tahun_aktif = DB::table('pengaturan')->value('tahun_aktif');
        // $this->tahun_anggaran = DB::table('m_tahun_anggaran')->where('id',$this->tahun_anggaran_id)->value('tahun_anggaran');
        // dd($tahun_anggaran);
    }

    public function index()
    {
        // return view('admin.dashboard', compact('provinsi','counter','counter1','total','total_penerima','tahun','total_k','total_p','total_p_all'));
         // jumlah pelanggaran yang dilakukan siswa
        $jml_pel_siswa = Pelanggaran::where('tahun_ajaran_id', $this->tahun_aktif)->count();

        // jumlah siswa yang melakukan pelanggaran
        $jml_siswa_pel = Pelanggaran::where('tahun_ajaran_id', $this->tahun_aktif)
                        ->distinct('siswa_id')->count('siswa_id');

        // jumlah aturan
        $jml_aturan = DB::table('aturan')->count();

        // jumlah hukuman
        $jml_hukuman = Hukuman::where('tahun_ajaran_id', $this->tahun_aktif)->count();

        $row=1;
        $top_ten_pelanggar =  DB::select('SELECT siswa.nama_siswa, count(siswa_id) as jumlah FROM pelanggaran
                              JOIN siswa ON siswa.id=pelanggaran.siswa_id
                              WHERE pelanggaran.tahun_ajaran_id = '.$this->tahun_aktif.'
                              GROUP BY siswa.nama_siswa ORDER BY jumlah DESC LIMIT 5');

                              // dd($top_ten_pelanggar);

        $row1=1;
        $top_ten_aturan =  DB::select('SELECT aturan.nama_aturan, count(aturan_id) as jumlah FROM pelanggaran
                              JOIN aturan ON aturan.id=pelanggaran.aturan_id
                              WHERE pelanggaran.tahun_ajaran_id = '.$this->tahun_aktif.'
                              GROUP BY aturan.nama_aturan ORDER BY jumlah DESC LIMIT 5');

        $row2=1;
        $jml_tot_pel =  DB::select('SELECT  siswa.nama_siswa, pelanggaran.siswa_id, sum(poin) as total, count(aturan_id) as jumlah FROM pelanggaran
                              JOIN aturan ON aturan.id=pelanggaran.aturan_id
                              JOIN siswa ON siswa.id=pelanggaran.siswa_id
                              WHERE pelanggaran.status = 0 AND pelanggaran.tahun_ajaran_id = '. $this->tahun_aktif .'
                              GROUP BY siswa.nama_siswa, pelanggaran.siswa_id
                              HAVING SUM(poin) > 39
                              ORDER BY total DESC');
        // dd($jml_tot_pel);

        return view('operator.dashboard',compact('jml_pel_siswa','jml_siswa_pel','jml_aturan','jml_hukuman','jumlah_pelanggaran','bulan','jumlah_pelanggaran_tahun','tahun','top_ten_pelanggar','row','top_ten_aturan','row1','jml_tot_pel','row2'));
    }

    public function pelanggaran()
    {
      // $guru = Guru::pluck('nama_guru','id')->all();
      $row = 1;
      $pelanggaran = Pelanggaran::where('user_id',Auth::user()->id)->where('tahun_ajaran_id',$this->tahun_aktif)->get();

      foreach ($pelanggaran as $info) {
          $info->tanggal = $this->tanggal_indo($info->tanggal,false);
      }

      return view('operator.pelanggaran_index',compact('pelanggaran','row'));
    }

    public function pelanggaran_create()
    {
      $guru = Guru::pluck('nama_guru','id')->all();
      $pelanggaran = Aturan::pluck('nama_aturan','id')->all();

      // if ($kelas == 0){
        $siswa = Siswa::pluck('nama_siswa','id')->all();
      // }else{
      //   $siswa = Siswa::where('kelas_id',$kelas)->get();
      // }
      return view('operator.pelanggaran_form',compact('siswa','pelanggaran','guru'));
    }

    public function pelanggaran_simpan(PelanggaranRequest $request)
    {
      $request->request->add(['user_id'=> \Auth::user()->id,'tahun_ajaran_id' => $this->tahun_aktif,'status' => 0,'hukuman_id' => 0]);
      $pelanggaran = Pelanggaran::create($request->all());

      return redirect('operator/pelanggaran');
    }

    public function pelanggaran_edit($id)
    {
      $pelanggaran1 = Pelanggaran::findOrFail($id);
      $guru = Guru::pluck('nama_guru','id')->all();
      $pelanggaran = Aturan::pluck('nama_aturan','id')->all();

      // if ($kelas == 0){
      $siswa = Siswa::pluck('nama_siswa','id')->all();
      // }else{
      //   $siswa = Siswa::where('kelas_id',$kelas)->get();
      // }
      return view('operator.pelanggaran_edit',compact('siswa','pelanggaran','pelanggaran1','guru'));
    }

    public function pelanggaran_update(PelanggaranRequest $request,$id)
    {
      $pelanggaran = Pelanggaran::findOrFail($id);
      // $request->request->add(['user_id'=> \Auth::user()->id,'tahun_ajaran_id' => $this->tahun_aktif]);
      $pelanggaran->update($request->all());

      return redirect('operator/pelanggaran');
    }

    public function pelanggaran_delete($id)
    {
        Pelanggaran::destroy($id);

        return redirect('operator/pelanggaran');
    }




    public function hukuman()
    {
      // $guru = Guru::pluck('nama_guru','id')->all();
      $row = 1;
      $hukuman = Hukuman::where('user_id',Auth::user()->id)->where('tahun_ajaran_id',$this->tahun_aktif)->get();

      foreach ($hukuman as $info) {
          $info->tanggal = $this->tanggal_indo($info->tanggal,false);
      }

      return view('operator.hukuman_index',compact('hukuman','row'));
    }

    public function hukuman_create()
    {
      $guru = Guru::pluck('nama_guru','id')->all();
      $hukuman = Sangsi::pluck('nama_sangsi','id')->all();

      // if ($kelas == 0){
        $siswa = Siswa::pluck('nama_siswa','id')->all();
      // }else{
      //   $siswa = Siswa::where('kelas_id',$kelas)->get();
      // }
      return view('operator.hukuman_form',compact('siswa','hukuman','guru'));
    }

    public function hukuman_simpan(HukumanRequest $request)
    {
      $request->request->add(['user_id'=> \Auth::user()->id,'tahun_ajaran_id' => $this->tahun_aktif]);

      DB::transaction(function() use(&$request) {
	      $hukuman = Hukuman::create($request->all());

	      $pelanggaran = Pelanggaran::where('tanggal', '<=', $request->tanggal)->
	      							  where('status', 0)->
	      							  where('siswa_id', $request->siswa_id)->get();

	      // update setiap pelanggaran yang sudah diberi hukuman
	      foreach ($pelanggaran as $kesalahan) {
	      	$kesalahan->status = 1;
	      	$kesalahan->hukuman_id = $hukuman->id;

	      	$kesalahan->save();
	      }
	  });

      return redirect('operator/hukuman');
    }

    public function hukuman_hukum($id)
    {
      $guru = Guru::pluck('nama_guru','id')->all();
      $hukuman = Sangsi::pluck('nama_sangsi','id')->all();

      // if ($kelas == 0){
        $siswa = Siswa::pluck('nama_siswa','id')->all();
      // }else{
      //   $siswa = Siswa::where('kelas_id',$kelas)->get();
      // }
      return view('operator.hukuman_hukum',compact('siswa','hukuman','guru','id'));
    }

    public function hukuman_hukum2(HukumanRequest $request)
    {
      $request->request->add(['user_id'=> \Auth::user()->id,'tahun_ajaran_id' => $this->tahun_aktif]);

      DB::transaction(function() use(&$request) {
        $hukuman = Hukuman::create($request->all());

        $pelanggaran = Pelanggaran::where('tanggal', '<=', $request->tanggal)->
                        where('status', 0)->
                        where('siswa_id', $request->siswa_id)->get();

        // update setiap pelanggaran yang sudah diberi hukuman
        foreach ($pelanggaran as $kesalahan) {
          $kesalahan->status = 1;
          $kesalahan->hukuman_id = $hukuman->id;

          $kesalahan->save();
        }
    });

      return redirect('operator');
    }

    public function hukuman_edit($id)
    {
      $hukuman1 = Hukuman::findOrFail($id);
      $guru = Guru::pluck('nama_guru','id')->all();
      $hukuman = Sangsi::pluck('nama_sangsi','id')->all();

      // if ($kelas == 0){
      $siswa = Siswa::pluck('nama_siswa','id')->all();
      // }else{
      //   $siswa = Siswa::where('kelas_id',$kelas)->get();
      // }
      return view('operator.hukuman_edit',compact('siswa','hukuman','hukuman1','guru'));
    }

    public function hukuman_update(HukumanRequest $request,$id)
    {
      $hukuman = Hukuman::findOrFail($id);
      // $request->request->add(['user_id'=> \Auth::user()->id,'tahun_ajaran_id' => $this->tahun_aktif]);
      $hukuman->update($request->all());

      return redirect('operator/hukuman');
    }

    public function hukuman_delete($id)
    {
    	DB::transaction(function() use (&$id){

    		$pelanggaran = Pelanggaran::where('hukuman_id', $id)->get();

	      // update setiap pelanggaran yang sudah diberi hukuman
	      foreach ($pelanggaran as $kesalahan) {
	      	$kesalahan->status = 0;
	      	$kesalahan->hukuman_id = 0;

	      	$kesalahan->save();
	      }

        	Hukuman::destroy($id);
        });

        return redirect('operator/hukuman');
    }



    function laporan_pelanggaran()
    {
    	// $id = Input::get('kelas_id');

    	$kelas = Kelas::pluck('nama_kelas','id')->toArray();
    	$guru = Guru::pluck('nama_guru','id')->toArray();
    	// $guru = Guru::pluck('nama_guru','id')->all();
      $info_kelas = "Semua Kelas";
      $info_guru =  "Semua Guru Asuh";

    	$row = 1;
      	$pelanggaran = DB::table('pelanggaran')
                        ->select('siswa.nama_siswa','aturan.nama_aturan','aturan.poin','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('aturan','aturan.id','=','pelanggaran.aturan_id')
                        ->leftJoin('siswa','siswa.id','=','pelanggaran.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->get();

      	return view('operator.laporan_pelanggaran',compact('pelanggaran','row','kelas','guru','info_kelas','info_guru'));
    }

    function laporan_pelanggaran2(Request $request)
    {
      $kelas_id = $request->kelas_id;
      $guru_id = $request->guru_id;

      $kelas = Kelas::pluck('nama_kelas','id')->toArray();
      $guru = Guru::pluck('nama_guru','id')->toArray();
      // $guru = Guru::pluck('nama_guru','id')->all();
        $row = 1;

      if($kelas_id == "0" AND $guru_id == "0"){
        $pelanggaran = DB::table('pelanggaran')
                        ->select('siswa.nama_siswa','aturan.nama_aturan','aturan.poin','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('aturan','aturan.id','=','pelanggaran.aturan_id')
                        ->leftJoin('siswa','siswa.id','=','pelanggaran.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->get();

        $info_kelas = "Semua Kelas";
        $info_guru =  "Semua Guru Asuh";
      }elseif($kelas_id != "0" AND $guru_id != "0"){
          $pelanggaran = DB::table('pelanggaran')
                        ->select('siswa.nama_siswa','aturan.nama_aturan','aturan.poin','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('aturan','aturan.id','=','pelanggaran.aturan_id')
                        ->leftJoin('siswa','siswa.id','=','pelanggaran.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->where('siswa.guru_asuh_id', $guru_id)
                        ->where('siswa.kelas_id', $kelas_id)
                        ->get();
          $info_kelas = DB::table('kelas')
                ->where('id', '=', $kelas_id)
                ->value('nama_kelas');
          $info_guru = DB::table('guru')
                ->where('id', '=', $guru_id)
                ->value('nama_guru');
      }else{
        if($kelas_id == "0"){
          $pelanggaran = DB::table('pelanggaran')
                        ->select('siswa.nama_siswa','aturan.nama_aturan','aturan.poin','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('aturan','aturan.id','=','pelanggaran.aturan_id')
                        ->leftJoin('siswa','siswa.id','=','pelanggaran.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->where('siswa.guru_asuh_id', $guru_id)
                        ->get();
          $info_kelas = "Semua Kelas";
          $info_guru = DB::table('guru')
                ->where('id', '=', $guru_id)
                ->value('nama_guru');
        }else{
          $pelanggaran = DB::table('pelanggaran')
                        ->select('siswa.nama_siswa','aturan.nama_aturan','aturan.poin','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('aturan','aturan.id','=','pelanggaran.aturan_id')
                        ->leftJoin('siswa','siswa.id','=','pelanggaran.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->where('siswa.kelas_id', $kelas_id)
                        ->get();

          $info_kelas = DB::table('kelas')
                ->where('id', '=', $kelas_id)
                ->value('nama_kelas');
          $info_guru = "Semua Guru Asuh";
        }
      }

        return view('operator.laporan_pelanggaran',compact('pelanggaran','row','kelas','guru','info_kelas','info_guru'));
    }

    function laporan_hukuman()
    {
      $kelas = Kelas::pluck('nama_kelas','id')->toArray();
      $guru = Guru::pluck('nama_guru','id')->toArray();
      // $guru = Guru::pluck('nama_guru','id')->all();
      $info_kelas = "Semua Kelas";
      $info_guru =  "Semua Guru Asuh";

    	// $guru = Guru::pluck('nama_guru','id')->all();
	    $row = 1;
	    $hukuman = DB::table('hukuman')
                        ->select('siswa.nama_siswa','sangsi.nama_sangsi','sangsi.kompensasi','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('sangsi','sangsi.id','=','hukuman.sangsi_id')
                        ->leftJoin('siswa','siswa.id','=','hukuman.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->get();

	      return view('operator.laporan_hukuman',compact('hukuman','row','kelas','guru','info_kelas','info_guru'));
    }

    function laporan_hukuman2(Request $request)
    {
      $kelas_id = $request->kelas_id;
      $guru_id = $request->guru_id;

      $kelas = Kelas::pluck('nama_kelas','id')->toArray();
      $guru = Guru::pluck('nama_guru','id')->toArray();
      // $guru = Guru::pluck('nama_guru','id')->all();
        $row = 1;

      if($kelas_id == "0" AND $guru_id == "0"){
        $hukuman = DB::table('hukuman')
                        ->select('siswa.nama_siswa','sangsi.nama_sangsi','sangsi.kompensasi','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('sangsi','sangsi.id','=','hukuman.sangsi_id')
                        ->leftJoin('siswa','siswa.id','=','hukuman.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->get();

        $info_kelas = "Semua Kelas";
        $info_guru =  "Semua Guru Asuh";
      }elseif($kelas_id != "0" AND $guru_id != "0"){
          $hukuman = DB::table('hukuman')
                        ->select('siswa.nama_siswa','sangsi.nama_sangsi','sangsi.kompensasi','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('sangsi','sangsi.id','=','hukuman.sangsi_id')
                        ->leftJoin('siswa','siswa.id','=','hukuman.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->where('siswa.guru_asuh_id', $guru_id)
                        ->where('siswa.kelas_id', $kelas_id)
                        ->get();
          $info_kelas = DB::table('kelas')
                ->where('id', '=', $kelas_id)
                ->value('nama_kelas');
          $info_guru = DB::table('guru')
                ->where('id', '=', $guru_id)
                ->value('nama_guru');
      }else{
        if($kelas_id == "0"){
          $hukuman = DB::table('hukuman')
                        ->select('siswa.nama_siswa','sangsi.nama_sangsi','sangsi.kompensasi','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('sangsi','sangsi.id','=','hukuman.sangsi_id')
                        ->leftJoin('siswa','siswa.id','=','hukuman.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->where('siswa.guru_asuh_id', $guru_id)
                        ->get();
          $info_kelas = "Semua Kelas";
          $info_guru = DB::table('guru')
                ->where('id', '=', $guru_id)
                ->value('nama_guru');
        }else{
          $hukuman = DB::table('hukuman')
                        ->select('siswa.nama_siswa','sangsi.nama_sangsi','sangsi.kompensasi','kelas.nama_kelas','guru.nama_guru')
                        ->leftJoin('sangsi','sangsi.id','=','hukuman.sangsi_id')
                        ->leftJoin('siswa','siswa.id','=','hukuman.siswa_id')
                        ->leftJoin('kelas','kelas.id','=','siswa.kelas_id')
                        ->leftJoin('guru','guru.id','=','siswa.guru_asuh_id')
                        ->where('tahun_ajaran_id', $this->tahun_aktif)
                        ->where('siswa.kelas_id', $kelas_id)
                        ->get();

          $info_kelas = DB::table('kelas')
                ->where('id', '=', $kelas_id)
                ->value('nama_kelas');
          $info_guru = "Semua Guru Asuh";
        }
      }

        return view('operator.laporan_hukuman',compact('hukuman','row','kelas','guru','info_kelas','info_guru'));
    }

    public function lihat_detil($id){
      $pelanggaran = Pelanggaran::where('siswa_id',$id)->where('tahun_ajaran_id',$this->tahun_aktif)->where('status',0)->orderBy('tanggal','asc')->get();
      $hukuman = Hukuman::where('siswa_id',$id)->where('tahun_ajaran_id',$this->tahun_aktif)->orderBy('tanggal','desc')->get();

      foreach($pelanggaran as $pel){
        $pel->tanggal = $this->tanggal_indo($pel->tanggal,true);
      }

      foreach($hukuman as $pel1){
        $pel1->tanggal = $this->tanggal_indo($pel1->tanggal,true);
      }

      $row =1;
      $row1=1;

      return view('lihat_detil2',compact('pelanggaran','row','hukuman','row1'));
    }


    function tanggal_indo($tanggal, $cetak_hari)
    {
      $hari = array ( 1 => 'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');

      $bulan = array (1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli',
            'Agustus','September','Oktober','November','Desember');

      // $jam = date('H:i:s', strtotime($tanggal));
      $tanggal = date('Y-m-d', strtotime($tanggal));

      // $split   = explode(' ', $row['tanggal_terbit']);
      // $tanggal = $split[0];
      $split 	  = explode('-', $tanggal);
      $tgl_indo = $split[2] . '-' . $split[1]. '-' . $split[0];

      if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo; //.' '.$jam;
      }
      return $tgl_indo;
    }
}
