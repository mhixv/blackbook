<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Pelanggaran;
use App\Hukuman;
use DB;

class HomeController extends Controller
{
    private $tahun_aktif;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->tahun_aktif = DB::table('pengaturan')->value('tahun_aktif');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function lihat(){
      return view('lihat');
    }

    public function lihat_data(Request $request){
      // dd($request);
      $this->validate($request, [
        'nama' => 'required'
      ]);

      $row =1;

      $hasil = Siswa::where('nama_siswa','like','%' .$request->nama. '%')->get();

      return view('lihat_hasil',compact('hasil','row'));
    }
    

    public function lihat_detil($id){
      $pelanggaran = Pelanggaran::where('siswa_id',$id)->where('tahun_ajaran_id',$this->tahun_aktif)->orderBy('tanggal','asc')->get();
      $hukuman = Hukuman::where('siswa_id',$id)->where('tahun_ajaran_id',$this->tahun_aktif)->orderBy('tanggal','desc')->get();

      foreach($pelanggaran as $pel){
        $pel->tanggal = $this->tanggal_indo($pel->tanggal,true);
      }

      foreach($hukuman as $pel1){
        $pel1->tanggal = $this->tanggal_indo($pel1->tanggal,true);
      }

      $row =1;
      $row1=1;

      return view('lihat_detil',compact('pelanggaran','row','hukuman','row1'));
    }




    function tanggal_indo($tanggal, $cetak_hari)
    {
      $hari = array ( 1 => 'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');

      $bulan = array (1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli',
            'Agustus','September','Oktober','November','Desember');

      // $jam = date('H:i:s', strtotime($tanggal));
      $tanggal = date('Y-m-d', strtotime($tanggal));

      // $split   = explode(' ', $row['tanggal_terbit']);
      // $tanggal = $split[0];
      $split 	  = explode('-', $tanggal);
      $tgl_indo = $split[2] . '-' . $split[1]. '-' . $split[0];

      if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo; //.' '.$jam;
      }
      return $tgl_indo;
    }
}
