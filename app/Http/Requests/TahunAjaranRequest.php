<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TahunAjaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
   	{
   		return [
   			'nama_tahun' => 'required'
   		];
   	}

   	public function messages()
   	{
   		return [
   			'nama_tahun.required' => 'Isian Nama Tahun Ajaran tidak boleh kosong'
   		// 	'semester.required' => 'Isian Nama Semester tidak boleh kosong'
   		];
   	}
}
