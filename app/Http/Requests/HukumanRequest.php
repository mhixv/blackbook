<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HukumanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       return [
         'siswa_id' => 'required',
         'sangsi_id' => 'required',
        //  'poin' => 'required',
         'tanggal' => 'required',
        //  'guru_id' => 'required',
         'keterangan' => 'required'
       ];
     }

     public function messages()
     {
       return [
         'siswa_id.required' => 'Isian Nama Siswa tidak boleh kosong',
         'sangsi_id.required' => 'Isian Nama Aturan tidak boleh kosong',
        //  'poin.required' => 'Isian Jumlah Poin tidak boleh kosong',
         'tanggal.required' => 'Isian Tanggal tidak boleh kosong',
        //  'guru_id.required' => 'Isian Nama Guru tidak boleh kosong',
         'keterangan.required' => 'Silahkan isi keterangan hukuman'
       ];
     }
}
