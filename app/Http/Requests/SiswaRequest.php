<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       return [
         'nama_siswa' => 'required',
         'nis' => 'required',
         'nisn' => 'required',
         'kelas_id' => 'required',
         'guru_asuh_id' => 'required'
       ];
     }

     public function messages()
     {
       return [
         'nama_siswa.required' => 'Isian Nama Siswa tidak boleh kosong',
         'nis.required' => 'Isian NIS Siswa tidak boleh kosong',
         'nisn.required' => 'Isian NISN Siswa tidak boleh kosong',
         'kelas_id.required' => 'Isian Nama Kelas tidak boleh kosong',
         'guru_asuh_id.required' => 'Isian Nama Guru Asuh tidak boleh kosong'
       ];
     }
}
