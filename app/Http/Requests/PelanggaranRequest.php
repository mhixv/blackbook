<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PelanggaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       return [
         'siswa_id' => 'required',
         'aturan_id' => 'required',
        //  'poin' => 'required',
         'tanggal' => 'required',
         'guru_id' => 'required',
         'keterangan' => 'required'
       ];
     }

     public function messages()
     {
       return [
         'siswa_id.required' => 'Isian Nama Siswa tidak boleh kosong',
         'aturan_id.required' => 'Isian Nama Aturan tidak boleh kosong',
        //  'poin.required' => 'Isian Jumlah Poin tidak boleh kosong',
         'tanggal.required' => 'Isian Tanggal tidak boleh kosong',
         'guru_id.required' => 'Isian Nama Guru tidak boleh kosong',
         'keterangan.required' => 'Silahkan isi keterangan hukuman'
       ];
     }
}
