<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenggunaRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nama' => 'required|max:30',
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed'
		];
	}

	public function messages()
	{
		return [
			'nama.required' => 'Username harus diisi',
			'nama.max' => 'Nama pengguna tidak lebih dari 30 karakter.',
			'nama.alpha_num' => 'Nama pengguna hanya boleh huruf dan angka.',
			'email.required' => 'Alamat E-Mail harus diisi',
			'email.email' => 'Gunakan E-mail yang valid',
			'email.unique' => 'Email ini sudah digunakan',
			'password.required' => 'Password tidak boleh kosong',
			'password.confirmed' => 'Password yang anda masukan tidak sama'
		];
	}

}
