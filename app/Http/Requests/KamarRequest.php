<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KamarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'jenis' => 'required',
        'nomor_kamar' => 'required',
      ];
    }

    public function messages()
    {
      return [
        'jenis.required' => 'Isian Jenis Asrama tidak boleh kosong',
        'nomor_kamar.required' => 'Isian Nomor Kamar tidak boleh kosong'
      ];
    }
}
