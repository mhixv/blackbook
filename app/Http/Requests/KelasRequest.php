<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KelasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       return [
         'nama_kelas' => 'required',
         'wali_kelas_id' => 'required|not_in:0'
       ];
     }

     public function messages()
     {
       return [
         'nama_kelas.required' => 'Isian Nama Kelas tidak boleh kosong',
         'wali_kelas_id.required' => 'Isian Wali Kelas tidak boleh kosong',
         'wali_kelas_id.not_in' => 'Isian Wali Kelas tidak boleh kosong'
       ];
     }
}
