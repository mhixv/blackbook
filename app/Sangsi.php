<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sangsi extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'sangsi';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];
}
