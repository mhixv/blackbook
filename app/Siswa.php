<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'siswa';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function guru_asuh()
  {
    return $this->belongsTo('App\Guru','guru_asuh_id','id');
  }

  public function kelas()
  {
    return $this->belongsTo('App\Kelas','kelas_id','id');
  }

}
