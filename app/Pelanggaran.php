<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggaran extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'pelanggaran';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function siswa()
  {
    return $this->belongsTo('App\Siswa','siswa_id','id');
  }
  public function aturan()
  {
    return $this->belongsTo('App\Aturan','aturan_id','id');
  }

  public function guru()
  {
    return $this->belongsTo('App\Guru','guru_id','id');
  }
}
