<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hukuman extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'hukuman';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function siswa()
  {
    return $this->belongsTo('App\Siswa','siswa_id','id');
  }
  public function sangsi()
  {
    return $this->belongsTo('App\Sangsi','sangsi_id','id');
  }

  public function guru()
  {
    return $this->belongsTo('App\Guru','guru_id','id');
  }
}
