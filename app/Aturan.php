<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aturan extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'aturan';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function pengguna()
  {
    return $this->belongsTo('App\Pengguna','user_id','id');
  }
}
