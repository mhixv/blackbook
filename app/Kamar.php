<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kamar extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'kamar';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function siswa()
  {
    return $this->hasMany('App\Siswa','kamar_id','id');
  }
}
