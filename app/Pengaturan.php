<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengaturan extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'pengaturan';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];
}
