<?php

use App\User;

class UserSeeder extends Seeder {
  public function run()
  {
    DB::table('users')->delete();

    $faker = Faker\Factory::create();
    $limit = 13;


    for ($i = 1; $i <= $limit; $i++) {
        DB::table('users')->insert([
          'nama' => $faker->name,
          'email' => 'admin'.$i.'@tes.com',
          'password' => bcrypt('000000'),
          'hak_akses' => 1,
        ]);
    }

    for ($i = 1; $i <= $limit; $i++) {
        DB::table('users')->insert([
          'nama' => $faker->name,
          'email' => 'operator'.$i.'@tes.com',
          'password' => bcrypt('000000'),
          'hak_akses' => 2,
        ]);
    }
  }
}