<?php

use Illuminate\Database\Seeder;
use App\TahunAjaran;
use App\Kelas;
use App\Kamar;
use App\Pengaturan;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TahunAjaranSeeder::class);
        $this->call(PengaturanSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(GuruSeeder::class);
        // $this->call(KelasSeeder::class);
        // $this->call(KamarSeeder::class);
    }
}

class TahunAjaranSeeder extends Seeder {
  public function run()
  {
    DB::table('tahun_ajaran')->delete();

    $tahun = [
      ['id'=>'1', 'nama_tahun'=>'2017/2018'],
      ['id'=>'2', 'nama_tahun'=>'2018/2019'],
      ['id'=>'3', 'nama_tahun'=>'2019/2020'],
      ['id'=>'4', 'nama_tahun'=>'2020/2021'],
    ];

    TahunAjaran::insert($tahun);
  }
}

// class KelasSeeder extends Seeder {
//   public function run()
//   {
//     DB::table('kelas')->delete();
//
//     $tahun = [
//       ['id'=>'1', 'nama_kelas'=>'Kelas X IPA 1','wali_kelas_id'=],
//       ['id'=>'2', 'nama_kelas'=>'Kelas X IPA 2'],
//       ['id'=>'3', 'nama_kelas'=>'Kelas X IPS 1'],
//       ['id'=>'4', 'nama_kelas'=>'Kelas X IPS 2'],
//       ['id'=>'5', 'nama_kelas'=>'Kelas XI IPA 1'],
//       ['id'=>'6', 'nama_kelas'=>'Kelas XI IPA 2'],
//       ['id'=>'7', 'nama_kelas'=>'Kelas XI IPS 1'],
//       ['id'=>'8', 'nama_kelas'=>'Kelas XI IPS 2'],
//       ['id'=>'9', 'nama_kelas'=>'Kelas XII IPA 1'],
//       ['id'=>'10', 'nama_kelas'=>'Kelas XII IPA 2'],
//       ['id'=>'11', 'nama_kelas'=>'Kelas XII IPA 3'],
//       ['id'=>'12', 'nama_kelas'=>'Kelas XII IPA 1'],
//     ];
//
//     Kelas::insert($tahun);
//   }
// }

// class KamarSeeder extends Seeder {
//   public function run()
//   {
//     DB::table('kamar')->delete();
//
//     $tahun = [
//       ['id'=>'1', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'101'],
//       ['id'=>'2', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'102'],
//       ['id'=>'3', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'103'],
//       ['id'=>'4', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'104'],
//       ['id'=>'5', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'105'],
//       ['id'=>'6', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'106'],
//       ['id'=>'7', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'107'],
//       ['id'=>'8', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'108'],
//       ['id'=>'9', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'201'],
//       ['id'=>'10', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'202'],
//       ['id'=>'11', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'203'],
//       ['id'=>'12', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'204'],
//       ['id'=>'13', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'205'],
//       ['id'=>'14', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'206'],
//       ['id'=>'15', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'207'],
//       ['id'=>'16', 'jenis'=>'Asrama Putra 1','nomor_kamar'=>'208'],
//
//       ['id'=>'17', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'101'],
//       ['id'=>'18', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'102'],
//       ['id'=>'19', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'103'],
//       ['id'=>'20', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'104'],
//       ['id'=>'21', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'105'],
//       ['id'=>'22', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'106'],
//       ['id'=>'23', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'107'],
//       ['id'=>'24', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'108'],
//       ['id'=>'25', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'201'],
//       ['id'=>'26', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'202'],
//       ['id'=>'27', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'203'],
//       ['id'=>'28', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'204'],
//       ['id'=>'29', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'205'],
//       ['id'=>'30', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'206'],
//       ['id'=>'31', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'207'],
//       ['id'=>'32', 'jenis'=>'Asrama Putra 2','nomor_kamar'=>'208'],
//
//       ['id'=>'33', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'101'],
//       ['id'=>'34', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'102'],
//       ['id'=>'35', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'103'],
//       ['id'=>'36', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'104'],
//       ['id'=>'37', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'105'],
//       ['id'=>'38', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'106'],
//       ['id'=>'39', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'107'],
//       ['id'=>'40', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'108'],
//       ['id'=>'41', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'201'],
//       ['id'=>'42', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'202'],
//       ['id'=>'43', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'203'],
//       ['id'=>'44', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'204'],
//       ['id'=>'45', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'205'],
//       ['id'=>'46', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'206'],
//       ['id'=>'47', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'207'],
//       ['id'=>'48', 'jenis'=>'Asrama Putri 1','nomor_kamar'=>'208'],
//
//       ['id'=>'49', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'101'],
//       ['id'=>'50', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'102'],
//       ['id'=>'51', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'103'],
//       ['id'=>'52', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'104'],
//       ['id'=>'53', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'105'],
//       ['id'=>'54', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'106'],
//       ['id'=>'55', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'107'],
//       ['id'=>'56', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'108'],
//       ['id'=>'57', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'201'],
//       ['id'=>'58', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'202'],
//       ['id'=>'59', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'203'],
//       ['id'=>'60', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'204'],
//       ['id'=>'61', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'205'],
//       ['id'=>'62', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'206'],
//       ['id'=>'63', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'207'],
//       ['id'=>'64', 'jenis'=>'Asrama Putri 2','nomor_kamar'=>'208'],
//
//     ];
//
//     Kamar::insert($tahun);
//   }
// }

class PengaturanSeeder extends Seeder {
  public function run()
  {
    DB::table('pengaturan')->delete();

    $tahun = [
      ['id'=>'1', 'tahun_aktif'=> 1]
    ];

    Pengaturan::insert($tahun);
  }
}
