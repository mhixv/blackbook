<?php

use App\Guru;

class GuruSeeder extends Seeder {
  public function run()
  {
    DB::table('guru')->delete();

    $tahun = [
      ['id'=>1, 'nama_guru'=> 'Dra. H. Hayatirruh, M.Ed'],
      ['id'=>2, 'nama_guru'=> 'Budianto, S.Sos'],
      ['id'=>3, 'nama_guru'=> 'Alexander Zulkarnain, S.Pd.Gr'],
      ['id'=>4, 'nama_guru'=> 'Alfianri, S.Pd'],
      ['id'=>5, 'nama_guru'=> 'Ari Luqman, S.Pd'],
      ['id'=>6, 'nama_guru'=> 'Asrul Sani, S.Pd.I'],
      ['id'=>7, 'nama_guru'=> 'Fatmawati, S.Pd.I'],
      ['id'=>8, 'nama_guru'=> 'Rahmita, S.Pd'],
      ['id'=>9, 'nama_guru'=> 'Sadriadi, S.Pd.I'],
      ['id'=>10, 'nama_guru'=> 'Wiwit Susanti, S.Pd'],
      ['id'=>11, 'nama_guru'=> 'Henri, S.Kom'],
      ['id'=>12, 'nama_guru'=> 'Isfan Fahrevibafnis, S.Pd'],
      ['id'=>13, 'nama_guru'=> 'Offi Trisia, S.Pd'],
      ['id'=>14, 'nama_guru'=> 'Rahmad Istiadi, S.Pd'],
      ['id'=>15, 'nama_guru'=> 'Razky, S.Pd'],
      ['id'=>16, 'nama_guru'=> 'Ria Susanti, S.Pd.I'],
      ['id'=>17, 'nama_guru'=> 'Yuli Armelia, S.Pd'],
      ['id'=>18, 'nama_guru'=> 'Atika Ulfa Novriani, S.Pd'],
      ['id'=>19, 'nama_guru'=> 'Cholid, S.Pd, M.A'],
      ['id'=>20, 'nama_guru'=> 'Dwi Yulia Handayani, M.Pd'],
      ['id'=>21, 'nama_guru'=> 'Jasri, S.Pd.I'],
      ['id'=>22, 'nama_guru'=> 'Jefri Asra, S.Pd'],
      ['id'=>23, 'nama_guru'=> 'Juni Kardian, S.Pd'],
      ['id'=>24, 'nama_guru'=> 'Martina Nengsih, S.Pd'],
      ['id'=>25, 'nama_guru'=> 'Masdaril Khairi, S.Pd.I'],
      ['id'=>26, 'nama_guru'=> 'Nabilah Hasibuan, S.Pd.I'],
      ['id'=>27, 'nama_guru'=> 'Prilia Dwiher Fitriana, S.Pd'],
      ['id'=>28, 'nama_guru'=> 'Rudiansyah Hasibuan, S.Pd'],
      ['id'=>29, 'nama_guru'=> 'Siti Asia, S.H.I, M.Pd'],
      ['id'=>30, 'nama_guru'=> 'Vitri Juliani, S.Pd'],
      ['id'=>31, 'nama_guru'=> 'Yovi Ersariadi, S.S, M. Hum']
    ];

    Guru::insert($tahun);
  }
}