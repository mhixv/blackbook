<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToPelanggaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pelanggaran', function(Blueprint $table)
        {
          $table->integer('status')->after('tahun_ajaran_id');
          $table->integer('hukuman_id')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pelanggaran', function(Blueprint $table)
  		{
  			$table->dropColumn('status');
        $table->dropColumn('hukuman_id');
  		});
    }
}
