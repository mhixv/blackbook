<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HukumanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hukuman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('siswa_id');
            $table->integer('sangsi_id');
            $table->date('tanggal');
            $table->integer('tahun_ajaran_id');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('hukuman');
    }
}
