<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeteranganSangsiToSangsiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sangsi', function(Blueprint $table)
        {
          $table->text('keterangan_sangsi')->after('nama_sangsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('sangsi', function(Blueprint $table)
  		{
  			$table->dropColumn('keterangan_sangsi');
  		});
    }
}
