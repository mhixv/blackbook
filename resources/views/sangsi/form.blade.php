<div class="form-group">
	{!! Form::label('nama_sangsi', 'Nama Sangsi :') !!}
	{!! Form::text('nama_sangsi',null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('keterangan_sangsi', 'Keterangan Sangsi :') !!}
	{!! Form::textarea('keterangan_sangsi',null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('kompensasi', 'Nilai Kompensasi :') !!}
	{!! Form::text('kompensasi',null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-4 col-md-offset-4">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
