
<div class="form-group">
	{!! Form::label('nama_guru', 'Nama Guru :') !!}
	{!! Form::text('nama_guru',null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-4 col-md-offset-4">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
