@extends('admin_index')

@section('content-header')
				<h1 class="page-header">Membuat Pengguna Baru</h1>
@endsection
<!-- /.row -->
@section('content')
<div class="row">
		<div class="col-lg-6">
				<div class="panel panel-default">
						<div class="panel-heading">Menambah Pengguna</div>
						<div class="panel-body">
							<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/pengguna') }}">
									{{ csrf_field() }}
									<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
											<label for="nama" class="col-md-4 control-label">Nama Pengguna</label>

											<div class="col-md-6">
													<input id="nama" type="text" class="form-control" name="nama" value="{{ old('nama') }}">

													@if ($errors->has('nama'))
															<span class="help-block">
																	<strong>{{ $errors->first('nama') }}</strong>
															</span>
													@endif
											</div>
									</div>
									<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
											<label for="email" class="col-md-4 control-label">Alamat E-Mail Pengguna</label>

											<div class="col-md-6">
													<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">

													@if ($errors->has('email'))
															<span class="help-block">
																	<strong>{{ $errors->first('email') }}</strong>
															</span>
													@endif
											</div>
									</div>

									<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
											<label for="password" class="col-md-4 control-label">Password</label>

											<div class="col-md-6">
												  {!! Form::password('password', [ 'class'=> 'form-control','id'=>'password']) !!}
													<!-- <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}">
													-->
													@if ($errors->has('password'))
															<span class="help-block">
																	<strong>{{ $errors->first('password') }}</strong>
															</span>
													@endif
											</div>
									</div>
									<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
											<label for="password_confirmation" class="col-md-4 control-label">Konfirmasi Password</label>

											<div class="col-md-6">
													{!! Form::password('password_confirmation', [ 'class'=> 'form-control','id'=>'password']) !!}
													<!--<input id="password_confirmation" type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
													-->
													@if ($errors->has('password_confirmation'))
															<span class="help-block">
																	<strong>{{ $errors->first('password_confirmation') }}</strong>
															</span>
													@endif
											</div>
									</div>
									<div class="form-group{{ $errors->has('hak_akses') ? ' has-error' : '' }}">
											<label for="hak_akses" class="col-md-4 control-label">Hak Akses</label>

											<div class="col-md-6">
												  {!! Form::select('hak_akses', ['1'=> 'Administrator Pusat','2' => 'Operator'], 1,['class' => 'form-control']) !!}
													<!-- ['255'=> 'Administrator','200' => 'Manager Pusat','100' => 'Manager Propinsi'] -->
													@if ($errors->has('hak_akses'))
															<span class="help-block">
																	<strong>{{ $errors->first('hak_akses') }}</strong>
															</span>
													@endif
											</div>
									</div>

									<div class="form-group">
										<label for="" class="col-md-4 control-label"></label>
									</div>
									<div class="form-group">
											<div class="col-md-6 col-md-offset-4">
													<button type="submit" class="btn btn-primary">
															<i class="fa fa-btn fa-user"></i> Simpan
													</button>
											</div>
									</div>
							</form>
						</div>
						<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('script')
@endsection
