@extends('admin_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}

@stop


@section('content-header')
	 <h1>Daftar Pengguna Aplikasi Black Book</h1>
@endsection

@section('content')
<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5 col-sm-offset-3 col-xs-offset-1 bottom10">
						<a class="btn btn-success" href="{!! URL::to('admin/pengguna/create') !!}"><i class="fa fa-plus-square fa-fw"></i> Tambah Pengguna</a><br>
					</div>
					<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="tabel-provinsi">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Pengguna</th>
										<th>Alamat E-Mail</th>
										<th>Hak Akses</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($pengguna as $user)
									<tr>
										<td class="center">
											{{ $counter++}}
										</td>
										<td>
											{{ $user->nama }}
									    </td>
										<td>
											{{ $user->email }}
										</td>
										<td>
											@if($user->hak_akses == 1)
												<p>Administrator</p>
											@else
												<p>Operator</p>
											@endif
										</td>
										<td class="center">
	                      <a class="btn btn-xs btn-success" href="{!! URL::to('admin/changepassworduser/'.$user->id) !!}"><i class="fa fa-repeat fa-fw"></i> Reset Password</a>
	                      &nbsp;&nbsp;
	                      <a class="btn btn-xs btn-info" href="{!! URL::to('admin/pengguna/'.$user->id.'/edit') !!}"><i class="fa fa-edit fa-fw"></i> Edit</a>
												&nbsp;&nbsp;
												<a class="btn btn-xs btn-danger" href="{!! URL::to('admin/pengguna/delete/'.$user->id) !!}" data-token="{{csrf_token()}}" data-method="delete" data-confirm="Anda yakin menghapus pengguna ini?"><i class="fa fa-remove fa-fw"></i> Hapus</a>
									   </td>
									</tr>
									@endforeach
								</tbody>
					   </table>
			 		</div>
				</div>
		 		<!-- /.panel-body -->
 			</div>
 			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
			$('#tabel-provinsi').DataTable({
							responsive: true,
							bSort : false,
              language: {
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data Provinsi.",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
              },
              "columnDefs": [
                { "orderable": false, "targets": 0 },
								{ width: 20, targets: 0 }
              ]
			});
	});
	</script>

	<!-- Delete Data JavaScript - Jeffry Wayy -->
	{!! Html::script('js/laravel.js', array('type' => 'text/javascript')) !!}

@stop
