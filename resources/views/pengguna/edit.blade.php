@extends('admin_index')

@section('content-header')
				<h1 class="page-header">Merubah Provinsi</h1>
@endsection
@section('content')
<div class="row">
		<div class="col-lg-6">
				<div class="panel panel-default">
						<div class="panel-heading">
								Merubah Pengguna
						</div>
						<div class="panel-body">
  								{!! Form::model($pengguna,['method' => 'PATCH','url' => ['admin/pengguna',$pengguna->id], 'class'=>'form-horizontal']) !!}
  										@include ('pengguna.form', ['text' => 'Simpan'])
  								{!! Form::close() !!}
						</div>
						<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('script')

@endsection
