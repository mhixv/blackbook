<div class="form-group {{ $errors->has('pendataan_pelaksana') ? ' has-error' : '' }}">
	{!! Form::label('pendataan_pelaksana', 'Pelaksana :') !!}
	{!! Form::text('pendataan_pelaksana',null, ['class' => 'form-control', 'placeholder' => 'Siapa yang melaksanakan?', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('pendataan_jml_pelaksanaan') ? ' has-error' : '' }}">
	{!! Form::label('pendataan_jml_pelaksanaan', 'Jumlah Pelaksanaan :') !!}
	{!! Form::text('pendataan_jml_pelaksanaan',null, ['class' => 'form-control', 'placeholder' => 'Berapa kali aktivitas dilaksanakan?', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('pendataan_lokasi_pelaksanaan') ? ' has-error' : '' }}">
	{!! Form::label('pendataan_lokasi_pelaksanaan', 'Lokasi Pelaksanaan :') !!}
	{!! Form::text('pendataan_lokasi_pelaksanaan',null, ['class' => 'form-control', 'placeholder' => 'Dimana saja aktivitas dilaksanakan?', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('pendataan_waktu_pelaksanaan') ? ' has-error' : '' }}">
	{!! Form::label('pendataan_waktu_pelaksanaan', 'Waktu Pelaksanaan :') !!}
    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
	{!! Form::text('pendataan_waktu_pelaksanaan',null, ['id'=> 'tanggal_pelaksanaan', 'class' => 'form-control', 'readonly' => $disabled]) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('pendataan_sumber_anggaran') ? ' has-error' : '' }}">
	{!! Form::label('pendataan_sumber_anggaran', 'Sumber Anggaran :') !!}
	{!! Form::text('pendataan_sumber_anggaran',null, ['class' => 'form-control', 'placeholder' => 'Anggaran bersumber dari mana saja?', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('pendataan_jml_anggaran') ? ' has-error' : '' }}">
	{!! Form::label('pendataan_jml_anggaran', 'Jumlah Anggaran :') !!}
	{!! Form::text('pendataan_jml_anggaran',null, ['class' => 'form-control', 'placeholder' => 'Berapa Anggaran yang digunakanRp ___.___.___.___','id' => 'anggaran_1', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('pendataan_jml_kat_1') ? ' has-error' : '' }} {{ $errors->has('pendataan_jml_kat_3') ? ' has-error' : '' }} {{ $errors->has('pendataan_jml_kat_3') ? ' has-error' : '' }}">
	{!! Form::label('penerima_anggaran', 'Jumlah Penerima Anggaran Per-Kategori :') !!}
<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI</div>
	{!! Form::text('pendataan_jml_kat_1',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat Kategori 1', 'readonly' => $disabled]) !!}
	{!! Form::text('pendataan_jml_kat_2',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat Kategori 2', 'readonly' => $disabled]) !!}
	{!! Form::text('pendataan_jml_kat_3',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat Kategori 3', 'readonly' => $disabled]) !!}
</div>
</div>

<div class="form-group {{ $errors->has('pendataan_deskripsi') ? ' has-error' : '' }}">
	{!! Form::label('deskripsi_singkat_aktifitas', 'Deskripsi Singkat Aktifitas :') !!}
	{!! Form::textarea('pendataan_deskripsi',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Deskripsi singkat aktifitas', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('kendala') ? ' has-error' : '' }}">
	{!! Form::label('kendala', 'Kendala :') !!}
<!-- <div class="form-group"> -->
	{!! Form::textarea('pendataan_kendala',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Jelaskan kendala-kendalah yang dihadapi!', 'readonly' => $disabled]) !!}
</div>

@if($pen_ver->status == 3)
<div class="form-group">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
@endif
@if($pen_ver->status == 2)
	<div class="alert alert-danger">
		<h4 style="text-align:center">Data Pendataan dan Verifikasi dalam proses Verifikasi Pusat</h4>
	</div>
@endif
@if($pen_ver->status == 3)
	<div class="alert alert-danger">
		<h4 style="text-align:center">Data Pendataan dan Verifikasi diminta revisi oleh Pusat</h4>
	</div>
@endif
@if($pen_ver->status == 4)
	<div class="alert alert-info">
		<h4 style="text-align:center">Data Pendataan dan Verifikasi telah disetujui Pusat</h4>
	</div>
@endif
