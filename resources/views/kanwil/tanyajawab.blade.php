@extends('kanwil/kanwil_index')

@section('content-header')
		<h1>
		Daftar Pertanyaan dan Jawaban
		<small>Daftar Pertanyaan dan Jawaban</small>
		</h1>
@endsection
@section('content')
<!-- row -->
<div class="row">
	<div class="col-md-12">
      <!-- DIRECT CHAT -->
      <div class="box box-primary box-solid direct-chat direct-chat-warning" style="min-height: 60px;">
        <div class="box-header">
          <h3 class="box-title">Pertanyaan dan Jawaban</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="max-height: 10";>
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
						@foreach ($tanyajawab as $tj)
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">{{ Auth::user()->nama }}</span>
                <span class="direct-chat-timestamp pull-right">{!! $tj->created_at !!}</span>
              </div>
              <!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="{{ asset('img/user1-128x128.jpg') }}" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                {!! $tj->pertanyaan !!}
              </div>
              <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Admin Pusat</span>
                <span class="direct-chat-timestamp pull-left">@if(!empty($tj->jawaban)){!! $tj->updated_at !!}@endif</span>
							</div>
              <!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="{{ asset('img/user2-128x128.jpg') }}" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                @if(!empty($tj->jawaban)) {!! $tj->jawaban !!} @endif
								@if(empty($tj->jawaban)) <b style="color: red;">Pertanyaan Belum di Jawab</b> @endif
              </div>
              <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->
						@endforeach
          </div>
          <!--/.direct-chat-messages-->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          {!! Form::open(['url' => 'kanwil/pertanyaan']) !!}
            <div class="input-group">
              <input type="text-area" name="pertanyaan" placeholder="Tulis Pertanyaan ..." class="form-control">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-warning btn-flat">Kirim Pertanyaan</button>
                  </span>
            </div>
          {!! Form::close() !!}
        </div>
        <!-- /.box-footer-->
      </div>
      <!--/.direct-chat -->
			<!-- <div class="row"> -->
				<div class="col-md-12">
					<!-- <div class="box"> -->
						@include('errors.list')
					<!-- </div> -->
				</div>
			<!-- </div> -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@stop
