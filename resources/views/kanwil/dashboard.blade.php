@extends('kanwil/kanwil_index')
@section('content-header')
  <table>
    <tr>
      <td rowspan="1" width="80px">
        <img src="{!! URL::to('/') !!}/img/logo/Kemenag-Logo.png">
      </td>
      <td>
        <h1>Sistem Informasi Program Indonesia Pintar Tahun {{ $date }}</h1>
      </td>
    </tr>
  </table>
@endsection
@section('content')
  <div class="row">
    <div class="col-lg-7">
      <!-- <div class="alert alert-warning"> -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-info-circle"></i>
          <h3 class="box-title"><b>Status Laporan Kegiatan</b></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <a href="{!! URL::to('kanwil/infoumum') !!}">
            @if($info_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> INFORMASI UMUM <i><b>sudah dilengkapi dan telah di Verifikasi oleh Pusat.</b></i></div>
            @elseif($info_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> INFORMASI UMUM <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($info_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> INFORMASI UMUM <i><b>sudah dilengkapi dan sedang dalam proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> INFORMASI UMUM <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>

          <a href="{!! URL::to('kanwil/sosialisasi') !!}">
            @if($sosialisasi_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> SOSIALISASI DAN KOORDINASI <i><b>sudah dilengkapi dan di Verifikasi oleh Pusat.</b></i></div>
            @elseif($sosialisasi_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> SOSIALISASI DAN KOORDINASI <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($sosialisasi_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> SOSIALISASI DAN KOORDINASI <i><b>sudah dilengkapi dan sedang dalam proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> SOSIALISASI DAN KOORDINASI <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>

          <a href="{!! URL::to('kanwil/pendataan') !!}">
            @if($pendataan_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> PENDATAAN DAN VERIFIKASI <i><b>sudah dilengkapi dan di Verifikasi oleh Pusat.</b></i></div>
            @elseif($pendataan_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> PENDATAAN DAN VERIFIKASI <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($pendataan_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> PENDATAAN DAN VERIFIKASI <i><b>sudah dilengkapi dan sedang proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> PENDATAAN DAN VERIFIKASI <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>
          <a href="{!! URL::to('kanwil/penetapan') !!}">
            @if($penetapan_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> PENETAPAN PENERIMA MANFAAT <i><b>sudah dilengkapi dan di Verifikasi oleh Pusat.</b></i></div>
            @elseif($penetapan_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> PENETAPAN PENERIMA MANFAAT <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($penetapan_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> PENETAPAN PENERIMA MANFAAT <i><b>sudah dilengkapi dan sedang proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> PENETAPAN PENERIMA MANFAAT <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>

          <a href="{!! URL::to('kanwil/penyaluran') !!}">
            @if($penyaluran_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> PENYALURAN DANA MANFAAT <i><b>sudah dilengkapi dan di Verifikasi oleh Pusat.</b></i></div>
            @elseif($penyaluran_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> PENYALURAN DANA MANFAAT <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($penyaluran_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> PENYALURAN DANA MANFAAT <i><b>sudah dilengkapi dan sedang proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> PENYALURAN DANA MANFAAT <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>

          <a href="{!! URL::to('kanwil/distribusi') !!}">
            @if($distribusi_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> DISTRIBUSI DANA MANFAAT <i><b>sudah dilengkapi dan di Verifikasi oleh Pusat.</b></i></div>
            @elseif($distribusi_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> DISTRIBUSI DANA MANFAAT <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($distribusi_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> DISTRIBUSI DANA MANFAAT <i><b>sudah dilengkapi dan sedang proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> DISTRIBUSI DANA MANFAAT <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>

          <a href="{!! URL::to('kanwil/monitoring') !!}">
            @if($monitoring_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> MONITORING DANA MANFAAT <i><b>sudah dilengkapi dan di Verifikasi oleh Pusat.</b></i></div>
            @elseif($monitoring_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> MONITORING DANA MANFAAT <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($monitoring_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> MONITORING DANA MANFAAT <i><b>sudah dilengkapi dan sedang proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> MONITORING DANA MANFAAT <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>

          <a href="{!! URL::to('kanwil/evaluasi') !!}">
            @if($evaluasi_status == 4)
              <div class="alert alert-success"><i class="fa fa-check"></i> EVALUASI DANA MANFAAT <i><b>sudah dilengkapi dan di Verifikasi oleh Pusat.</b></i></div>
            @elseif($evaluasi_status == 3)
              <div class="alert alert-warning"><i class="fa fa-times"></i> EVALUASI DANA MANFAAT <i><b>memerlukan perbaikan dari Pusat, silahkan perbaiki. </b></i></div>
            @elseif($evaluasi_status == 2)
              <div class="alert alert-info"><i class="fa fa-check"></i> EVALUASI DANA MANFAAT <i><b>sudah dilengkapi dan sedang proses Verifikas Pusat. </b></i></div>
            @else
              <div class="alert alert-error"><i class="fa fa-times"></i> EVALUASI DANA MANFAAT <i><b>belum dilengkapi, silahkan dilengkapi. </b></i></div>
            @endif
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-5">
      <div class="box box-success">
        <div class="box-header with-border">
          <i class="fa fa-bullhorn"></i>

          <h3 class="box-title"><b>Informasi Kementerian Agama Pusat :</b></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          @foreach ($informasi as $info)
          <div class="callout callout-success">
            <h4><u>{{ $info->judul_informasi }}</u></h4>
            <h5 style="color:yellow;">{{ $info->tanggal }}</h5>
            <p>{!! $info->isi_informasi !!}</p>

          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@stop
