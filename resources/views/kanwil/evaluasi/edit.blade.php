@extends('kanwil/kanwil_index')

@section('content-header')
	      <h1>
	        Evaluasi KIP
	        <small>Evaluasi KIP</small>
	      </h1>
@endsection
@section('content')
<div class="row">
	<!-- <div class="box box-info"> -->
		<div class="col-lg-7">
			<div class="box box-info">
			    <div class="box-header with-border">
			      <h3 class="box-title">Data Evaluasi KIP :</h3>
			    </div>
					<div class="box-body">
							{!! Form::model($evaluasi,['method' => 'PATCH','url' => ['kanwil/evaluasi',$evaluasi->id]]) !!}
								@include ('kanwil.evaluasi.form-edit', ['text' => 'Kirim Notifikasi Validasi'])
							{!! Form::close() !!}
					</div>
			</div>
		</div>
		<div class="col-lg-5">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Status Data :</h3>
					</div>
					<div class="box-body">
							@if ($evaluasi->status == 2)
								<b style="color : orange"> Menunggu Verifikasi.</b>
							@endif
							@if ($evaluasi->status == 3)
								<b style="color : red"> Revisi Pusat, Silahkan Edit Data dan Kirim Kembali</b>
							@endif
							@if ($evaluasi->status == 4)
								<b style="color : green"> Selesai.</b>
							@endif
					</div>
				</div>
				<!-- <div class="box box-danger">
					<div class="box-header with-border">
			      <h3 class="box-title">Informasi Feedback Dari Pusat :</h3>
			    </div>
					<div class="box-body">
              <p>{{ $evaluasi->feedback }}</p>
          </div>
        </div> -->
				<div class="row">
					<div class="col-md-12">
						@include('errors.list')
					</div>
				</div>
			</div>
	</div>
	@stop

	@section('script')

	<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script> -->

	<!-- <script>
	$(function() {
	  //$( "#tgl_lahir" ).datepicker();
	  $('#tanggal_pelaksanaan').datepicker({ dateFormat: "yy-mm-dd", changeMonth: true,
	          changeYear: true, yearRange: '2010:2020', defaultDate: ''
	      });
	});
	</script> -->


	<!-- <script type="text/javascript">
	$(function() {
	      //  $( "#tgl_lahir" ).datepicker( "option", "dateFormat", 'yy-mm-dd');
	}); -->

	</script>

	@endsection
