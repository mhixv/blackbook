<div class="form-group col-md-12">
	{!! Form::label('evaluasi_juknis', 'Petunjuk Teknis :') !!}
	{!! Form::textarea('evaluasi_juknis',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Apakah ketentuan dalam Petunjuk Teknis dapat dipahami dengan baik? Jelaskan!']) !!}
</div>

<div class="form-group col-md-12">
	{!! Form::label('evaluasi_target_kerja', 'Target Kinerja :') !!}
	{!! Form::textarea('evaluasi_target_kerja',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Apakah Target Kinerja yang diberikan telah sesuai dengan kondisi real di lapangan? Jelaskan!']) !!}
</div>

<div class="form-group col-md-12">
	{!! Form::label('evaluasi_pengelola_pip', 'Pengelola PIP Wilayah :') !!}
	{!! Form::textarea('evaluasi_pengelola_pip',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Apakah dibentuk Tim Pengeloa PIP di Wilayah? Siapa saja unsur yang terlibat? Jelaskan!']) !!}
</div>

<div class="form-group col-md-12">
	{!! Form::label('evaluasi_uu_terkait', 'Peraturan Perundang-Undangan Terkait :') !!}
	{!! Form::textarea('evaluasi_uu_terkait',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Bagamana hubungan Peraturan Perundang-undangan terkait dengan ketentuan dalam Pedoman/Petunjuk Teknis,  apakah ketentuan tersebut saling bersinergi atau  bertentangang? Jelaskan!']) !!}
</div>

<div class="form-group col-md-12">
	{!! Form::label('evaluasi_penggunaan_dana', 'Penggunaan Dana Manfaat :') !!}
	{!! Form::textarea('evaluasi_penggunaan_dana',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Apakah penyelenggaraan PIP sudah berjalan efektif untuk mencapai tujuan sebagaimana dalam Pedoman/Juknis? Jelaskan!']) !!}
</div>

<div class="form-group col-md-12">
	{!! Form::label('evaluasi_saran', 'Saran - Saran :') !!}
	{!! Form::textarea('evaluasi_saran',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Bagaimana saran-saran yang bisa diberikan untuk perbaikan kedepannya?']) !!}
</div>

<div class="form-group col-md-12">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
