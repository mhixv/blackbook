@extends('kanwil/kanwil_index')

@section('content-header')
	      <h1>
	        Evaluasi KIP
	        <small>Evaluasi KIP</small>
	      </h1>
@endsection

@section('content')
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Data Evaluasi KIP :</h3>
    </div>
				<div class="box-body">
						<div class="row">
							<div class="col-lg-6">
								{!! Form::open(['url' => 'kanwil/evaluasi']) !!}
									@include('kanwil.evaluasi.form-create', ['text' => 'Kirim Notifikasi Validasi'])
								{!! Form::close() !!}
							</div>
							<div class="col-lg-6">
								@if( $errors->first('monitoring_pelaksana') )
									<span class="help-block text-red">{{ $errors->first('monitoring_pelaksana') }}</span>
								@endif
								@if( $errors->first('monitoring_jml') )
									<span class="help-block text-red">{{ $errors->first('monitoring_jml') }}</span>
								@endif
								<br />
								@if( $errors->first('monitoring_lokasi') )
									<span class="help-block text-red">{{ $errors->first('monitoring_lokasi') }}</span>
								@endif
								@if( $errors->first('monitoring_waktu') )
									<span class="help-block text-red">{{ $errors->first('monitoring_waktu') }}</span>
								@endif
								<br />
								@if( $errors->first('monitoring_sumber_anggaran') )
									<span class="help-block text-red">{{ $errors->first('monitoring_sumber_anggaran') }}</span>
								@endif
								@if( $errors->first('monitoring_jml_anggaran') )
									<span class="help-block text-red">{{ $errors->first('monitoring_jml_anggaran') }}</span>
								@endif
								@if( $errors->first('monitoringi_deskripsi') )
									<span class="help-block text-red">{{ $errors->first('monitoringi_deskripsi') }}</span>
								@endif
								<!-- @include('errors.list') -->
							</div>
					</div>
					<!-- /.row (nested) -->
			</div>
			<!-- /.panel-body -->
</div>
<!-- /.row -->
@stop
