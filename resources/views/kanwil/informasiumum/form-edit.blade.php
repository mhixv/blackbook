<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 1</div>
	<!-- {!! Form::label('Katagori 1', 'KATEGORI 1 :') !!} -->
	{!! Form::text('k1_jml_penerima',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat', 'readonly' => $disabled]) !!}
	{!! Form::text('k1_jml_dana',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat', 'readonly' => $disabled,'id' => 'anggaran_1']) !!}
</div><br>

<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 2</div>
	<!-- {!! Form::label('Katagori 2', 'Kategori 2 :') !!} -->
	{!! Form::text('k2_jml_penerima',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat', 'readonly' => $disabled]) !!}
	{!! Form::text('k2_jml_dana',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat', 'readonly' => $disabled,'id' => 'anggaran_2']) !!}
</div><br>

<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 3</div>
	<!-- {!! Form::label('Katagori 3', 'Kategori 3 :') !!} -->
	{!! Form::text('k3_jml_penerima',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat', 'readonly' => $disabled]) !!}
	{!! Form::text('k3_jml_dana',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat', 'readonly' => $disabled,'id' => 'anggaran_3']) !!}
</div><br>

<div class="input-group col-md-12">
	{!! Form::label('anggaran_manajemen', 'Anggaran Manajemen :') !!}
	{!! Form::text('anggaran_manajemen',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Anggaran Layanan Manajemen PIP', 'readonly' => $disabled,'id' => 'anggaran_4']) !!}
</div><br>
@if($infoumum->status == 3)
<div class="form-group">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
@endif
@if($infoumum->status == 2)
	<div class="alert alert-danger">
		<h4 style="text-align:center">Data Informasi Umum dalam proses Verifikasi Pusat</h4>
	</div>
@endif
@if($infoumum->status == 3)
	<div class="alert alert-danger">
		<h4 style="text-align:center">Data Informasi Umum diminta revisi oleh Pusat</h4>
	</div>
@endif
@if($infoumum->status == 4)
	<div class="alert alert-info">
		<h4 style="text-align:center">Data Informasi Umum telah disetujui Pusat</h4>
	</div>
@endif
