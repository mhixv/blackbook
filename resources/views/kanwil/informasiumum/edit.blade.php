@extends('kanwil/kanwil_index')

@section('content-header')
	      <h1>
	        Informasi Umum
	        <small>Informasi Umum</small>
	      </h1>
@endsection
@section('content')
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Target Penerima dan Dana Manfaat Per-Kategori :</h3>
    </div>
		<!-- <div class="col-lg-6"> -->
				<!-- <div class="panel panel-default"> -->
						<div class="box-body">
								<div class="row">
									<!-- <div class="row"> -->
										<!-- <div class="col-md-8">
											@include('errors.list')
										</div> -->
									<!-- </div> -->
									<div class="col-lg-6">
										{!! Form::model($infoumum,['method' => 'PATCH','url' => ['kanwil/infoumum',$infoumum->id]]) !!}
											@include ('kanwil.informasiumum.form-edit', ['text' => 'Kirim Notifikasi Validasi'])
										{!! Form::close() !!}
											<!-- <div class="row">
												<div class="col-md-12">
													@include('errors.list')
												</div>
											</div> -->
									</div>
									<div class="col-lg-6">
										@if( $errors->first('k1_jml_penerima') )
											<span class="help-block text-red">{{ $errors->first('k1_jml_penerima') }}</span>
										@endif
										@if( $errors->first('k1_jml_dana') )
											<span class="help-block text-red">{{ $errors->first('k1_jml_dana') }}</span>
										@endif
										<br />
										@if( $errors->first('k2_jml_penerima') )
											<span class="help-block text-red">{{ $errors->first('k2_jml_penerima') }}</span>
										@endif
										@if( $errors->first('k2_jml_dana') )
											<span class="help-block text-red">{{ $errors->first('k2_jml_dana') }}</span>
										@endif
										<br />
										@if( $errors->first('k3_jml_penerima') )
											<span class="help-block text-red">{{ $errors->first('k3_jml_penerima') }}</span>
										@endif
										@if( $errors->first('k3_jml_dana') )
											<span class="help-block text-red">{{ $errors->first('k3_jml_dana') }}</span>
										@endif
										<br />
										<br />
										<br />
										@if( $errors->first('anggaran_manajemen') )
											<span class="help-block text-red">{{ $errors->first('anggaran_manajemen') }}</span>
										@endif
										<!-- @include('errors.list') -->
									</div>
								</div>
								<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
				<!-- </div> -->
				<!-- /.panel -->
		<!-- </div> -->
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	@stop
	@section('script')
		{!! Html::script('plugins/input-mask/jquery.inputmask.bundle.js', array('type' => 'text/javascript')) !!}
		<script>
		$(document).ready(function(){

	    //Datemask dd/mm/yyyy
	    // $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
	    //Datemask2 mm/dd/yyyy
	    // $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
	    //Money Euro
	    // $("[data-mask]").inputmask();
			$("#anggaran_1").inputmask('Rp 999.999.999.999',{
				numericInput: true,
				autoUnmask : true,
				removeMaskOnSubmit:true,
			});
			$("#anggaran_2").inputmask('Rp 999.999.999.999',{
				numericInput: true,
				autoUnmask : true,
				removeMaskOnSubmit:true,
			});
			$("#anggaran_3").inputmask('Rp 999.999.999.999',{
				numericInput: true,
				autoUnmask : true,
				removeMaskOnSubmit:true,
			});
			$("#anggaran_4").inputmask('Rp 999.999.999.999',{
				numericInput: true,
				autoUnmask : true,
				removeMaskOnSubmit:true,
			});
		});
		</script>
	@endsection
