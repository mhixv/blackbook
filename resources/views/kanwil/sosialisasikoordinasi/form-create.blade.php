<div class="form-group {{ $errors->has('sosialisasi_pelaksana') ? ' has-error' : '' }}">
	{!! Form::label('pelaksana', 'Pelaksana :') !!}
	{!! Form::text('sosialisasi_pelaksana',null, ['class' => 'form-control', 'placeholder' => 'Siapa yang melaksanakan?']) !!}
	<!-- @if( $errors->first('sosialisasi_pelaksana') )
		<span class="help-block text-red">{{ $errors->first('sosialisasi_pelaksana') }}</span>
	@endif -->
</div>

<div class="form-group {{ $errors->has('sosialisasi_jml_pelaksanaan') ? ' has-error' : '' }}">
	{!! Form::label('sosialisasi_jml_pelaksanaan', 'Jumlah Pelaksanaan :') !!}
	{!! Form::text('sosialisasi_jml_pelaksanaan',null, ['class' => 'form-control', 'placeholder' => 'Berapa kali aktivitas dilaksanakan?']) !!}
	<!-- @if( $errors->first('sosialisasi_jml_Pelaksanaan') )
		<span class="help-block text-red">{{ $errors->first('sosialisasi_jml_pelaksanaan') }}</span>
	@endif -->
</div>

<div class="form-group {{ $errors->has('sosialisasi_waktu_pelaksanaan') ? ' has-error' : '' }}">
	{!! Form::label('waktu_pelaksanaan', 'Waktu Pelaksanaan :') !!}
    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
	{!! Form::text('sosialisasi_waktu_pelaksanaan', null, array('id' => 'tanggal_pelaksanaan', 'class' => 'form-control')) !!}
    </div>
		<!-- @if( $errors->first('sosialisasi_waktu_pelaksanaan') )
			<span class="help-block text-red">{{ $errors->first('sosialisasi_waktu_pelaksanaan') }}</span>
		@endif -->
</div>

<div class="form-group {{ $errors->has('sosialisasi_sumber_anggaran') ? ' has-error' : '' }}">
	{!! Form::label('sumber_anggaran', 'Sumber Anggaran :') !!}
	{!! Form::text('sosialisasi_sumber_anggaran',null, ['class' => 'form-control', 'placeholder' => 'Anggaran bersumber dari mana saja?']) !!}
	@if( $errors->first('sosialisasi_sumber_anggaran') )
		<span class="help-block text-red">{{ $errors->first('sosialisasi_sumber_anggaran') }}</span>
	@endif
</div>

<div class="form-group {{ $errors->has('sosialisasi_jml_anggaran') ? ' has-error' : '' }}">
	{!! Form::label('jml_anggaran', 'Jumlah Anggaran :') !!}
	{!! Form::text('sosialisasi_jml_anggaran',null, ['class' => 'form-control', 'placeholder' => 'Berapa Anggaran yang digunakan Rp ___.___.___.___','id' => 'anggaran_1']) !!}
	@if( $errors->first('sosialisasi_jml_anggaran') )
		<span class="help-block text-red">{{ $errors->first('sosialisasi_jml_anggaran') }}</span>
	@endif
</div>

<div class="form-group {{ $errors->has('sosialisasi_deskripsi') ? ' has-error' : '' }}">
	{!! Form::label('deskripsi_singkat_aktifitas', 'Deskripsi Singkat aktifitas :') !!}

	{!! Form::textarea('sosialisasi_deskripsi',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Deskripsi singkat aktifitas']) !!}
	@if( $errors->first('sosialisasi_deskripsi') )
		<span class="help-block text-red">{{ $errors->first('sosialisasi_deskripsi') }}</span>
	@endif
</div>

<div class="form-group {{ $errors->has('sosialisasi_kendala') ? ' has-error' : '' }}">
	{!! Form::label('kendala', 'Kendala :') !!}

	{!! Form::textarea('sosialisasi_kendala',null, ['class' => 'form-control','rows'=> 4 ,'placeholder' => 'Jelaskan kendala-kendalah yang dihadapi!']) !!}
	@if( $errors->first('sosialisasi_kendala') )
		<span class="help-block text-red">{{ $errors->first('sosialisasi_kendala') }}</span>
	@endif
</div>

<div class="form-group">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
