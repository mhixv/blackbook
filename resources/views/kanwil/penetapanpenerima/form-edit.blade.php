<div class="form-group {{ $errors->has('penetapan_lokasi') ? ' has-error' : '' }}">
	{!! Form::label('lokasi_pelaksanaan', 'Lokasi Pelaksanaan :') !!}
	{!! Form::text('penetapan_lokasi',null, ['class' => 'form-control', 'placeholder' => 'Dimana saja aktivitas dilaksanakan?', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('pendataan_pelaksana') ? ' has-error' : '' }}">
	{!! Form::label('penetapan_waktu', 'Waktu Pelaksanaan :') !!}
    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
	{!! Form::text('penetapan_waktu',null, ['id'=> 'tanggal_pelaksanaan', 'class' => 'form-control', 'readonly' => $disabled]) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('penetapan_jml_dana_kat_1') ? ' has-error' : '' }} {{ $errors->has('penetapan_jml_kat_1') ? ' has-error' : '' }}">
<label class="control-label">Data Penetapan dan Jumlah Dana Manfaat Per-Kategori :</label>

<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 1</div>
	<!-- {!! Form::label('Katagori 1', 'KATEGORI 1 :') !!} -->
	{!! Form::text('penetapan_jml_kat_1',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat', 'readonly' => $disabled]) !!}
	{!! Form::text('penetapan_jml_dana_kat_1',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_1', 'readonly' => $disabled]) !!}
</div></div>

<div class="form-group {{ $errors->has('penetapan_jml_dana_kat_2') ? ' has-error' : '' }} {{ $errors->has('penetapan_jml_kat_2') ? ' has-error' : '' }}">
<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 2</div>
	<!-- {!! Form::label('Katagori 2', 'Kategori 2 :') !!} -->
	{!! Form::text('penetapan_jml_kat_2',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat', 'readonly' => $disabled]) !!}
	{!! Form::text('penetapan_jml_dana_kat_2',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_2', 'readonly' => $disabled]) !!}
</div></div>

<div class="form-group {{ $errors->has('penetapan_jml_dana_kat_3') ? ' has-error' : '' }} {{ $errors->has('penetapan_jml_kat_3') ? ' has-error' : '' }}">
<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 3</div>
	<!-- {!! Form::label('Katagori 3', 'Kategori 3 :') !!} -->
	{!! Form::text('penetapan_jml_kat_3',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat', 'readonly' => $disabled]) !!}
	{!! Form::text('penetapan_jml_dana_kat_3',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_3', 'readonly' => $disabled]) !!}
</div></div>

<div class="form-group {{ $errors->has('penetapan_deskripsi') ? ' has-error' : '' }}">
	{!! Form::label('penetapan_deskripsi', 'Deskripsi Singkat Aktifitas :') !!}
<!-- <div class="form-group"> -->
	{!! Form::textarea('penetapan_deskripsi',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Deskripsi singkat aktifitas', 'readonly' => $disabled]) !!}
</div>

<div class="form-group {{ $errors->has('penetapan_kendala') ? ' has-error' : '' }}">
	{!! Form::label('penetapan_kendala', 'Kendala :') !!}
<!-- <div class="form-group"> -->
	{!! Form::textarea('penetapan_kendala',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Jelaskan kendala-kendalah yang dihadapi!', 'readonly' => $disabled]) !!}
</div>

@if($pen_pen->status == 3)
<div class="form-group">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
@endif
@if($pen_pen->status == 2)
	<div class="alert alert-danger">
		<h4 style="text-align:center">Data Penetapan Penerima KIP dalam proses Verifikasi Pusat</h4>
	</div>
@endif
@if($pen_pen->status == 3)
	<div class="alert alert-danger">
		<h4 style="text-align:center">Data Penetapan Penerima KIP diminta revisi oleh Pusat</h4>
	</div>
@endif
@if($pen_pen->status == 4)
	<div class="alert alert-info">
		<h4 style="text-align:center">Data Penetapan Penerima KIP telah disetujui Pusat</h4>
	</div>
@endif
