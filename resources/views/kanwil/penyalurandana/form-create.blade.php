<div class="form-group {{ $errors->has('penyaluran_lokasi') ? ' has-error' : '' }}">
	{!! Form::label('penyaluran_lokasi', 'Lokasi Penyaluran :') !!}
	{!! Form::text('penyaluran_lokasi',null, ['class' => 'form-control', 'placeholder' => 'Dimana saja aktivitas dilaksanakan?']) !!}
</div>

<div class="form-group {{ $errors->has('penyaluran_waktu') ? ' has-error' : '' }}">
	{!! Form::label('penyaluran_waktu', 'Waktu Pelaksanaan :') !!}
    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
	{!! Form::text('penyaluran_waktu',null, ['id'=> 'tanggal_pelaksanaan', 'class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('penyaluran_dana_kat_1') ? ' has-error' : '' }} {{ $errors->has('penyaluran_jml_kat_1') ? ' has-error' : '' }}">
<label class="control-label">Jumlah Penerima dan Dana Manfaat Per-Kategori :</label>

<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 1</div>
	<!-- {!! Form::label('Katagori 1', 'KATEGORI 1 :') !!} -->
	{!! Form::text('penyaluran_jml_kat_1',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat']) !!}
	{!! Form::text('penyaluran_dana_kat_1',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_1']) !!}
</div></div>

<div class="form-group {{ $errors->has('penyaluran_dana_kat_2') ? ' has-error' : '' }} {{ $errors->has('penyaluran_jml_kat_2') ? ' has-error' : '' }}">
<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 2</div>
	<!-- {!! Form::label('Katagori 2', 'Kategori 2 :') !!} -->
	{!! Form::text('penyaluran_jml_kat_2',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat']) !!}
	{!! Form::text('penyaluran_dana_kat_2',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_2']) !!}
</div></div>

<div class="form-group {{ $errors->has('penyaluran_dana_kat_3') ? ' has-error' : '' }} {{ $errors->has('penyaluran_jml_kat_3') ? ' has-error' : '' }}">
<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 3</div>
	<!-- {!! Form::label('Katagori 3', 'Kategori 3 :') !!} -->
	{!! Form::text('penyaluran_jml_kat_3',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat']) !!}
	{!! Form::text('penyaluran_dana_kat_3',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_3']) !!}
</div></div>

<div class="form-group {{ $errors->has('penyaluran_deskripsi') ? ' has-error' : '' }}">
	{!! Form::label('penyaluran_deskripsi', 'Deskripsi Singkat Aktifitas :') !!}
<!-- <div class="form-group"> -->
	{!! Form::textarea('penyaluran_deskripsi',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Deskripsi singkat aktifitas']) !!}
</div>

<div class="form-group {{ $errors->has('penyaluran_kendala') ? ' has-error' : '' }}">
	{!! Form::label('penyaluran_kendala', 'Kendala :') !!}
<!-- <div class="form-group"> -->
	{!! Form::textarea('penyaluran_kendala',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Jelaskan kendala-kendalah yang dihadapi!']) !!}
</div>

<div class="form-group">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
