<div class="form-group {{ $errors->has('distribusi_pelaksana') ? ' has-error' : '' }}">
	{!! Form::label('distribusi_pelaksana', 'Lokasi Penyaluran :') !!}
	{!! Form::text('distribusi_pelaksana',null, ['class' => 'form-control', 'placeholder' => 'Siapa yang melaksanakan?']) !!}
</div>
<div class="form-group {{ $errors->has('distribusi_jml') ? ' has-error' : '' }}">
	{!! Form::label('distribusi_jml', 'Jumlah Pelaksanaan :') !!}
	{!! Form::text('distribusi_jml',null, ['class' => 'form-control', 'placeholder' => 'Berapa kali aktifitas dijalankan?']) !!}
</div>
<div class="form-group {{ $errors->has('distribusi_lokasi') ? ' has-error' : '' }}">
	{!! Form::label('distribusi_lokasi', 'Lokasi Pelaksanaan :') !!}
	{!! Form::text('distribusi_lokasi',null, ['class' => 'form-control', 'placeholder' => 'Diaman saja aktifitas dijalankan?']) !!}
</div>

<div class="form-group {{ $errors->has('distribusi_waktu') ? ' has-error' : '' }}">
	{!! Form::label('distribusi_waktu', 'Waktu Pelaksanaan :') !!}
    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
	{!! Form::text('distribusi_waktu',null, ['id'=> 'tanggal_pelaksanaan', 'class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('distribusi_dana_kat_1') ? ' has-error' : '' }} {{ $errors->has('distribusi_jml_kat_1') ? ' has-error' : '' }}">
<label class="control-label">Jumlah Penerima dan Dana Manfaat Per-Kategori :</label>

<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 1</div>
	<!-- {!! Form::label('Katagori 1', 'KATEGORI 1 :') !!} -->
	{!! Form::text('distribusi_jml_kat_1',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat']) !!}
	{!! Form::text('distribusi_dana_kat_1',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_1']) !!}
</div></div>

<div class="form-group {{ $errors->has('distribusi_dana_kat_2') ? ' has-error' : '' }} {{ $errors->has('distribusi_jml_kat_2') ? ' has-error' : '' }}">
<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 2</div>
	<!-- {!! Form::label('Katagori 2', 'Kategori 2 :') !!} -->
	{!! Form::text('distribusi_jml_kat_2',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat']) !!}
	{!! Form::text('distribusi_dana_kat_2',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_2']) !!}
</div></div>

<div class="form-group {{ $errors->has('distribusi_dana_kat_3') ? ' has-error' : '' }} {{ $errors->has('distribusi_jml_kat_3') ? ' has-error' : '' }}">
<div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1">KATEGORI 3</div>
	<!-- {!! Form::label('Katagori 3', 'Kategori 3 :') !!} -->
	{!! Form::text('distribusi_jml_kat_3',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Penerima Manfaat']) !!}
	{!! Form::text('distribusi_dana_kat_3',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Dana Manfaat Rp ___.___.___.___','id' => 'anggaran_3']) !!}
</div></div>

<div class="form-group {{ $errors->has('distribusi_deskripsi') ? ' has-error' : '' }}">
	{!! Form::label('distribusi_deskripsi', 'Deskripsi Singkat Aktifitas :') !!}
<!-- <div class="form-group"> -->
	{!! Form::textarea('distribusi_deskripsi',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Deskripsi singkat aktifitas']) !!}
</div>

<div class="form-group {{ $errors->has('penyaluran_kendala') ? ' has-error' : '' }}">
	{!! Form::label('distribusi_kendala', 'Kendala :') !!}
<!-- <div class="form-group"> -->
	{!! Form::textarea('distribusi_kendala',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Jelaskan kendala-kendalah yang dihadapi!']) !!}
</div>

<div class="form-group">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
@endif
