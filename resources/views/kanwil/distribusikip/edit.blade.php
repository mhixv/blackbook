@extends('kanwil/kanwil_index')

@section('content-header')
	      <h1>
	        Distribusi KIP
	        <small>Distribusi KIP</small>
	      </h1>
@endsection
@section('content')
<div class="row">
	<!-- <div class="box box-info"> -->
		<div class="col-lg-7">
				<div class="box box-info">
					<div class="box-header with-border">
			      <h3 class="box-title">Data Distribusi KIP :</h3>
			    </div>
						<div class="box-body">
								<div class="row">
									<div class="col-lg-12">
										{!! Form::model($distribusi,['method' => 'PATCH','files'=>true,'url' => ['kanwil/distribusi',$distribusi->id]]) !!}
											@include('kanwil.distribusikip.form-edit', ['text' => 'Kirim Notifikasi Validasi'])
										<!-- {!! Form::close() !!} -->

											<!-- <div class="row">
												<div class="col-md-12">
													@include('errors.list')
												</div>
											</div> -->
									</div>
								</div>
								<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
		<div class="col-lg-5">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Status Data :</h3>
					</div>
						<div class="box-body">
							@if ($distribusi->status == 2)
								<b style="color : orange"> Menunggu Verifikasi.</b>
							@endif
							@if ($distribusi->status == 3)
								<b style="color : red"> Revisi Pusat, Silahkan Edit Data dan Kirim Kembali</b>
							@endif
							@if ($distribusi->status == 4)
								<b style="color : green"> Selesai.</b>
							@endif
					</div>
				</div>
				<div class="box box-danger">
					<div class="box-header with-border">
			      <h3 class="box-title">Informasi Feedback Dari Pusat :</h3>
			    </div>
						<div class="box-body">
              <p>{{ $distribusi->feedback }}</p>
          </div>
        </div>
				{!! Form::close() !!}

				<div class="box box-info">
					<div class="box-header with-border">
			      <h3 class="box-title">Upload Data Pendukung :</h3>
			    </div>
						<div class="box-body">
							@if ($distribusi->status == 3)
							{!! Form::model($distribusi,['method' => 'PATCH','files'=>true,'url' => ['kanwil/distribusi_update_upload',$distribusi->id]]) !!}
              <div class="form-group">
                {!! Form::label('Dokumen_Pendukung', 'Dokumen Pendukung :') !!}
                <!-- {!! Form::file('dokumen_pendukung',null, ['class' => 'form-control']) !!} -->
								<input type="file" name="dokumen_pendukung[]" multiple />
								<p class="help-block">Anda bisa mengunggah lebih dari satu file.<br/> Jika mengunggah file yang sama, harap menyertakan revisinya.</p>
              </div>
							<div class="form-group">
								{!! Form::submit('Upload', ['class' => 'btn btn-primary form-control']) !!}
							</div>
							@else
								@if ($distribusi->status == 2)
									<b style="color : orange"> Menunggu Verifikasi.</b>
								@endif
								@if ($distribusi->status == 4)
									<b style="color : green"> Selesai.</b>
								@endif
							@endif
							{!! Form::close() !!}
							<hr \>
              <div class="form-group">
                {!! Form::label('list_Dokumen_Pendukung', 'Daftar Dokumen Pendukung :') !!}
										@foreach ($list_files as $file)
										<ul>
												<li>
												<a href="{{ URL::to('/download/distribusi/'.$file->nama_file) }}">{{ $file->nama_file }}</a>
											</li>
										</ul>
										@endforeach
              </div>
          </div>
        </div>
				<div class="row">
					<div class="col-md-12">
						@include('errors.list')
					</div>
				</div>
    </div>
	<!-- </div> -->
</div>
<!-- /.row -->
@stop


@section('script')

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

{!! Html::script('plugins/input-mask/jquery.inputmask.bundle.js', array('type' => 'text/javascript')) !!}
<script>
$(document).ready(function(){
	$("#anggaran_1").inputmask('Rp 999.999.999.999',{
		numericInput: true,
		autoUnmask : true,
		removeMaskOnSubmit:true,
	});
	$("#anggaran_2").inputmask('Rp 999.999.999.999',{
		numericInput: true,
		autoUnmask : true,
		removeMaskOnSubmit:true,
	});
	$("#anggaran_3").inputmask('Rp 999.999.999.999',{
		numericInput: true,
		autoUnmask : true,
		removeMaskOnSubmit:true,
	});
});
</script>

<script>
$(function() {
  //$( "#tgl_lahir" ).datepicker();
  $('#tanggal_pelaksanaan').datepicker({ dateFormat: "yy-mm-dd", changeMonth: true,
          changeYear: true, yearRange: '2010:2020', defaultDate: ''
      });
});
</script>


<script type="text/javascript">
$(function() {
      //  $( "#tgl_lahir" ).datepicker( "option", "dateFormat", 'yy-mm-dd');
});

</script>

@endsection
