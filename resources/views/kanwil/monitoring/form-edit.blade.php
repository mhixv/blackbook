<div class="form-group col-md-12">
	{!! Form::label('monitoring_pelaksana', 'Pelaksana Monitoring :') !!}
	{!! Form::text('monitoring_pelaksana',null, ['class' => 'form-control', 'placeholder' => 'Siapa yang melaksanakan?', 'readonly' => $disabled]) !!}
</div>
<div class="form-group col-md-12">
	{!! Form::label('monitoring_jml', 'Jumlah Pelaksanaan :') !!}
	{!! Form::text('monitoring_jml',null, ['class' => 'form-control', 'placeholder' => 'Berapa kali aktifitas dilakukan?', 'readonly' => $disabled]) !!}
</div>
<div class="form-group col-md-12">
	{!! Form::label('monitoring_lokasi', 'Lokasi Pelaksanaan :') !!}
	{!! Form::text('monitoring_lokasi',null, ['class' => 'form-control', 'placeholder' => 'Dimana saja aktifitas dilakukan', 'readonly' => $disabled]) !!}
</div>
<div class="form-group col-md-12">
	{!! Form::label('monitoring_waktu', 'Waktu Pelaksanaan :') !!}
    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
	{!! Form::text('monitoring_waktu',null, ['id'=> 'tanggal_pelaksanaan', 'class' => 'form-control', 'readonly' => $disabled]) !!}
    </div>
</div>
<div class="form-group col-md-12">
	{!! Form::label('monitoring_sumber_anggaran', 'Sumber Anggaran :') !!}
	{!! Form::text('monitoring_sumber_anggaran',null, ['class' => 'form-control', 'placeholder' => 'Jumlah Anggaran Layanan Manajemen PIP', 'readonly' => $disabled]) !!}
</div>
<div class="form-group col-md-12">
	{!! Form::label('monitoring_jml_anggaran', 'Jumlah Anggaran :') !!}
	{!! Form::text('monitoring_jml_anggaran',null, ['class' => 'form-control', 'placeholder' => 'Berapa anggaran yang digunakan Rp ___.___.___.___','id' => 'anggaran_1', 'readonly' => $disabled]) !!}
</div>
<div class="form-group col-md-12">
	{!! Form::label('monitoringi_deskripsi', 'Deskripsi Singkat Aktifitas :') !!}
	{!! Form::textarea('monitoring_deskripsi',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Deskripsi singkat aktifitas', 'readonly' => $disabled]) !!}
</div>

<div class="form-group col-md-12">
	{!! Form::label('monitoring_kendala', 'Kendala :') !!}
	{!! Form::textarea('monitoring_kendala',null, ['class' => 'form-control','rows'=> 4, 'placeholder' => 'Jelaskan kendala-kendalah yang dihadapi!', 'readonly' => $disabled]) !!}
</div>

@if($monitoring->status == 3)
<div class="form-group col-md-12">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
@endif
@if($monitoring->status == 2)
  <div class="form-group col-md-12">
	<div class="alert alert-danger col-md-12">
		<h4 style="text-align:center">Data Monitoring KIP dalam proses Verifikasi Pusat</h4>
	</div>
	</div>
@endif
@if($monitoring->status == 3)
	<div class="form-group col-md-12">
	<div class="alert alert-danger col-md-12">
		<h4 style="text-align:center">Data Monitoring KIP diminta revisi oleh Pusat</h4>
	</div>
	</div>
@endif
@if($monitoring->status == 4)
	<div class="form-group col-md-12">
	<div class="alert alert-danger col-md-12">
		<h4 style="text-align:center">Data Monitoring KIP telah disetujui Pusat</h4>
	</div>
	</div>
@endif
