<html>
<head>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> -->
<style>

body {

  padding-top: -80px;
  /*background: rgb(204,204,204);*/
}
table.style1 {

    border: 1px solid black;
    border-collapse: collapse;
    font-size: 12px;
    font-weight: ;
}
table.style2  {

    border: 0px solid black;
    border-collapse: collapse;
    font-size: 12px;
    font-weight: ;
}

div.page1
{
    page-break-after: always;
    page-break-inside: avoid;
    margin-top: 80px;

}

div.page
{
    margin-top: 80px;
}

</style>
</head>
<h4>
<?php $total_baris = count($pelanggaran);
$n = intval($total_baris/20);
//$n=2;
//echo "Total Baris: ". $total_baris;
?>

@for ($a = 0; $a <= $n; $a++)

@if($a==0)
  <div class="page1">
@else
  <div class="page">
@endif

<?php

$batas_bawah = $a*20;
//echo "Batas Bawah: ". $batas_bawah;

$batas_atas = $batas_bawah+20;

if($batas_atas > $total_baris)
  $batas_atas = $total_baris;
//echo "Batas Atas: ". $batas_atas;
?>
<br />
  <table border="0" class="style2" style="font-size:12px;">
  	<tr>
  		<td width="100px">
  			<center>
  			<img src="img/logo/kemenag.png" height="75px" width="75px">
  		</center>
  		</td>
  		<td width="480px" style="font-size: 15px">
  		<center>
        	KEMENTERIAN AGAMA REPUBLIK INDONESIA <br />
        	KANTOR KEMENTERIAN AGAMA KABUPATEN SIAK<br />
        	MADRASAH ALIYAH NEGERI INSAN CENDEKIA SIAK<br />
        	Jl. Pemda KM. 10, Desa Perawang Barat, Kec. Tualang, Kab. Siak, Prov. Riau
        </center>
  		</td>
  		<td width="100px">
  			<center>
  			<img src="img/logo/ic.png" height="75px" width="75px">
  		</center>
  		</td>
  	</tr>
  	<hr />
  </table>
	<center>
		<br />
		<strong>DAFTAR PELANGGARAN SISWA<br>
		MAN INSAN CENDEKIA SIAK TAHUN  {{ date("Y") }}</strong><br><br>
	</center>

	<table border="0" class="style2" style="font-size:14px;font-weight: bold">
  	<tr>
  		<td width="50px">
  			<left>
  			NAMA</left>
  		</td>
  		<td width="10px" style="font-size: 14px">
  			<center>:</center>
  		</td>
  		<td width="100px">
  			<left>{{ strtoupper($siswa->nama_siswa) }}</left>
  		</td>
  	</tr>
  	<tr>
  		<td width="50px">
  			<left>
  			KELAS</left>
  		</td>
  		<td width="10px" style="font-size: 14px">
  			<center>:</center>
  		</td>
  		<td width="100px">
  			<left>{{ strtoupper($siswa->kelas->nama_kelas) }}</left>
  		</td>
  	</tr>
  </table>
  <br />
</h4>

<table border="1" align="center" cellpadding="3" class="style1">
	<thead>
    <tr>
		<th width="20px">
			<center>
				NO
			</center>
		</th>
		<th width="100px">
			<center>
				TANGGAL
			</center>
		</th>
		<th width="195px">
			<center>
				NAMA ATURAN
			</center>
		</th>
		<th width="40px">
			<center>
				POIN
			</center>
		</th>
		<th width="195px">
			<center>
				DESKRIPSI
			</center>
		</th>
		<th width="70px">
			<center>
				KETERANGAN
			</center>
		</th>
  </tr>
	</thead>
  <tbody>
  
  @for ($i = $batas_bawah; $i < $batas_atas; $i++)
  <tr>
		<td>
			<center>
				{{ $row++ }}
			</center>
		</td>
		<td>
			<left>
			{{ $pelanggaran[$i]->tanggal }}
			</left>
		</td>
		<td>
			<left>
        	{{ $pelanggaran[$i]->nama_aturan }}
			</left>
		</td>
		<td>
			<center>
        	{{ $pelanggaran[$i]->poin }}
			</center>
		</td>
		<td>
			<left>
				{{ $pelanggaran[$i]->keterangan }}
			</left>
		</td>
		<td>
			
		</td>
  </tr>
  @endfor
  </tbody>
</table>

@if($a == $n)

<br />

<table border="0" class="style2">
	<tr>
	    <td width="50px">
			&nbsp;
		</td>
		<td width="200px">
			Mengetahui,<br>
		    Guru Asuh <br>
		</td>
		<td width="230px">
			&nbsp;
		</td>
		<td width="200px">
		    Siak, {{ $tanggal }}<br>
		    Waka. Kesiswaan <br>
		</td>
 	</tr>
 	<tr><td colspan="4">&nbsp;</td></tr><tr><td colspan="4">&nbsp;</td></tr><tr><td colspan="4">&nbsp;</td></tr>
	<tr>
	    <td width="50px">
			&nbsp;
		</td>
		<td width="200px">
			{{ $siswa->guru_asuh->nama_guru }}
		</td>
		<td width="250px">
			&nbsp;
		</td>
	    <td width="200px">
	      <strong>Sadriadi, S.Pd.I</strong>
		</td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr><tr><td colspan="4">&nbsp;</td></tr>
 	<tr>
		<td colspan="4">
			<center>
				Mengetahui,<br />
				Kepala Madrasah
			</center>
		</td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr><tr><td colspan="4">&nbsp;</td></tr><tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td colspan="4">
			<center>
			 	Dra. Hayatirruh, M.Ed<br />
			 	NIP. 19680112 199302 2 003
			</center>
		</td>
	</tr>
</table>


@endif

</div>
@endfor


</html>
