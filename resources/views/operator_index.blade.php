<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aplikasi Catatan Pelanggaran Siswa | MAN Insan Cendekia Siak</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <<!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('ionicons-2.0.1/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

  <link href="{{ asset('img/favicon.ico') }}" rel="shortcut icon">

  @yield('css')
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<!-- <body class="hold-transition skin-green sidebar-collapse sidebar-mini"> -->
<body class="hold-transition skin-blue">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="{!! URL::to('/admin') !!}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>IC</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ICSIAK</b> - Black Books</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="hidden-xs" style="font-family:'Source Sans Pro';font-size: 15px;">&nbsp;&nbsp;MENU OPERATOR</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <a href="#" class="navbar-custom-menu-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <ul class="nav navbar-nav">
          <li class="user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#">
              <i class="fa fa-calendar"></i>
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Tahun Ajaran Aktif : {{ $tahun_ajaran_aktif }}</span>
            </a>
          </li>
          <!-- User Account Menu -->
          <li class="user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#">
              <i class="fa fa-user"></i>
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"> {!! Auth::user()->nama !!} (Operator)</span>
            </a>
          </li>
          <li class="user user-menu">
            <a href="{{ URL::to('/logout') }}">
              <i class="fa fa-sign-out"></i>
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Keluar</span>
            </a>
          </li>

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->


  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="treeview">
          <a href="{!! URL::to('/operator') !!}">
            <i class="fa fa-pie-chart"></i>
            <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="{!! URL::to('operator/pelanggaran') !!}">
            <i class="fa fa-wrench"></i>
            <span>Pelanggaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="{!! URL::to('operator/hukuman') !!}">
            <i class="fa fa-wrench"></i>
            <span>Sangsi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="{!! URL::to('operator/laporan_pelanggaran') !!}">
            <i class="fa fa-line-chart"></i>
            <span>Laporan Pelanggaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="{!! URL::to('operator/laporan_hukuman') !!}">
            <i class="fa fa-line-chart"></i>
            <span>Laporan Hukuman</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @if (session()->has('pesan.message'))
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box-body">
          <div class="alert alert-{{ session()->get('pesan.level') }} alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  {!! session()->get('pesan.message') !!}
          </div>
          <!-- <div class="alert alert-{{ session()->get('pesan.level') }} fade in top10 bottom7">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('pesan.message') !!}
          </div> -->
        </div>
      </div>
    </div>
    @endif
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('content-header')
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Content will be place right here! -->
      @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Kementerian Agama - RI
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} <a href="#">Information and Communication Technology (ICT) Center © MAN Insan Cendekia Siak</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

<script>
   $(document).on('ready', function() {
        var url = window.location;
        // Will only work if string in href matches with location
        $('.treeview a[href="' + url + '"]').parent().addClass('active');
        $('.treeview-menu li a[href="' + url + '"]').parent().addClass('active');
        $('.treeview-menu li a[href="' + url + '"]').parent().parent().parent().addClass('active');
        // Will also work for relative and absolute hrefs
        // $('.treeview-menu li a').filter(function() {
        //     return this.href == url;
        // }).parent().parent().parent().addClass('active');
    });
</script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/app.min.js') }}"></script>

@yield('script')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
