<div class="form-group">
	{!! Form::label('nama_tahun', 'Nama Tahun :') !!}
	{!! Form::text('nama_tahun',null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-4 col-md-offset-4">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
