@extends('admin_index')

@section('content-header')
	      <h1>Merubah Tahun Ajaran</h1>
@endsection

@section('content')
<div class="row">
		<div class="col-lg-6">
				<div class="box box-primary">
						<div class="box-header with-border">Merubah Tahun Ajaran</div>
						<div class="box-body">
								<div class="row">
									<div class="col-lg-12">
										{!! Form::model($ta,['method' => 'PATCH','url' => ['admin/tahunajaran',$ta->id]]) !!}
											@include ('tahunajaran.formedit', ['text' => 'Simpan'])
										{!! Form::close() !!}

										<div class="row">
											<div class="col-md-12">
												@include('errors.list')
											</div>
										</div>
									</div>
								</div>
								<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop
