<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Program Indonesia Pintar - Pendidikan Keagamaan Islam | Kementerian Agama RI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-green.min.css') }}">

  <!-- Custom PIP CSS -->
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

  <link href="{{ asset('img/favicon.ico') }}" rel="shortcut icon">

  @yield('css')

  <style>
      body {
          /*font-family: 'Lato';*/
          background-color: #444;
          background: url("{{asset('/img/background.jpg')}}") no-repeat center center fixed;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
      }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="skin-green">

  <!-- <div class="wrapper"> -->

    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="{!! URL::to('/') !!}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>P</b>IP</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>PIP</b> - PAKIS</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <a href="#" class="navbar-custom-menu-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <form class="nav navbar-nav navbar-form" role="form" method="POST" action="{{ url('/login') }}">
              {{ csrf_field() }}
              <div class="form-group">
                  <input type="text" class="form-control" name="email" placeholder="Alamat E-Mail">
                  @if ($errors->has('email'))
                  <!-- <ul class="nav navbar-nav"> -->
                    <span>
                        <strong>{{ $errors->first('email') }}</strong>&nbsp;
                    </span>
                  <!-- </ul> -->
                  @endif
              </div>
              <div class="form-group">
                  <input type="password" class="form-control" name="password" placeholder="Password">
                  @if ($errors->has('password'))
                  <!-- <ul class="nav navbar-nav"> -->
                    <span>
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  <!-- </ul> -->
                  @endif
              </div>
              <button type="submit" class="btn btn-default">Masuk</button>
          </form>
          <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">MENU <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ URL::to('/lapor') }}">Layanan Pengaduan</a>
                    </li>
                    <li><a href="{{ URL::to('/download/manual/Manual Aplikasi.pdf') }}">Petunjuk Teknis PIP Pendidikan Keagamaan Islam</a>
                    </li>
                    <!-- <li class="divider"></li>
                    <li><a href="{{ URL::to('/login') }}">Halaman Login</a>
                    </li> -->
                </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <!-- <div class="content-wrapper"> -->

      <!-- Main content -->
      <section class="content">
        <div class="container">
          @if (session()->has('pesan.message'))
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="box-body">
                <div class="alert alert-{{ session()->get('pesan.level') }} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                        {!! session()->get('pesan.message') !!}
                </div>
                <!-- <div class="alert alert-{{ session()->get('pesan.level') }} fade in top10 bottom7">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {!! session()->get('pesan.message') !!}
                </div> -->
              </div>
            </div>
          </div>
          @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Lapor / Pengaduan !</h3>
                        </div>
                        <div class="box-body">
                          {!! Form::open(['url' => 'lapor','files' =>true]) !!}
                            <div class="form-group {{ ! $errors->first('nama_lengkap') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('nama_lengkap', 'Nama Lengkap') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::text('nama_lengkap',null ,['class' => 'form-control', 'placeholder' => 'Nama Lengkap']) !!}
                                </div>
                                @if( $errors->first('nama_lengkap') )
                                  <span class="help-block text-danger">{{ $errors->first('nama_lengkap') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('alamat_email') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('alamat_email', 'Alamat E-mail') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::text('alamat_email',null ,['class' => 'form-control', 'placeholder' => 'Alamat E-Mail']) !!}
                                </div>
                                @if( $errors->first('alamat_email') )
                                  <span class="help-block text-danger">{{ $errors->first('alamat_email') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('alamat') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('alamat', 'Alamat Rumah') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::text('alamat',null ,['class' => 'form-control', 'placeholder' => 'Alamat Rumah']) !!}
                                </div>
                                @if( $errors->first('alamat') )
                                  <span class="help-block text-danger">{{ $errors->first('alamat') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('provinsi_id') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('provinsi_id', 'Provinsi') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::select('provinsi_id',[0 => 'Pilih Provinsi']+$provinsi,0 ,['class' => 'form-control']) !!}
                                </div>
                                @if( $errors->first('provinsi_id') )
                                  <span class="help-block text-danger">{{ $errors->first('provinsi_id') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('no_telepon') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('no_telepon', 'Nomor Telepon') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::text('no_telepon',null ,['class' => 'form-control', 'placeholder' => 'Nomor Telepon']) !!}
                                </div>
                                @if( $errors->first('no_telepon') )
                                  <span class="help-block text-danger">{{ $errors->first('no_telepon') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('no_ktp') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('no_ktp', 'Nomor KTP') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::text('no_ktp',null ,['class' => 'form-control', 'placeholder' => 'Nomor KTP']) !!}
                                </div>
                                @if( $errors->first('no_ktp') )
                                  <span class="help-block text-danger">{{ $errors->first('no_ktp') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('judul_pengaduan') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('judul_pengaduan', 'Judul Pengaduan') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::text('judul_pengaduan',null ,['class' => 'form-control', 'placeholder' => 'Judul Pengaduan']) !!}
                                </div>
                                @if( $errors->first('judul_pengaduan') )
                                  <span class="help-block text-danger">{{ $errors->first('judul_pengaduan') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('deskripsi_pengaduan') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('deskripsi_pengaduan', 'Deskripsi Pengaduan') !!}
                                </div>
                                <div class="col-md-5">
                                {!! Form::textarea('deskripsi_pengaduan',null ,['class' => 'form-control', 'placeholder' => 'Deskripsi Pengaduan']) !!}
                                </div>
                                @if( $errors->first('deskripsi_pengaduan') )
                                  <span class="help-block text-danger">{{ $errors->first('deskripsi_pengaduan') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group {{ ! $errors->first('dokumen_pengaduan') ? '': 'has-error' }}">
                              <div class="row">
                                <div class="col-md-3">
                                {!! Form::label('dokumen_pengaduan', 'Dokumen Bukti Pengaduan:') !!}
                                </div>
                                <div class="col-md-5">
                                  <input type="file" name="dokumen_pengaduan" id="dokumen_pengaduan" />
                  								<p class="help-block">Optional. Dalam Bentuk Image (jpeg, png, bmp, gif, atau svg). Tidak lebih dari 2MB.</p>
                                </div>
                                @if( $errors->first('dokumen_pengaduan') )
                                  <span class="help-block text-danger">{{ $errors->first('dokumen_pengaduan') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-5">
                                 {!! Form::submit("Kirim Pengaduan", ['class' => 'btn btn-primary form-control']) !!}
                                </div>
                              </div>
                            </div>
                          {!! Form::close() !!}
                        </div>

                        <div class="box-footer">
                          <p style="text-align:center;">Direktorat Pendidikan Diniyah dan Pondok Pesantren © Ditjen Pendidikan Islam Kementerian Agama RI<p>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
            </div>
        </div>
      </section>
<!-- /.content -->
<!-- </div> -->
<!-- /.content-wrapper -->
<!-- </div> -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/app.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
Both of these plugins are recommended to enhance the
user experience. Slimscroll is required when using the
fixed layout. -->
</body>
</html>
