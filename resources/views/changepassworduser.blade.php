@extends('admin_index')

@section('content-header')
		<h1 class="page-header top15">Reset Password Pengguna : <strong>{{ $pengguna->nama}}</strong</h1>
@endsection
@section('content')
<div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default">
						<div class="panel-heading">Reset Password Pengguna</div>
						<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/changepassworduser/'.$pengguna->id) }}">
	                      {{ csrf_field() }}
												<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	                          <label for="password" class="col-md-4 control-label">Password Baru</label>

	                          <div class="col-md-6">
                                {!! Form::password('password', [ 'class'=> 'form-control','id'=>'password', 'placeholder'=> "Password Baru"]) !!}
	                              <!-- <input id="Password_Baru" type="password" placeholder="Password Baru" class="form-control" name="Password_Baru"> -->

	                              @if ($errors->has('password'))
	                                  <span class="help-block">
	                                      <strong>{{ $errors->first('password') }}</strong>
	                                  </span>
	                              @endif
	                          </div>
	                      </div>
												<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
	                          <label for="password_confirmation" class="col-md-4 control-label">Konfirmasi Password</label>

	                          <div class="col-md-6">
                              {!! Form::password('password_confirmation', [ 'class'=> 'form-control','id'=>'password']) !!}
	                              <!-- <input id="Konfirmasi_Password" type="password" placeholder="Konfirmasi Password" class="form-control" name="Konfirmasi_Password"> -->

	                              @if ($errors->has('password_confirmation'))
	                                  <span class="help-block">
	                                      <strong>{{ $errors->first('password_confirmation') }}</strong>
	                                  </span>
	                              @endif
	                          </div>
	                      </div>
												<div class="form-group">
	                        <label for="" class="col-md-4 control-label"></label>
	                      </div>
	                      <div class="form-group">
	                          <div class="col-md-6 col-md-offset-4">
	                              <button type="submit" class="btn btn-primary">
	                                  <i class="fa fa-btn fa-user"></i> Simpan
	                              </button>
	                          </div>
	                      </div>
                    </form>
									</div>
								</div>
								<!-- /.row (nested) -->
                <div class="row">
                  <div class="col-md-12">
                  </div>
                </div>

						</div>
						<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop
