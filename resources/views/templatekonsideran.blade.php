<!DOCTYPE html>
<html>
<head>
<style type="text/css">
@font-face {
    font-family: BOOKOS;
    src: url("{{ asset('fonts/BOOKOS.TTF') }}");
    font-weight: normal;
}

@page {
    margin-bottom: 2cm;
}

body {
  font-family: 'BOOKOS';
  /*padding-top: -80px;*/
  /*background: rgb(204,204,204);*/
  height:100%;
  margin:2;
  margin-bottom:50px;
  padding:2;
  /*padding-bottom: 5;*/
  border:0;
}
page[size="F4"] {
  background: white;
  width: 21,6cm;
  height: 33cm;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
}
@media print {
  body, page[size="F4"] {
    margin: 5;
    box-shadow: 0;
    margin-bottom: 2cm;
  }
}

div{
    margin:0;
    border:0;
}

td {
    vertical-align: top;
}

table {
  font-family: Bookman Old;
	font-size: 14px;
}

table.style1{
    font-family: 'BOOKOS';
    border-collapse: collapse;
}

table.style2{
    border-collapse: collapse;
}

table.style1, th.style1, td.style1 {
    font-family: 'BOOKOS';
    border: 0px solid black;
}

table.style2, th.style2, td.style2 {
    border: 1px solid black;
}

    div.page1
    {
        page-break-after: always;
        page-break-inside: avoid;

    }

    div.page
    {
        page-break-after: always;
        page-break-inside: avoid;
        margin-top: 100px;
    }
</style>
</head>
<body>
<div id="header">
<table class="style1" style="padding-left:25;padding-right:20">
	<tr align="center">
		<td rowspan="3" width="700px">
      <img src="logo/logo.jpg" width="75px" height="75px">
			<!-- <img src="logo.jpg" height="50px" width="50px"> -->
			<b><p>
				KEPUTUSAN PEJABAT PEMBUAT KOMITMEN <br />
				BIDANG ..............................<br />
				KANTOR WILAYAH KEMENTERIAN AGAMA<br />
				PROVINSI ... <br />
				NOMOR .........................................<br />
				TENTANG<br />
				PENETAPAN PESERTA SELEKSI<br />
				PROGRAM BEASISWA SANTRI BERPRESTASI<br />
				TAHUN ANGGARAN 2017<br /><br />
				DENGAN RAHMAT TUHAN YANG MAHA ESA<br /><br />
				PEJABAT PEMBUAT KOMITMEN<br />
				BIDANG ............................................<br />
				KANTOR WILAYAH KEMENTERIAN AGAMA<br />
				PROVINSI  ...,<br />
			</p></b>
		</td>
	</tr>
</table>
<table class="style1" style="padding-left:25;padding-right:20">
	<tr>
		<td width="80">Menimbang
		</td>
		<td width="10">:
		</td>
		<td width="10">a.
		</td>
		<td width="260" align="justify">bahwa pesantren sebagai Institusi pendidikan yang khas
		<i>(indegenous)</i> Indonesia, mempunyai kekuatan dan
		kemampuan untuk menghasilkan manusia yang
		berkualitas, berpengetahuan luas, berpikiran maju,
		berwawasan kebangsaan yang kuat dan dibingkai dengan
		keimanan dan ketakwaan sebagai motivasi utamanya,
		apabila diberikan dukungan dan akses yang luas untuk
		mengembangkan potensinya;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">b.
		</td>
		<td width="260"  align="justify">bahwa dalam rangka pengembangan potensi pesantren
		sebagaimana dimaksud dalam huruf a, dipandang perlu
		dilakukan upaya meningkatkan kualitas SDM pondok
		pesantren di bidang sains, teknologi serta sosial
		kemasyarakatan, bagi santri melalui upaya
		pembangunan karakter, kemampuan penalaran, dan
		prestasi untuk memperoleh pendidikan tinggi, melalui
		Program Beasiswa Santri Berprestasi, yaitu program
		afirmatif perluasan akses santri untuk melanjutkan studi
		ke perguruan tinggi berikut dukungan pendanaan studi;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">c.
		</td>
		<td width="260" align="justify" >bahwa nama-nama yang tercantum pada lampiran
		keputusan ini memenuhi syarat administrasi dan
		kelayakan untuk menjadi peserta dan mengikuti seleksi
		Program Beasiswa Santri Berprestasi Tahun Anggaran
		2017;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">d.
		</td>
		<td width="260" align="justify">bahwa berdasarkan pertimbangan sebagaimana
		dimaksud dalam huruf a, huruf b, dan huruf c, perlu
		menetapkan Keputusan Pejabat Pembuat Komitmen
		Bidang ................ tentang Penetapan Peserta
		Seleksi Program Beasiswa Santri Berprestasi Tahun
		Anggaran 2017;
		</td>
	</tr>
</table>
<br /><br />
<table class="style1">
	<tr class="style2">
		<td width="120" class="style1">
		</td>
		<td width="120" class="style2" align="center">Kepala Seksi
		</td>
		<td width="120" class="style2" align="center">Kepala Bidang
		</td>
		<td width="120" class="style2" align="center">Kepala Bagian Tata Usaha
		</td>
		<td width="120" class="style1">
		</td>
	</tr>
	<tr class="style2">
		<td width="120" class="style1">
		</td>
		<td width="120" class="style2" align="center">&nbsp;
		</td>
		<td width="120" class="style2" align="center">&nbsp;
		</td>
		<td width="120" class="style2" align="center">&nbsp;
		</td>
		<td width="120" class="style1">
		</td>
	</tr>
</table>
<br /><br />
<table class="style1" style="padding-left:25;padding-right:20">
	<tr>
		<td width="80">Mengingat
		</td>
		<td width="10">:
		</td>
		<td width="10">1.
		</td>
		<td width="250" align="justify">Undang-Undang Nomor 17 Tahun 2003 tentang
		Keuangan Negara (Lembaran Negara Republik Indonesia
		Tahun 2003 Nomor 47, Tambahan Lembaran Negara
		Republik Indonesia Nomor 4286);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">2.
		</td>
		<td width="250"  align="justify">Undang-Undang Nomor 20 Tahun 2003 tentang Sistem
		Pendidikan Nasional (Lembaran Negara Republik
		Indonesia Tahun 2003 Nomor 78, Tambahan Lembaran
		Negara Republik Indonesia Nomor 4301);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">3.
		</td>
		<td width="250" align="justify" >
		Undang-Undang Nomor 1 Tahun 2003 tentang
		Perbendaharaan Negara (Lembaran Negara Republik
		Indonesia Tahun 2004 Nomor 5, Tambahan Lembaran
		Negara Republik Indonesia 4355);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">4.
		</td>
		<td width="250" align="justify">
		Undang-Undang Nomor 15 Tahun 2004 tentang
		Pemeriksaan Pengelolaan dan Tanggung Jawab
		Keuangan (Lembaran Negara Republik Indonesia Tahun
		2004 Nomor 66, Tambahan Lembaran Negara Republik
		Indonesia Nomor 4400);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">5.
		</td>
		<td width="250" align="justify">
		Undang-Undang Nomor 18 Tahun 2016 tentang
		Anggaran Pendapatan dan Belanja Negara Tahun
		Anggaran 2017 (Lembaran Negara Republik Indonesia
		Tahun 2016 Nomor 240, Tambahan Lembaran Negara
		Republik Indonesia Nomor 5948);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">6.
		</td>
		<td width="250" align="justify">
		Peraturan Pemerintah Nomor 19 Tahun 2005 tentang
		Standar Nasional Pendidikan (Lembaran Negara Republik
		Indonesia Tahun 2005 Nomor 41, Tambahan Lembaran
		Negara Republik Indonesia Nomor 4496) sebagaimana
		telah beberapa kali diubah terakhir dengan Peraturan
		Pemerintah Nomor 13 Tahun 2015 tentang Perubahan
		Kedua Atas Peraturan Pemerintah Nomor 19 Tahun 2005
		tentang Standar Nasional Pendidikan (Lembaran Negara
		Republik Indonesia Tahun 2015 Nomor 45, Tambahan
		Lembaran Negara Republik Indonesia Nomor 5670);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">7.
		</td>
		<td width="250" align="justify">
		Peraturan Pemerintah Nomor 55 Tahun 2007 tentang
		Pendidikan Agama dan Pendidikan Keagamaan
		(Lembaran Negara Republik Indonesia Tahun 2007
		Nomor 124, Tambahan Lembaran Negara Republik
		Indonesia Nomor 4769);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">8.
		</td>
		<td width="250" align="justify">
		Peraturan Pemerintah Nomor 17 Tahun 2010 tentang
		Pengelolaan dan Penyelenggaraan Pendidikan (Lembaran
		Negara Republik Indonesia Tahun 2010 Nomor 23,
		Tambahan Lembaran Negara Republik Indonesia Nomor
		5105) sebagaimana telah diubah dengan Peraturan
		Pemerintah Nomor 66 Tahun 2010 tentang Perubahan
		Atas Peraturan Pemerintah Nomor 17 Tahun 2010
		tentang Pengelolaan dan Penyelenggaraan Pendidikan
		(Lembaran Negara Republik Indonesia Tahun 2010
		Nomor 112, Tambahan Lembaran Negara Republik
		Indonesia Nomor 5157);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">9.
		</td>
		<td width="250" align="justify">
		Peraturan Pemerintah Nomor 45 Tahun 2013 tentang
		Tata Cara Pelaksanaan Anggaran Pendapatan dan
		Belanja Negara (Lembaran Negara Republik Indonesia
		Tahun 2013 Nomor 103, Tambahan Lembaran Negara
		Republik Indonesia Nomor 5423);
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">10.
		</td>
		<td width="250" align="justify">
		Peraturan Presiden Nomor 54 Tahun 2010 tentang
		Pengadaan Barang/Jasa Pemerintah sebagaimana telah
		beberapa kali diubah terakhir dengan Peraturan Presiden
		Nomor 4 Tahun 2015 tentang Perubahan Keeempat Atas
		Peraturan Presiden Nomor 54 Tahun 2010 tentang
		Pengadaan Barang/Jasa Pemerintah;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">11.
		</td>
		<td width="250" align="justify">
		Peraturan Presiden Nomor 83 Tahun 2015 tentang
		Kementerian Agama;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">12.
		</td>
		<td width="250" align="justify">
		Peraturan Menteri Keuangan Nomor 190/PMK.05/2012
		tentang Tata Cara Pembayaran Dalam Rangka
		Pelaksanaan Anggaran Pendapatan dan Belanja Negara;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">13.
		</td>
		<td width="250" align="justify">
		Peraturan Menteri Agama Nomor 13 Tahun 2014 tentang
		Pendidikan Keagamaan Islam;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">14.
		</td>
		<td width="250" align="justify">
		Peraturan Menteri Agama Nomor 45 Tahun 2014 tentang
		Pejabat Perbendaharaan Negara Pada Kementerian
		Agama sebagaimana telah diubah dengan Peraturan
		Menteri Agama Nomor 63 Tahun 2016 tentang
		Perubahan Atas Peraturan Menteri Agama Nomor 45
		Tahun 2014 tentang Pejabat Perbendaharaan Pada
		Kementerian Agama;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">15.
		</td>
		<td width="250" align="justify">
		Peraturan Menteri Keuangan Nomor 168/PMK.05/2015
		tentang Mekanisme Pelaksanaan Anggaran Bantuan
		Pemerintah Pada Kementerian Negara/Lembaga
		sebagaimana telah diubah dengan Peraturan Menteri
		Keuangan Nomor 173/PMK.05/2016 tentang Perubahan
		Atas Peraturan Menteri Keuangan Nomor
		168/PMK.05/2015 tentang Mekanisme Pelaksanaan
		Anggaran Bantuan Pemerintah Pada Kementerian
		Negara/Lembaga;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">16.
		</td>
		<td width="250" align="justify">
		Peraturan Menteri Agama Nomor 67 Tahun 2015 tentang
		Bantuan Pemerintah pada Kementerian Agama
		sebagaimana telah beberapa kali diubah terakhir dengan
		Peraturan Menteri Agama Nomor 62 Tahun 2016 tentang
		Perubahan Kedua Atas Peraturan Menteri Agama Nomor
		67 Tahun 2015 tentang Bantuan Pemerintah Pada
		Kementerian Agama;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">17.
		</td>
		<td width="250" align="justify">
		Peraturan Menteri Agama Nomor 42 Tahun 2016 tentang
		Organisasi dan Tata Kerja Kementerian Agama;
		</td>
	</tr>
	<tr>
		<td width="80">
		</td>
		<td width="10">
		</td>
		<td width="10">18.
		</td>
		<td width="250" align="justify">
		Keputusan Direktur Jenderal Pendidikan Islam Nomor
		7392 Tahun 2017 tentang Petunjuk Teknis Pengelolaan
		Program Beasiswa Santri Berprestasi Tahun Anggaran
		2017
		</td>
	</tr>
</table>
<br />
<table class="style1" style="padding-left:25;padding-right:20">
	<tr>
		<td colspan="3" align="center"><b>MEMUTUSKAN:</b><br /><br />
		</td>
	</tr>
	<tr>
		<td width="80">Menetapkan
		</td>
		<td width="10">:
		</td>
		<td width="200" align="justify">
		KEPUTUSAN PEJABAT PEMBUAT KOMITMEN BIDANG
		.............. .................. KANTOR WILAYAH
		KEMENTERIAN AGAMA PROVINSI ... TENTANG
		PENETAPAN PESERTA SELEKSI PROGRAM BEASISWA
		SANTRI BERPRESTASI TAHUN ANGGARAN 2017.
		</td>
	</tr>
	<tr>
		<td width="80">KESATU
		</td>
		<td width="10">:
		</td>
		<td width="200" align="justify">
		Menetapkan nama-nama sebagaimana tercantum dalam
		Lampiran Keputusan ini sebagai Peserta Seleksi Program
		Beasiswa Santri Berprestasi Tahun Anggaran 2017.
		</td>
	</tr>
	<tr>
		<td width="80">KEDUA
		</td>
		<td width="10">:
		</td>
		<td width="200" align="justify">
		 Peserta seleksi wajib: <br />
		a. Terdaftar secara sah sebagai peserta seleksi;<br />
		b. Mematuhi tata tertib pelaksanaan seleksi;<br />
		c. Mengikuti seluruh tahapan pelaksanaan seleksi; dan<br />
		d. Memenuhi bukti persyaratan administrasi telah
		mengikuti seleksi.<br />
		</td>
	</tr>
	<tr>
		<td width="80">KETIGA
		</td>
		<td width="10">:
		</td>
		<td width="200" align="justify">
		Segala biaya yang ditimbulkan sebagai akibat dari
		keputusan ini dibebankan pada Daftar Isian Pelaksanaan
		Anggaran (DIPA) Kantor Wilayah Kementerian Agama
		Provinsi ... Tahun Anggaran 2017.
		</td>
	</tr>
	<tr>
		<td width="80">KEEMPAT
		</td>
		<td width="10">:
		</td>
		<td width="200" align="justify">
		Keputusan ini mulai berlaku pada tanggal ditetapkan.
		</td>
	</tr>
</table>
<br /><br />
<table class="style1" style="padding-left:30;padding-right:20">
	<tr>
		<td width="250" align="left">
		<br /><br />
		<br /><br />
		<br /><br />
		Disahkan oleh<br />
		KEPALA KANWIL,<br />
		</td>
		<td width="250" align="left">
		Ditetapkan di ... <br />
		pada tanggal, ...<br /><br />
		PEJABAT PEMBUAT KOMITMEN,<br />
		BIDANG .....................,<br />
		KANTOR WILAYAH KEMENTERIAN AGAMA<br />
		PROVINSI ... ,<br />
		</td>
	</tr>
</table>
<br /><br /><br /><br />
<table class="style1" style="padding-left:30;padding-right:20">
	<tr>
		<td width="250" align="left" >
		<font size="2"> ... </font> -->
		</td>
		<td width="250" align="left" ><font size="2">
		...........................................................</font>
		</td>
	</tr>
</table>






</div>

</body>
</html>
