<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lihat Data Pelanggaran Siswa | MAN Insan Cendekia Siak</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-green.min.css') }}">

  <!-- Custom PIP CSS -->
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

  <link href="{{ asset('img/favicon.ico') }}" rel="shortcut icon">

	<!-- DataTables CSS -->
	<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
	<!-- DataTables Responsive CSS -->
	<!-- <link rel="stylesheet" href="{{ asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}"> -->

  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css"> -->
  <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">


	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css"> -->
	<link rel="stylesheet" href="{{ asset('css/responsive.dataTables.min.css') }}">

  <style>
      body {
          /*font-family: 'Lato';*/
          background-color: #444;
          background: url("{{asset('/img/background.jpg')}}") no-repeat center center fixed;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
      }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="skin-green">

  <!-- <div class="wrapper"> -->

    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="{!! URL::to('/') !!}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>IC</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ICSIAK</b> - Black Books</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <a href="#" class="navbar-custom-menu-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
					<ul class="nav navbar-nav">
            <li><a href="{{ URL::to('/login') }}"><i class="fa fa-user"></i> LOGIN</a></li>
            <li><a href="{{ URL::to('/lihat') }}"><i class="fa fa-file-text-o"></i> CEK PELANGGARAN</a></li>

          </ul>
        </div>
      </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <!-- <div class="content-wrapper"> -->

      <!-- Main content -->
      <section class="content">
					<div class="row">
							<div class="col-lg-12">
									<div class="box box-primary">
										<div class="box-header with-border">
											<h1 class="box-title">Lihat Data Pelanggaran Siswa</h1>
										</div>
											<div class="box-body">


												<div class="dataTable_wrapper">
														<table class="table table-responsive table-bordered table-hover" id="tabel2">

															<thead>
																<tr>
																	<th style="width:1%;">No</th>
                                  									<th style="width:12%;">Nama</th>
																	<th style="width:10%;">Tanggal</th>
																	<th>Pelanggaran</th>
																	<th>Keterangan</th>
																	<th style="width:7%;">Poin</th>
																</tr>
															</thead>
															<tbody>
																@foreach ($pelanggaran as $data)
																@if($data->status == 1)
																<tr style="color: #ff0000;">
																@else
																<tr>
																@endif
																	<td>
																		{{ $row++ }}
																	</td>
                                  									<td>
																		{{ $data->siswa->nama_siswa }}
																	</td>
																	<td>
																		{{ $data->tanggal }}
																	</td>
																	<td>
																		{{ $data->aturan->nama_aturan }}
																	</td>
																	<td>
																		{{ $data->keterangan }}
																	</td>
																	<td>
																		{{ $data->aturan->poin }} Poin
																	</td>
																</tr>
																@endforeach
															</tbody>
															<tfoot>
												        <tr>
												          <th></th>
												          <th></th>
												          <th></th>
                                  <th></th>
												          <th>Jumlah Poin</th>
												          <th>-</th>
												        </tr>
												      </tfoot>
													 </table>
										 </div>
										 </div>
					 					<!-- /.panel-body -->
					 			</div>
					 			<!-- /.panel -->
					 	</div>
					 	<!-- /.col-lg-12 -->
					</div>
					 <!-- /.row -->
					 <div class="row">
							<div class="col-lg-12">
									<div class="box box-primary">
										<div class="box-header with-border">
											<h1 class="box-title">Lihat Data Hukuman Siswa</h1>
										</div>
											<div class="box-body">


												<div class="dataTable_wrapper">
														<table class="table table-responsive table-striped table-bordered table-hover" id="tabel3">

															<thead>
																<tr>
																	<th style="width:1%;">No</th>
                                  									<th style="width:15%;">Nama</th>
																	<th style="width:10%;">Tanggal</th>
																	<th style="width:12%;">Kategori Sangsi</th>
																	<th>Keterangan</th>
																	<th style="width:7%;">Kompensasi</th>
																</tr>
															</thead>
															<tbody>
																@foreach ($hukuman as $data)
																<tr>
																	<td>
																		{{ $row1++ }}
																	</td>
                                  									<td>
																		{{ $data->siswa->nama_siswa }}
																	</td>
																	<td>
																		{{ $data->tanggal }}
																	</td>
																	<td>
																		{{ $data->sangsi->nama_sangsi }}
																	</td>
																	<td>
																		{{ $data->keterangan }}
																	</td>
																	<td>
																		{{ $data->sangsi->kompensasi }}
																	</td>
																</tr>
																@endforeach
															</tbody>
															<tfoot>
												        <tr>
												          <th></th>
												          <th></th>
												          <th></th>
                                  <th></th>
												          <th>Jumlah Poin</th>
												          <th>-</th>
												        </tr>
												      </tfoot>
													 </table>
										 </div>
										 </div>
					 					<!-- /.panel-body -->
					 			</div>
					 			<!-- /.panel -->
					 	</div>
					 	<!-- /.col-lg-12 -->
					</div>
					 <!-- /.row -->
			</section>

			<!-- jQuery 2.2.3 -->
	  	<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
	  	<!-- Bootstrap 3.3.6 -->
	  	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	 	<!-- AdminLTE App -->
	 	<script src="{{ asset('dist/js/app.min.js') }}"></script>

	 	<!-- DataTables JavaScript -->
	 	<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
	 	<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
		<!-- <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script> -->

		<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script> -->
		<script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
		<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script> -->
		<script src="{{ asset('js/buttons.flash.min.js') }}"></script>
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> -->
		<script src="{{ asset('js/jszip.min.js') }}"></script>
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script> -->
		<script src="{{ asset('js/pdfmake.min.js') }}"></script>
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> -->
		<script src="{{ asset('js/vfs_fonts.js') }}"></script>
		<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script> -->
		<script src="{{ asset('js/buttons.html5.min.js') }}"></script>
		<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script> -->
		<script src="{{ asset('js/buttons.print.min.js') }}"></script>

		<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.3/js/dataTables.rowReorder.min.js"></script> -->
		<!-- <script src="https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script> -->
		<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
    $('#tabel2').DataTable({
						"footerCallback": function (row, data, start, end, display) {
								var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
								var api = this.api(),
								intVal = function (i) {
											return typeof i === 'string' ?
													 i.replace(/[\Poin]/g, '')*1 :
													 typeof i === 'number' ?
													 i : 0;
								},
								 total5 = api
										.column(5)
										.data()
										.reduce(function (a, b) {
												return intVal(a) + intVal(b);
										}, 0);

								 $(api.column(5).footer()).html(
										'' + jumFormat(total5) + ' Poin'
								 );
						},
						// responsive: true,
						iDisplayLength: 50,
						scrollX: true,
						bSort : false,
						language: {
							"sProcessing":   "Sedang memproses...",
							"sLengthMenu":   "Tampilkan _MENU_ entri",
							"sZeroRecords":  "Tidak ditemukan Data.",
							"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
							"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
							"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
							"sInfoPostFix":  "",
							"sSearch":       "Cari:",
							"sUrl":          "",
							"oPaginate": {
									"sFirst":    "Pertama",
									"sPrevious": "Sebelumnya",
									"sNext":     "Selanjutnya",
									"sLast":     "Terakhir"
							}
						},
						"columnDefs": [
							{ "orderable": false, "targets": 0 },
							{ width: 20, targets: 0 }
						],
						dom: 'Bfrtip',
		        buttons: [
		            'excel', 'pdf', 'print',{
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'A4'
            }
		        ]
		});

    $('#tabel3').DataTable({						
						responsive: true,
						bSort : false,
						iDisplayLength: 50,
						scrollX: true,
						language: {
							"sProcessing":   "Sedang memproses...",
							"sLengthMenu":   "Tampilkan _MENU_ entri",
							"sZeroRecords":  "Tidak ditemukan Data.",
							"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
							"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
							"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
							"sInfoPostFix":  "",
							"sSearch":       "Cari:",
							"sUrl":          "",
							"oPaginate": {
									"sFirst":    "Pertama",
									"sPrevious": "Sebelumnya",
									"sNext":     "Selanjutnya",
									"sLast":     "Terakhir"
							}
						},
						"columnDefs": [
							{ "orderable": false, "targets": 0 },
							{ width: 20, targets: 0 }
						],
						dom: 'Bfrtip',
		        buttons: [
		            'excel', 'pdf', 'print',{
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'A4'
            }
		        ]
		});
	} );
	</script>

</body>
</html>
