<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mencari Data Pelanggaran Siswa | MAN Insan Cendekia Siak</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-green.min.css') }}">

  <!-- Custom PIP CSS -->
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

  <link href="{{ asset('img/favicon.ico') }}" rel="shortcut icon">

  @yield('css')

  <style>
      body {
          /*font-family: 'Lato';*/
          background-color: #444;
          background: url("{{asset('/img/background.jpg')}}") no-repeat center center fixed;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
      }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="skin-green">

  <!-- <div class="wrapper"> -->

    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="{!! URL::to('/') !!}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>IC</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ICSIAK</b> - Black Books</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <a href="#" class="navbar-custom-menu-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('/login') }}"><i class="fa fa-user"></i> LOGIN</a></li>
            <li><a href="{{ URL::to('/lihat') }}"><i class="fa fa-file-text-o"></i> CEK PELANGGARAN</a></li>

          </ul>
        </div>
      </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <!-- <div class="content-wrapper"> -->

      <!-- Main content -->
      <section class="content">
        <div class="container">
          @if (session()->has('pesan.message'))
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="box-body">
                <div class="alert alert-{{ session()->get('pesan.level') }} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                        {!! session()->get('pesan.message') !!}
                </div>
                <!-- <div class="alert alert-{{ session()->get('pesan.level') }} fade in top10 bottom7">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {!! session()->get('pesan.message') !!}
                </div> -->
              </div>
            </div>
          </div>
          @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Cek Data Pelanggaran Siswa</h3>
                        </div>
                        <div class="box-body">
                          <br />

                          {!! Form::open(['url' => 'lihat']) !!}
                            <div class="form-group {{ ! $errors->first('nama') ? '': 'has-error' }}">
                                <div class="col-md-2 col-md-offset-2">
                                {!! Form::label('nama', 'Masukan Nama Anda :') !!}
                                </div>
                                <div class="col-md-4">
                                {!! Form::text('nama',null ,['class' => 'form-control', 'placeholder' => 'Nama']) !!}
                                @if( $errors->first('nama') )
                                  <span class="help-block text-danger">{{ $errors->first('nama') }}</span>
                                @endif
                                </div>
                                <div class="col-md-2">
                                 {!! Form::submit("Cek", ['class' => 'btn btn-primary form-control']) !!}
                                </div>
                            </div>
                          {!! Form::close() !!}
                          <br />
                        </div>

                        <div class="box-footer">
                          <p style="text-align:center;">Information and Communication Technology (ICT) Center © MAN Insan Cendekia Siak<p>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
            </div>
        </div>
      </section>
<!-- /.content -->
<!-- </div> -->
<!-- /.content-wrapper -->
<!-- </div> -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/app.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
Both of these plugins are recommended to enhance the
user experience. Slimscroll is required when using the
fixed layout. -->
</body>
</html>
