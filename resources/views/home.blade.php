@extends('app')

@section('content')
<div class="container">
    <div class="row">
        @if (Session::has('token_error'))
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('token_error') }}
          </div>
        @endif
        <div class="col-md-12">
            <div class="box box-info box-transparent">
                <div class="box-header with-border white">
                  <h3 class="box-title">SELAMAT DATANG !</h3>
                </div>
                <div class="box-body white">

                      <h3 style="text-align:center;">Sistem Informasi dan Manajemen</h3>
                      <h3 style="text-align:center;">CATATAN PELANGGARAN SISWA - MAN INSAN CENDEKIA SIAK</h3>

                      <!-- <h4>Informasi Umum</h4>

                      <p>Sistem informasi dan manajemen Program Indonesia Pintar Pada Pendidikan Keagamaan Islam bertujuan untuk:</p>
                      <ul>
                          <li>Menyediakan informasi yang dipergunakan di dalam perencanaan kegiatan dan anggaran Program Indonesia Pintar Pada Pendidikan Keagamaan Islam;</li>
                          <li>Menyediakan informasi yang dipergunakan dalam pengendalian, pengawasan, evaluasi, dan perbaikan berkelanjutan; dan</li>
                          <li>Menyediakan informasi untuk pengambilan keputusan dan/atau kebijakan.</li>
                      </ul>
                      <p>Sistem informasi dan manajemen Program Indonesia Pintar Pada Pendidikan Keagamaan Islam diselenggarakan secara terpadu dengan Sistem Informasi dan Manajemen Pendidikan (Education Management Information System/EMIS), Pendidikan Islam.</p> -->

                </div>

                <div class="box-footer box-transparent white">
                  <p style="text-align:center;">Information and Communication Technology (ICT) Center © MAN Insan Cendekia Siak<p>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
</div>
@endsection
