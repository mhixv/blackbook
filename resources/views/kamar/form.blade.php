<div class="form-group">
	{!! Form::label('jenis', 'Jenis Asrama :') !!}
	{!! Form::select('jenis',['Asrama Putra 1'=>'Asrama Putra 1','Asrama Putra 2'=>'Asrama Putra 2',
														'Asrama Putri 1'=>'Asrama Putri 1','Asrama Putri 2'=>'Asrama Putri 2'],null,
														['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('nomor_kamar', 'Nama Kamar :') !!}
	{!! Form::text('nomor_kamar',null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-4 col-md-offset-4">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
