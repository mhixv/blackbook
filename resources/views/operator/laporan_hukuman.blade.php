@extends('operator_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}

	<link href="{{ asset('css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css"> -->
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">


	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css"> -->
	<!-- <link rel="stylesheet" href="{{ asset('css/responsive.dataTables.min.css') }}"> -->

	<style>
	.input-group[class*="col-"] {
		float: none;
		padding-right: 15px;
		padding-left: 15px;
	}
	.chosen-container-single .chosen-single {
		line-height: 30px;
		font-size: 14px;
	}
	.heighttext{ padding: 20px 10px; line-height: 28px; }
	</style>

@stop

@section('content-header')
	      <h1>Laporan Data Hukuman Siswa</h1>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h1 class="box-title">Laporan Data Hukuman Siswa</h1>
			</div>
			<div class="row">
							<div class="col-md-6 col-md-offset-3" >
								<br>
									{!! Form::model(null,['method' => 'POST','url' => 'operator/laporan_hukuman']) !!}
									<!-- {!! Form::label('university_name', 'Pilih Universitas :') !!} -->
									<div class="col-md-6">
									{!! Form::select('kelas_id', array_merge(['0' => 'Pilih Kelas'], $kelas), 0,['id'=>'kelas_id', 'class' => 'form-control heighttext']) !!}</div><div class="col-md-6">
									{!! Form::select('guru_id', array_merge(['0' => 'Pilih Guru Asuh'], $guru), 0,['id'=>'guru_id', 'class' => 'form-control heighttext']) !!}</div>
									&nbsp;
									<div class="col-md-12">
									{!! Form::submit('Tampilkan data', ['class' => 'btn btn-primary form-control']) !!}</div>
	<!-- 								<div class="col-md-2 col-md-offset-2">
									<br>
										<a class="btn btn-success" href="{!! URL::to('admin/DonwloadDepartement') !!}"><i class="glyphicon glyphicon-download-alt"></i> Download in excel</a>
									</div> -->
									{!! Form::close() !!}
								<br><div class="col-md-12 col-md-offset-3">
									<p class="navbar-text" align="center" style="font-size:14px"><b>Kelas : {!! $info_kelas !!}<br>Guru Asuh : {!! $info_guru !!}</b></p></div>
							</div>
						</div>
			<div class="box-body">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="tabel-kabupaten">
						<thead>
							<tr>
								<th style="width:1%;">No</th>
								<th style="width:15%;">Nama Siswa</th>
								<th style="width:40%;">Hukuman</th>
								<th style="width:7%;">Poin Kompensasi</th>
								<th style="width:15%;">Kelas</th>
								<th style="width:25%;">Guru Asuh</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($hukuman as $sis)
							<tr>
								<td class="center">
									{{ $row++ }}
								</td>
								<td class="center">
									{{ $sis->nama_siswa }}
								</td>
								<td class="center">
									{{ $sis->nama_sangsi }}
								</td>
								<td class="center">
									{{ $sis->kompensasi }}
								</td>
								<td class="center">
									{{ $sis->nama_kelas }}
								</td>
								<td class="center">
									{{ $sis->nama_guru }}
								</td>
							</tr>
							@endforeach
						</tbody>
				 	</table>
		 		</div>
		 	</div>
				<!-- /.panel-body -->
 		</div>
 			<!-- /.panel -->
 	</div>
 	<!-- /.col-lg-12 -->
</div>
 <!-- /.row -->

@stop


@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}

	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script> -->
	<script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script> -->
	<script src="{{ asset('js/buttons.flash.min.js') }}"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> -->
	<script src="{{ asset('js/jszip.min.js') }}"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script> -->
	<script src="{{ asset('js/pdfmake.min.js') }}"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> -->
	<script src="{{ asset('js/vfs_fonts.js') }}"></script>
	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script> -->
	<script src="{{ asset('js/buttons.html5.min.js') }}"></script>
	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script> -->
	<script src="{{ asset('js/buttons.print.min.js') }}"></script>

	<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.3/js/dataTables.rowReorder.min.js"></script> -->
	<!-- <script src="https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script> -->
	<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
			$('#tabel-kabupaten').DataTable({
							responsive: true,
							scrollX: true,
							bSort : false,
              language: {
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data Pelanggaran.",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
              },
              "columnDefs": [
                { "orderable": false, "targets": 0 },
								{ width: 4, targets: 0 }
              ],
				dom: 'Bfrtip',
		        buttons: [
		            'excel', 'pdf', 'print',{
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'A4'
            }
		        ]
		});
	} );
	</script>

	<script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script>
	<script>
		$(document).ready(function () {
			$('#kelas_id').chosen({width: "100%"});
			$('#guru_id').chosen({width: "100%"});
		});
	</script>

	<!-- Delete Data JavaScript - Jeffry Wayy -->
	{!! Html::script('js/laravel.js', array('type' => 'text/javascript')) !!}

@stop