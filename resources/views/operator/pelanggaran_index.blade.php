@extends('operator_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}

@stop

@section('content-header')
	      <h1>Daftar Data Pelanggaran Siswa Yang Anda Laporkan</h1>
@endsection

@section('content')
<div class="row">
		<div class="col-lg-12">
				<div class="box box-primary">
						<div class="box-body">
							<div class="col-md-4 col-md-offset-4">
								<a class="btn btn-success" href="{!! URL::to('operator/pelanggaran/create') !!}"><i class="fa fa-floppy-o fa-fw"></i> Tambah Pelanggaran</a>
							</div>
							<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="tabel-kabupaten">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Siswa</th>
												<th>Pelanggaran</th>
												<th>Poin</th>
												<th>Tanggal</th>
												<th>Guru</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($pelanggaran as $sis)
											<tr>
												<td class="center">
													{{ $row++ }}
												</td>
												<td class="center">
													{{ $sis->siswa->nama_siswa }}
												</td>
												<td class="center">
													{{ $sis->aturan->nama_aturan }}
												</td>
												<td class="center">
													{{ $sis->aturan->poin }}
												</td>
												<td class="center">
													{{ $sis->tanggal }}
												</td>
												<td class="center">
													{{ $sis->guru->nama_guru }}
												</td>
												<td class="center">
														<a class="btn btn-xs btn-success" href="{{ URL::to('operator/pelanggaran/'.$sis->id.'/edit') }}"><i class="fa fa-edit fa-fw"></i> Edit</a>
														&nbsp;&nbsp;
														<a class="btn btn-xs btn-danger" href="{{ URL::to('operator/pelanggaran/delete/'.$sis->id) }}" data-token="{!! csrf_token() !!} " data-method="delete" data-confirm="Anda yakin menghapus data Pelanggaran?"><i class="fa fa-remove fa-fw"></i> Hapus</a>
												</td>
											</tr>
											@endforeach
										</tbody>
								 </table>
					 </div>
					 </div>
 					<!-- /.panel-body -->
 			</div>
 			<!-- /.panel -->
 	</div>
 	<!-- /.col-lg-12 -->
</div>
 <!-- /.row -->

@stop


@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
			$('#tabel-kabupaten').DataTable({
							responsive: true,
							bSort : false,
              language: {
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data Pelanggaran.",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
              },
              "columnDefs": [
                { "orderable": false, "targets": 0 },
								{ width: 20, targets: 0 }
              ]
			});
	});
	</script>

	<!-- Delete Data JavaScript - Jeffry Wayy -->
	{!! Html::script('js/laravel.js', array('type' => 'text/javascript')) !!}

@stop
