@extends('operator_index')

@section('css')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
<link href="{{ asset('css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
<style>
.input-group[class*="col-"] {
	float: none;
	padding-right: 15px;
	padding-left: 15px;
}
.chosen-container-single .chosen-single {
	line-height: 30px;
	font-size: 14px;
}
.heighttext{ padding: 20px 10px; line-height: 28px; }
</style>
@endsection

@section('content-header')
				<h1 class="page-header">Edit Data Pelanggaran</h1>
@endsection
<!-- /.row -->
@section('content')
<div class="row">
		<div class="col-lg-12">
				<div class="box box-primary">
						<div class="box-body">
							{!! Form::model($guru,['method' => 'PATCH','class'=>'form-horizontal','url' => ['/operator/pelanggaran',$pelanggaran1->id]]) !!}
							<!-- <form class="form-horizontal" role="form" method="PATCH" action="{{ url('/operator/pelanggaran').$pelanggaran1->id }}"> -->
									{{ csrf_field() }}
									<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
											<label for="nama" class="col-md-2 control-label">Nama Siswa</label>
											<div class="col-md-8">
												{!! Form::select('siswa_id', $siswa, $pelanggaran1->siswa_id,['id'=>'nama','class' => 'form-control','placeholder' => 'Pilih Nama Siswa...']) !!}
											</div>
									</div>
									<div class="form-group{{ $errors->has('pelanggaran') ? ' has-error' : '' }}">
											<label for="pelanggaran" class="col-md-2 control-label">Jenis Pelanggaran</label>
											<div class="col-md-8">
												  {!! Form::select('aturan_id', $pelanggaran, $pelanggaran1->aturan_id,['id'=>'pelanggaran','class' => 'form-control heighttext','placeholder' => 'Pilih Jenis Pelanggaran...']) !!}
											</div>
									</div>
									<div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
											<label for="tanggal" class="col-md-2 control-label">Tanggal Pelanggaran</label>
											<div class="input-group col-md-8">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
											<!-- <div class=""> -->
													{!! Form::text('tanggal', $pelanggaran1->tanggal,['id'=>'tanggal','class' => 'form-control','placeholder' => 'Pilih Jenis Pelanggaran...']) !!}
													<!-- <input id="tanggal" type="text" class="form-control" class="tanggal" name="tanggal" value="{{ old('tanggal') }}"> -->
											</div>
									</div>
									<div class="form-group{{ $errors->has('pelapor') ? ' has-error' : '' }}">
											<label for="pelapor" class="col-md-2 control-label">Nama Pelapor</label>
											<div class="col-md-8">
												{!! Form::select('guru_id', $guru, $pelanggaran1->guru_id,['id'=>'guru','class' => 'form-control','placeholder' => 'Pilih Pelapor...']) !!}
											</div>
									</div>
									<div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
											<label for="keterangan" class="col-md-2 control-label">Detil Pelanggaran</label>
											<div class="col-md-8">
												{!! Form::textarea('keterangan',$pelanggaran1->keterangan,['class' => 'form-control','placeholder' => 'Detil Pelanggaran...']) !!}
											</div>
									</div>

									<div class="form-group">
										<label for="" class="col-md-2 control-label"></label>
									</div>
									<div class="form-group">
											<div class="col-md-8 col-md-offset-2">
													<button type="submit" class="btn btn-primary">
															<i class="fa fa-btn fa-user"></i> Simpan
													</button>
											</div>
									</div>
							</form>
							<div class="row">
								<div class="col-md-12">
									@include('errors.list')
								</div>
							</div>
						</div>
						<!-- /.box-body -->
				</div>
				<!-- /.box -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('script')
<!-- bootstrap datepicker -->
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<script>
	$(function() {
	  $('#tanggal').datepicker({ 
	  		format: 'yyyy-mm-dd',
			// todayHighlight: TRUE,
    		autoclose: true
	    });
	});
</script>

<script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script>
<script>
	$(document).ready(function () {
		$('#nama').chosen({width: "100%"});
		$('#pelanggaran').chosen({width: "100%"});
		$('#guru').chosen({width: "100%"});
	});
</script>
@endsection
