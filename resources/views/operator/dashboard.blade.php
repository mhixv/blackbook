@extends('operator_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}

	<style>

	</style>

@stop

@section('content-header')
	      <h1>Dasboard Data Aplikasi Black Book</h1>
@endsection
@section('content')
<!-- Small boxes (Stat box) -->
<!-- Small boxes (Stat box) -->
<div class="row">
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				Total Pelanggaran Siswa
				<h3> {{ $jml_pel_siswa }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-person"></i>
			</div>
			<a href="#" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				Siswa Pelanggar
				<h3> {{ $jml_siswa_pel }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="#" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-blue">
			<div class="inner">
				Total Hukuman
				<h3> {{ $jml_hukuman }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-card"></i>
			</div>
			<a href="#" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				Jumlah Aturan
				<h3> {{ $jml_aturan }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-pie-graph"></i>
			</div>
			<a href="#" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-warning">
          <div class="box-header with-border" style="background-color:#ff0000;">
            <h1 class="box-title" >Siswa dengan nilai pelangaran melebihi 40 Poin</h1>
          </div>
            <div class="box-body">
              <div class="dataTable_wrapper">
                  <table class="table table-responsive table-striped table-bordered table-hover" id="tabel4">

                    <thead>
                      <tr>
                        <th style="width:1%;">No</th>
                        <th>Nama Siswa</th>
                        <th style="text-align: center">Jumlah Pelanggaran</th>
                        <th style="text-align: center">Jumlah Poin</th>
                        <th style="text-align: center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($jml_tot_pel as $data)
                      <tr>
                        <td>
                          {{ $row2++ }}
                        </td>
                        <td>
                          {{ $data->nama_siswa }}
                        </td>
                        <td style="width:7%;text-align: center">
                          {{ $data->jumlah }} Kali
                        </td>
                        <td style="width:7%;text-align: center">
                          {{ $data->total }} Poin
                        </td>
                        <td style="width:25%;text-align: center">
                          <a class="btn btn-xs btn-warning" href="{{ URL::to('operator/hukuman/'.$data->siswa_id) }}"><i class="fa fa-edit fa-fw"></i> Beri Hukuman</a>&nbsp;
                          <a class="btn btn-xs btn-info" href="{{ URL::to('operator/lihat/'.$data->siswa_id) }}"><i class="fa fa-eye fa-fw"></i> Lihat Detil</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                 </table>
           </div>
           </div>
          <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
  </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h1 class="box-title">Top 5 Siswa Pelanggar</h1>
          </div>
            <div class="box-body">
              <div class="dataTable_wrapper">
                  <table class="table table-responsive table-striped table-bordered table-hover" id="tabel3">

                    <thead>
                      <tr>
                        <th style="width:1%;">No</th>
                        <th style="width:20%;">Nama Siswa</th>
                        <th style="width:5%;text-align: center">Jumlah</th>
                        <th style="width:5%;">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($top_ten_pelanggar as $data)
                      <tr>
                        <td>
                          {{ $row++ }}
                        </td>
                        <td>
                          {{ $data->nama_siswa }}
                        </td>
                        <td style="width:7%;text-align: center">
                          {{ $data->jumlah }} Kali
                        </td>
                        <td>
                         
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                 </table>
           </div>
           </div>
          <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
  </div>
  <!-- /.col-lg-6 -->
  <div class="col-lg-6 col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h1 class="box-title">Top 5 Aturan Paling Sering Dilanggar</h1>
          </div>
            <div class="box-body">
              <div class="dataTable_wrapper">
                  <table class="table table-responsive table-striped table-bordered table-hover" id="tabel3">

                    <thead>
                      <tr>
                        <th style="width:1%;">No</th>
                        <th style="width:35%;">Nama Aturan</th>
                        <th style="width:5%;text-align: center">Jumlah</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($top_ten_aturan as $data)
                      <tr>
                        <td>
                          {{ $row1++ }}
                        </td>
                        <td>
                          {{ $data->nama_aturan }}
                        </td>
                        <td style="width:7%;text-align: center">
                          {{ $data->jumlah }} Kali
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                 </table>
           </div>
           </div>
          <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
  </div>
  <!-- /.col-lg-6 -->
</div>
 <!-- /.row -->
@endsection

@section('script')
<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
			$('#tabel4').DataTable({
					// responsive: true,
						bSort : false,
						iDisplayLength: 20,
              language: {
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data.",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
              }
			});
	});
	</script>
@endsection
