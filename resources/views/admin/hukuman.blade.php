@extends('admin_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}

@stop


@section('content-header')
	 <h1>Daftar Hukuman kepada Siswa Tahun Ajaran {{ $tahun_ajaran_aktif }}</h1>
@endsection

@section('content')
<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="tabel-provinsi">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Sangsi</th>
										<th>Poin</th>
										<th>Pelanggar</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($hukuman as $huk)
									<tr>
										<td class="center">
											{{ $counter++}}
										</td>
										<td>
											{{ $huk->nama_sangsi }}
									    </td>
										<td>
											{{ $huk->kompensasi }}
										</td>
										<td>
											{{ $huk->nama_siswa }}
										</td>
									</tr>
									@endforeach
								</tbody>
					   </table>
			 		</div>
				</div>
		 		<!-- /.panel-body -->
 			</div>
 			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
			$('#tabel-provinsi').DataTable({
							responsive: true,
							bSort : false,
              language: {
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data Hukuman.",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
              },
              "columnDefs": [
                { "orderable": false, "targets": 0 },
								{ width: 20, targets: 0 }
              ]
			});
	});
	</script>

	<!-- Delete Data JavaScript - Jeffry Wayy -->
	{!! Html::script('js/laravel.js', array('type' => 'text/javascript')) !!}

@stop
