@extends('admin_index')

@section('content-header')
	      <h1>Konfigurasi Aplikasi Black Book</h1>
@endsection
@section('content')
<div class="row">
	<!-- <div class="box box-info"> -->
		<div class="col-lg-6">
				<div class="box box-info">
					<div class="box-header with-border">
			      <h3 class="box-title">Konfigurasi Semester Aktif :</h3>
			    </div>
						<div class="box-body">
						{!! Form::model($pengaturan,['method' => 'PATCH','url' => ['admin/tahun_aktif',$pengaturan->id]]) !!}
								<div class="form-group {{ $errors->has('ta_aktif') ? ' has-error' : '' }}">
										<label for="ta_aktif" class="col-md-4 control-label">Semester Aktif</label>

										<div class="col-md-6">
												{!! Form::select('tahun_aktif', array('0'=> '-Pilih Tahun Aktif-')+$tahunajaran, $pengaturan->tahun_aktif, ['class' => 'form-control']) !!}
												@if ($errors->has('tahun_aktif'))
														<span class="help-block">
																<strong>{{ $errors->first('tahun_aktif') }}</strong>
														</span>
												@endif
										</div>
								</div>
						</div>
						<!-- /.panel-body -->
						<div class="box-footer">
                {!! Form::submit('Simpan', ['class' => 'btn btn-primary form-control']) !!}
            </div>
				{!! Form::close() !!}
				</div>
				<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
		<div class="col-lg-6">
				<div class="box box-info">
					<div class="box-header with-border">
			      <h3 class="box-title">Konfigurasi Lainnya (belum aktif) :</h3>
			    </div>
						<div class="box-body">
              <div class="form-group">
                {!! Form::label('Dokumen_Pendukung', 'Dokumen Pendukung :') !!}
                {!! Form::file('dokumen_pendukung',null, ['class' => 'form-control']) !!}
              </div>
          </div>
        </div>
				<!-- {!! Form::close() !!} -->
				<!-- <div class="row">
					<div class="col-md-12">
						@include('errors.list')
					</div>
				</div> -->
    </div>
	<!-- </div> -->
</div>
<!-- /.row -->
@stop
