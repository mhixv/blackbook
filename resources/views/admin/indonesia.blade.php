@extends('admin_index')


@section('css')
<!-- DataTables CSS -->
{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
<!-- {!! Html::style('plugins/datatables/jquery.dataTables.min.css') !!} -->
<!-- DataTables Responsive CSS -->
{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}
{!! Html::style('plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.min.css') !!}

<style>
.nav-tabs-custom > .nav-tabs > li.active {
	border-top-color: #3c8dbc;
}
.nav-tabs-custom > .nav-tabs > li.active > a {
	border-top-color: transparent;
	border-left-color: #00a65a;
	border-right-color: #00a65a;
}
.nav-tabs-custom > .nav-tabs > li.active > a, .nav-tabs-custom > .nav-tabs > li.active:hover > a {
	background-color: #00a65a;
	color: #444;
}

</style>

@stop


@section('content-header')
	      <h1>
					Status Kegiatan Pelaksanaan PIP Tahun Anggaran {{ $tahun_anggaran_aktif }}
	      </h1>
@endsection

@section('content')
<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="box box-success">
				<div class="box-body">

					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_1" data-toggle="tab"><b>Informasi Umum</b></a></li>
							<li><a href="#tab_2" data-toggle="tab"><b>Sosialisasi</b></a></li>
							<li><a href="#tab_3" data-toggle="tab"><b>Pendataan</b></a></li>
							<li><a href="#tab_4" data-toggle="tab"><b>Penetapan</b></a></li>
							<li><a href="#tab_5" data-toggle="tab"><b>Penyaluran</b></a></li>
							<li><a href="#tab_6" data-toggle="tab"><b>Distribusi</b></a></li>
							<li><a href="#tab_7" data-toggle="tab"><b>Monitoring</b></a></li>
							<li><a href="#tab_8" data-toggle="tab"><b>Evaluasi</b></a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="dataTable_wrapper">
								    <table class="table table-striped table-bordered table-hover display" id="tabel_1" width="100%" cellspacing="0">
								      <thead>
								        <tr>
								          <th style="width: 5px;text-align:center;">No</th>
								          <th>Provinsi</th>
								          <th>Jml Penerima K1</th>
								          <th>Jumlah Dana K1</th>
								          <th>Jml Penerima K2</th>
								          <th>Jumlah Dana K2</th>
								          <th>Jml Penerima K3</th>
								          <th>Jumlah Dana K3</th>
								          <th>Status</th>
								          <th>Aksi</th>
								        </tr>
								      </thead>
								      <tbody>
								        @foreach ($info_umum as $sos)
								        <tr>
								          <td class="center">
								            {{ $counter++}}
								          </td>
								          <td class="center">
								            {{ $sos->user_id }}
								           </td>
								          <td class="center">
								            {{ $sos->k1_jml_penerima }}
								          </td>
								          <td class="center">
								            Rp {{ $sos->k1_jml_dana }}
								          </td>
								          <td class="center">
								            {{ $sos->k2_jml_penerima }}
								          </td>
								          <td class="center">
								            Rp {{ $sos->k2_jml_dana }}
								          </td>
								          <td class="center">
								            {{ $sos->k3_jml_penerima }}
								          </td>
								          <td class="center">
								            Rp {{ $sos->k3_jml_dana }}
								          </td>
								          <td class="center">
								            @if ($sos->status == 2)
								              <b style="color : orange"> Menunggu Verifikasi</b>
								            @endif
								            @if ($sos->status == 3)
								              <b style="color : red"> Revisi Pusat</b>
								            @endif
								            @if ($sos->status == 4)
								              <b style="color : green"> Selesai</b>
								            @endif
								          </td>
								          <td class="center">
								              <a class="btn btn-xs btn-success" href="{!! URL::to('admin/informasi_umum/lihat/'.$sos->id) !!}"><i class="fa fa-eye"></i> Detil</a>
								           </td>
								        </tr>
								        @endforeach
								      </tbody>
								      <tfoot>
								        <tr>
								          <th>-</th>
								          <th>-</th>
								          <th>Jml Penerima K1</th>
								          <th>Jml Dana K1</th>
								          <th>Jml Penerima K2</th>
								          <th>Jml Dana K2</th>
								          <th>Jml Penerima K3</th>
								          <th>Jml Dana K3</th>
								          <th>-</th>
								          <th>-</th>
								        </tr>
								      </tfoot>
								   </table>
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_2">
								<div class="dataTable_wrapper">
								    <table class="table table-striped table-bordered table-hover display" id="tabel_2" width="100%" cellspacing="0">
								      <thead>
								        <tr>
								          <th>No</th>
								          <th>Provinsi</th>
								          <th>Pelaksana</th>
								          <th>Jumlah Pelaksanaan</th>
								          <th>Waktu Pelaksanaan</th>
								          <th>Sumber Anggaran</th>
								          <th>Jumlah Anggaran</th>
								          <th>Status</th>
								          <th>Aksi</th>
								        </tr>
								      </thead>
								      <tbody>
								        @foreach ($sos_koo as $sos)
								        <tr>
								          <td class="center">
								            {{ $counter1++}}
								          </td>
								          <td class="center">
								            {{ $sos->user_id }}
								            </td>
								          <td class="center">
								            {{ $sos->sosialisasi_pelaksana }}
								          </td>
								          <td class="center">
								            {{ $sos->sosialisasi_jml_pelaksanaan }}  Kali
								          </td>
								          <td class="center">
								            {{ $sos->sosialisasi_waktu_pelaksanaan }}
								          </td>
								          <td class="center">
								            {{ $sos->sosialisasi_sumber_anggaran }}
								          </td>
								          <td class="center">
								            Rp {{ $sos->sosialisasi_jml_anggaran }}
								          </td>
								          <td class="center">
								            @if ($sos->status == 2)
								              <b style="color : orange"> Menunggu Verifikasi</b>
								            @endif
								            @if ($sos->status == 3)
								              <b style="color : red"> Revisi Pusat</b>
								            @endif
								            @if ($sos->status == 4)
								              <b style="color : green"> Selesai</b>
								            @endif
								          </td>
								          <td class="center">
								              <a class="btn btn-xs btn-success" href="{!! URL::to('admin/sosialisasi_koordinasi/lihat/'.$sos->id) !!}"><i class="fa fa-eye"></i> Detil</a>
								           </td>
								        </tr>
								        @endforeach
								      </tbody>
								      <tfoot>
								        <tr>
								          <th>-</th>
								          <th>-</th>
								          <th>-</th>
								          <th>-</th>
								          <th>-</th>
								          <th>-</th>
								          <th>-</th>
								          <th>-</th>
								          <th>-</th>
								        </tr>
								      </tfoot>
								   </table>
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_3">
								@include('admin.pendataan_verifikasi.partial')
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_4">
								@include('admin.penetapan_penerima.partial')
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_5">
								@include('admin.penyaluran_dana.partial')
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_6">
								@include('admin.distribusi_kip.partial')
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_7">
								@include('admin.monitoring.partial')
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_8">
								@include('admin.evaluasi.partial')
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- nav-tabs-custom -->

				</div>
		 		<!-- /.panel-body -->
 			</div>
 			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::script('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js', array('type' => 'text/javascript')) !!}
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->

	{!! Html::script('plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js', array('type' => 'text/javascript')) !!}

	<script>
	$(document).ready(function() {
			$('#tabel_1').DataTable({
				"footerCallback": function (row, data, start, end, display) {
						var numFormat = $.fn.dataTable.render.number( '\.', '.', 0, 'Rp ' ).display;
						var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
						var api = this.api(),
						intVal = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Rp.]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						 total2 = api
								.column(2)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(2).footer()).html(
								'(' + jumFormat(total2) + ' orang)'
						 );

						 total3 = api
								.column(3)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(3).footer()).html(
							 '('+ numFormat(total3)+ ')'
						 );

						 total4 = api
								.column(4)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(4).footer()).html(
								'( ' + jumFormat(total4) + ' orang )'
						 );

						 total5 = api
								.column(5)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(5).footer()).html(
							 ' ( '+ numFormat(total5)+ ' )'
						 );

						 total6 = api
								.column(6)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(6).footer()).html(
								'(' + jumFormat(total6) + ' orang)'
						 );

						 total7 = api
								.column(7)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(7).footer()).html(
							 '('+ numFormat(total7)+ ')'
						 );
				},
				responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering": false
			});

			$('#tabel_2').DataTable({
				"footerCallback": function (row, data, start, end, display) {
						var numFormat = $.fn.dataTable.render.number( '\.', '.', 0, 'Rp ' ).display;
						var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
						var api = this.api(),
						intVal = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Rp.]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						intVal3 = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Kali]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						 total3 = api
								.column(3)
								.data()
								.reduce(function (a, b) {
										return intVal3(a) + intVal3(b);
								}, 0);

						 $(api.column(3).footer()).html(
							 '('+ jumFormat(total3)+ ' Kali)'
						 );

						 total6 = api
								.column(6)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(6).footer()).html(
								'(' + numFormat(total6)+ ')'
						 );
				},
				// responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering": false
			});

			$('#tabel_3').DataTable({
				"footerCallback": function (row, data, start, end, display) {
						var numFormat = $.fn.dataTable.render.number( '\.', '.', 0, 'Rp ' ).display;
						var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
						var api = this.api(),
						intVal = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Rp.]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						intVal3 = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Kali]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						 total3 = api
								.column(3)
								.data()
								.reduce(function (a, b) {
										return intVal3(a) + intVal3(b);
								}, 0);

						 $(api.column(3).footer()).html(
							 '('+ jumFormat(total3)+ ' Kali)'
						 );

						 total7 = api
								.column(7)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(7).footer()).html(
								'(' + numFormat(total7)+ ')'
						 );
				},
				// responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering": false
			});

			$('#tabel_4').DataTable({
				"footerCallback": function (row, data, start, end, display) {
						var numFormat = $.fn.dataTable.render.number( '\.', '.', 0, 'Rp ' ).display;
						var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
						var api = this.api(),
						intVal = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Rp.]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						 total4 = api
								.column(4)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(4).footer()).html(
								'(' + jumFormat(total4) + ' orang)'
						 );

						 total5 = api
								.column(5)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(5).footer()).html(
							 '('+ numFormat(total5)+ ')'
						 );

						 total6 = api
								.column(6)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(6).footer()).html(
								'(' + jumFormat(total6) + ' orang)'
						 );

						 total7 = api
								.column(7)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(7).footer()).html(
							 '('+ numFormat(total7)+ ')'
						 );

						 total8 = api
								.column(8)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(8).footer()).html(
								'(' + jumFormat(total8) + ' orang)'
						 );

						 total9 = api
								.column(9)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(9).footer()).html(
							 '('+ numFormat(total9)+ ')'
						 );
				},
				// responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering": false
			});

			$('#tabel_5').DataTable({
				"footerCallback": function (row, data, start, end, display) {
						var numFormat = $.fn.dataTable.render.number( '\.', '.', 0, 'Rp ' ).display;
						var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
						var api = this.api(),
						intVal = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Rp.]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						 total4 = api
								.column(4)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(4).footer()).html(
								'(' + jumFormat(total4) + ' orang)'
						 );

						 total5 = api
								.column(5)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(5).footer()).html(
							 '('+ numFormat(total5)+ ')'
						 );

						 total6 = api
								.column(6)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(6).footer()).html(
								'(' + jumFormat(total6) + ' orang)'
						 );

						 total7 = api
								.column(7)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(7).footer()).html(
							 '('+ numFormat(total7)+ ')'
						 );

						 total8 = api
								.column(8)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(8).footer()).html(
								'(' + jumFormat(total8) + ' orang)'
						 );

						 total9 = api
								.column(9)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(9).footer()).html(
							 '('+ numFormat(total9)+ ')'
						 );
				},
				// responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering": false
			});

			$('#tabel_6').DataTable({
				"footerCallback": function (row, data, start, end, display) {
						var numFormat = $.fn.dataTable.render.number( '\.', '.', 0, 'Rp ' ).display;
						var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
						var api = this.api(),
						intVal = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Rp.]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						 total4 = api
								.column(4)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(4).footer()).html(
								'(' + jumFormat(total4) + ' orang)'
						 );

						 total5 = api
								.column(5)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(5).footer()).html(
							 '('+ numFormat(total5)+ ')'
						 );

						 total6 = api
								.column(6)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(6).footer()).html(
								'(' + jumFormat(total6) + ' orang)'
						 );

						 total7 = api
								.column(7)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(7).footer()).html(
							 '('+ numFormat(total7)+ ')'
						 );

						 total8 = api
								.column(8)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(8).footer()).html(
								'(' + jumFormat(total8) + ' orang)'
						 );

						 total9 = api
								.column(9)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(9).footer()).html(
							 '('+ numFormat(total9)+ ')'
						 );
				},
				// responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering":false
			});

			$('#tabel_7').DataTable({
				"footerCallback": function (row, data, start, end, display) {
						var numFormat = $.fn.dataTable.render.number( '\.', '.', 0, 'Rp ' ).display;
						var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
						var api = this.api(),
						intVal = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Rp.]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						intVal3 = function (i) {
									return typeof i === 'string' ?
											 i.replace(/[\Kali]/g, '')*1 :
											 typeof i === 'number' ?
											 i : 0;
						},
						 total3 = api
								.column(3)
								.data()
								.reduce(function (a, b) {
										return intVal3(a) + intVal3(b);
								}, 0);

						 $(api.column(3).footer()).html(
							 '('+ jumFormat(total3)+ ' Kali)'
						 );

						 total7 = api
								.column(7)
								.data()
								.reduce(function (a, b) {
										return intVal(a) + intVal(b);
								}, 0);

						 $(api.column(7).footer()).html(
								'(' + numFormat(total7)+ ')'
						 );
				},
				// responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering": false
			});

			$('#tabel_8').DataTable({
				// responsive: true,
				scrollY:        '50vh',
				scrollCollapse: true,
				paging:  false,
				"ordering": false
			});
	});

	// $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	// 		 $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
	//  } );

	 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust()
        .fixedColumns().relayout();
		});
	</script>

	@yield('add_script')

	<!-- Delete Data JavaScript - Jeffry Wayy -->
	{!! Html::script('js/laravel.js', array('type' => 'text/javascript')) !!}

@stop
