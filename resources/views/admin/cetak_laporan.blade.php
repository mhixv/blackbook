@extends('admin_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">

	<link href="{{ asset('css/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
	<style>
	.input-group[class*="col-"] {
		float: none;
		padding-right: 15px;
		padding-left: 15px;
	}
	.chosen-container-single .chosen-single {
		line-height: 30px;
		font-size: 14px;
	}
	.heighttext{ padding: 20px 10px; line-height: 28px; }
	</style>

@stop

@section('content-header')
	      <h1>Laporan Data Pelanggaran Siswa</h1>
@endsection

@section('content')
<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h1 class="box-title">Cetak Laporan Pelanggaran Siswa</h1>
				</div>
				<div class="box-body">
					<div class="col-md-6 col-md-offset-3" >
						<br>
							{!! Form::model(null,['method' => 'POST','url' => 'admin/cetak_laporan']) !!}
								<div class="form-group col-md-12">
									{!! Form::select('siswa_id', array_merge(['0' => 'Pilih Nama Siswa'], $siswa), 0,['id'=>'siswa_id', 'class' => 'form-control heighttext']) !!}
								</div>
								<div class="form-group col-md-5">
									<div class="input-group">
					                  <div class="input-group-addon">
					                    <i class="fa fa-calendar"></i>
					                  </div>
										<input id="tanggal1" type="text" class="form-control" class="tanggal1" name="tanggal1" value="{{ old('tanggal1') }}" placeholder="Tanggal Mulai">
									</div>
								</div>
								<div class="form-group col-md-2"><p>S / D</p>
								</div>
								<div class="form-group col-md-5">
									<div class="input-group">
					                  <div class="input-group-addon">
					                    <i class="fa fa-calendar"></i>
					                  </div>
										<input id="tanggal2" type="text" class="form-control" class="tanggal2" name="tanggal2" value="{{ old('tanggal2') }}" placeholder="Tanggal Akhir">
									</div>
					            </div>
								<div class="form-group col-md-12">
									{!! Form::submit('Print Laporan (PDF)', ['class' => 'btn btn-primary form-control']) !!}
								</div>
	<!-- 								<div class="col-md-2 col-md-offset-2">
								<br>
									<a class="btn btn-success" href="{!! URL::to('admin/DonwloadDepartement') !!}"><i class="glyphicon glyphicon-download-alt"></i> Download in excel</a>
								</div> -->
								{!! Form::close() !!}
							<br>
					</div>
			 	</div>
					<!-- /.panel-body -->
				</div>
 			<!-- /.panel -->
 	</div>
 	<!-- /.col-lg-12 -->
</div>
 <!-- /.row -->

@stop


@section('script')
	<!-- bootstrap datepicker -->
	<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>

	<script>
		$(function() {
		  $('#tanggal1').datepicker({
		  		format: 'yyyy-mm-dd',
				// todayHighlight: TRUE,
	    		autoclose: true
		     });

		  $('#tanggal2').datepicker({
		  		format: 'yyyy-mm-dd',
				// todayHighlight: TRUE,
	    		autoclose: true
		     });
		});
	</script>

	<script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script>
	<script>
		$(document).ready(function () {
			$('#siswa_id').chosen({width: "100%"});
		});
	</script>

@stop
