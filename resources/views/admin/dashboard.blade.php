@extends('admin_index')



@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}

	<style>

	</style>

@stop

@section('content-header')
	      <h1>
	        Dasboard Data Pelanggaran Tahun Ajaran {{ $tahun_ajaran_aktif}}
	      </h1>
@endsection
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				Total Pelanggaran Siswa
				<h3> {{ $jml_pel_siswa }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-person"></i>
			</div>
			<a href="{{ URL::to('#') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				Siswa Pelanggar
				<h3> {{ $jml_siswa_pel }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="{{ URL::to('#') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-blue">
			<div class="inner">
				Total Hukuman
				<h3> {{ $jml_hukuman }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-card"></i>
			</div>
			<a href="{{ URL::to('admin/hukuman') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-md-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				Jumlah Aturan
				<h3> {{ $jml_aturan }} </h3>
			</div>
			<div class="icon">
				<i class="ion ion-pie-graph"></i>
			</div>
			<a href="{{ URL::to('admin/aturan') }}" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h1 class="box-title">Top 10 Siswa Pelanggar</h1>
          </div>
            <div class="box-body">
              <div class="dataTable_wrapper">
                  <table class="table table-responsive table-striped table-bordered table-hover" id="tabel3">

                    <thead>
                      <tr>
                        <th style="width:1%;">No</th>
                        <th style="width:20%;">Nama Siswa</th>
                        <th style="width:5%;text-align: center">Jumlah</th>
                        <th style="width:5%;">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($top_ten_pelanggar as $data)
                      <tr>
                        <td>
                          {{ $row++ }}
                        </td>
                        <td>
                          {{ $data->nama_siswa }}
                        </td>
                        <td style="width:7%;text-align: center">
                          {{ $data->jumlah }} Kali
                        </td>
                        <td>
                         
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                 </table>
           </div>
           </div>
          <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
  </div>
  <!-- /.col-lg-6 -->
  <div class="col-lg-6 col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h1 class="box-title">Top 10 Aturan Paling Sering Dilanggar</h1>
          </div>
            <div class="box-body">
              <div class="dataTable_wrapper">
                  <table class="table table-responsive table-striped table-bordered table-hover" id="tabel3">

                    <thead>
                      <tr>
                        <th style="width:1%;">No</th>
                        <th style="width:35%;">Nama Aturan</th>
                        <th style="width:5%;text-align: center">Jumlah</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($top_ten_aturan as $data)
                      <tr>
                        <td>
                          {{ $row1++ }}
                        </td>
                        <td>
                          {{ $data->nama_aturan }}
                        </td>
                        <td style="width:7%;text-align: center">
                          {{ $data->jumlah }} Kali
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                 </table>
           </div>
           </div>
          <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
  </div>
  <!-- /.col-lg-6 -->
</div>
 <!-- /.row -->

<div class="row row-eq-height">
  <div class="col-md-6">
		<div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Grafik Pelanggaran Per Bulan</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="barChart"></canvas>
        </div>
      </div>
    </div>
	</div>
	<div class="col-md-6">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Grafik Pelanggaran Per Tahun</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <canvas id="barChart2"></canvas>
      </div>
    </div>
  </div>
</div>
@endsection








@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/chartjs/Chart.min.js', array('type' => 'text/javascript')) !!}

	<script>
	var randomColorFactor = function() {
      return Math.round(Math.random() * 255);
  };
  var randomColor = function() {
      return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',.7)';
  };

  $(function () {
		Chart.defaults.global.scaleLabel = "<%=parseInt(value).toLocaleString()%>";
		Chart.defaults.global.tooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=parseInt(value).toLocaleString()%>";
		//templateInterpolators changed to [[ and ]] or these could be passed as options when generating the graph

		Chart.defaults.global.scaleLabel = "<%=parseInt(value).toLocaleString()%>";
		Chart.defaults.global.tooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=parseInt(value).toLocaleString()%>";
		Chart.defaults.global.multiTooltipTemplate = "<%if (label){%><%=label%>: <%}%><%=parseInt(value).toLocaleString()%>";

    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    //var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    //var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
			// labels: ["January", "February", "March", "April", "May", "June", "July"],
      labels: {!! json_encode($bulan) !!},
      datasets: [
        {
          label: "Jumlah Pelanggaran",
					backgroundColor: randomColor(),
          data: {!! json_encode($jumlah_pelanggaran) !!}
        }
      ],

    };

		var areaChartData2 = {
      labels: {!! json_encode($tahun) !!},
      datasets: [
        {
          label: "Jumlah Pelanggaran",
          backgroundColor: randomColor(),
          data: {!! json_encode($jumlah_pelanggaran_tahun) !!}
        }
      ]
    };

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true,
			//tooltipTemplate "<%if (label){%><%=label%>: <%}%><%=parseInt(value).toLocaleString()%>",
			tooltipTemplate: "<%= value %> Pelanggaran",

    };
		barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);

		var barChartCanvas2 = $("#barChart2").get(0).getContext("2d");
    var barChart2 = new Chart(barChartCanvas2);
    var barChartData2 = areaChartData2;
    var barChartOptions2 = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true,
			//tooltipTemplate "<%if (label){%><%=label%>: <%}%><%=parseInt(value).toLocaleString()%>",
			tooltipTemplate: "<%= value %> Pelanggaran",

    };
		barChartOptions2.datasetFill = false;
    barChart2.Bar(barChartData2, barChartOptions2);
	  });
	</script>

	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
			$('#tabel-provinsi').DataTable({
							responsive: true,
							bSort : false,
							bPaginate: false,
			        bFilter: false,
			        bInfo: false,
              language: {
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data.",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
              },
              "columnDefs": [
                { "orderable": true, "targets": 0 },
								{ width: 20, targets: 0 }
              ]
			});
	});
	</script>
	<script>
	$(document).ready(function() {
			$('#tabel-provinsi1').DataTable({
							responsive: true,
							bSort : false,
							bPaginate: false,
			        bFilter: false,
			        bInfo: false,
              language: {
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data.",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
              },
              "columnDefs": [
                { "orderable": true, "targets": 0 },
								{ width: 20, targets: 0 }
              ]
			});
	});
	</script>

@stop
