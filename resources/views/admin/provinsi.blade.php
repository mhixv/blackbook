@extends('admin_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- {!! Html::style('plugins/datatables/jquery.dataTables.min.css') !!} -->
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}
	{!! Html::style('plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.min.css') !!}

	<style>
	.nav-tabs-custom > .nav-tabs > li.active {
    border-top-color: #3c8dbc;
	}
	.nav-tabs-custom > .nav-tabs > li.active > a {
    border-top-color: transparent;
    border-left-color: #00a65a;
    border-right-color: #00a65a;
	}
	.nav-tabs-custom > .nav-tabs > li.active > a, .nav-tabs-custom > .nav-tabs > li.active:hover > a {
    background-color: #00a65a;
    color: #444;
	}

	</style>
@stop


@section('content-header')
	      <h1>
	        Status Kegiatan Pelaksanaan PIP Provinsi {{ $prov->nama_provinsi }} Tahun Anggaran {{ $tahun_anggaran_aktif }}
	      </h1>
@endsection

@section('content')
<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="box box-success">
				<div class="box-body">
					<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab"><b>Status Data</b></a></li>
              <li><a href="#tab_2" data-toggle="tab"><b>Informasi Umum</b></a></li>
              <li><a href="#tab_3" data-toggle="tab"><b>Sosialisasi</b></a></li>
              <li><a href="#tab_4" data-toggle="tab"><b>Pendataan</b></a></li>
							<li><a href="#tab_5" data-toggle="tab"><b>Penetapan</b></a></li>
              <li><a href="#tab_6" data-toggle="tab"><b>Penyaluran</b></a></li>
							<li><a href="#tab_7" data-toggle="tab"><b>Distribusi</b></a></li>
              <li><a href="#tab_8" data-toggle="tab"><b>Monitoring</b></a></li>
							<li><a href="#tab_9" data-toggle="tab"><b>Evaluasi</b></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
								<div class="dataTable_wrapper">
										<table class="table table-bordered" id="tabel-provinsi">
											<thead>
												<tr>
													<th style="width: 150px;text-align:center;">Informasi Umum</th>
													<th style="width: 150px;text-align:center;">Sosialisasi dan Koordinasi</th>
													<th style="width: 150px;text-align:center;">Pendataan dan Verifikasi</th>
													<th style="width: 150px;text-align:center;">Penetapan Penerima</th>
													<th style="width: 150px;text-align:center;">Penyaluran Dana Manfaat</th>
													<th style="width: 150px;text-align:center;">Distribusi KIP</th>
													<th style="width: 150px;text-align:center;">Monitoring</th>
													<th style="width: 150px;text-align:center;">Evaluasi</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[0] == 2)
															<a href="{!! URL::to('admin/informasi_umum/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[0] == 3)
															<a href="{!! URL::to('admin/informasi_umum/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[0] == 4)
															<a href="{!! URL::to('admin/informasi_umum/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
												  </td>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[1] == 2)
															<a href="{!! URL::to('admin/sosialisasi_koordinasi/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[1] == 3)
															<a href="{!! URL::to('admin/sosialisasi_koordinasi/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[1] == 4)
															<a href="{!! URL::to('admin/sosialisasi_koordinasi/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
													</td>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[2] == 2)
															<a href="{!! URL::to('admin/pendataan_verifikasi/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[2] == 3)
															<a href="{!! URL::to('admin/pendataan_verifikasi/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[2] == 4)
															<a href="{!! URL::to('admin/pendataan_verifikasi/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
													</td>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[3] == 2)
															<a href="{!! URL::to('admin/penetapan_penerima/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[3] == 3)
															<a href="{!! URL::to('admin/penetapan_penerima/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[3] == 4)
															<a href="{!! URL::to('admin/penetapan_penerima/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
													</td>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[4] == 2)
															<a href="{!! URL::to('admin/penyaluran_dana/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[4] == 3)
															<a href="{!! URL::to('admin/penyaluran_dana/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[4] == 4)
															<a href="{!! URL::to('admin/penyaluran_dana/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
													</td>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[5] == 2)
															<a href="{!! URL::to('admin/distribusi_kip/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[5] == 3)
															<a href="{!! URL::to('admin/distribusi_kip/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[5] == 4)
															<a href="{!! URL::to('admin/distribusi_kip/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
													</td>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[6] == 2)
															<a href="{!! URL::to('admin/monitoring/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[6] == 3)
															<a href="{!! URL::to('admin/monitoring/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[6] == 4)
															<a href="{!! URL::to('admin/monitoring/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
													</td>
													<td style="text-align:center;padding-top:10px;">
														@if($stats[7] == 2)
															<a href="{!! URL::to('admin/evaluasi/provinsi/'.$prov->id) !!}"><i class="fa fa-clock-o fa-5x text-yellow"></i><br />Menunggu Verifikasi</a>
														@elseif($stats[7] == 3)
															<a href="{!! URL::to('admin/evaluasi/provinsi/'.$prov->id) !!}"><i class="fa fa-close fa-5x text-red"></i><br />Sedang Merevisi</a>
														@elseif($stats[7] == 4)
															<a href="{!! URL::to('admin/evaluasi/provinsi/'.$prov->id) !!}"><i class="fa fa-check fa-5x text-green"></i><br />Telah di Verifikasi</a>
														@else
															<a href="#"><i class="fa fa-close fa-5x text-red"></i><br />Belum Mengirim Data</a>
														@endif
												  </td>
												</tr>
											</tbody>
								   </table>
						 		</div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                @include('admin.informasi_umum.partial')
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                @include('admin.sosialisasi_koordinasi.partial')
              </div>
              <!-- /.tab-pane -->
							<div class="tab-pane" id="tab_4">
                @include('admin.pendataan_verifikasi.partial')
              </div>
              <!-- /.tab-pane -->
							<div class="tab-pane" id="tab_5">
                @include('admin.penetapan_penerima.partial')
              </div>
              <!-- /.tab-pane -->
							<div class="tab-pane" id="tab_6">
                @include('admin.penyaluran_dana.partial')
              </div>
              <!-- /.tab-pane -->
							<div class="tab-pane" id="tab_7">
                @include('admin.distribusi_kip.partial')
              </div>
              <!-- /.tab-pane -->
							<div class="tab-pane" id="tab_8">
                @include('admin.monitoring.partial')
              </div>
              <!-- /.tab-pane -->
							<div class="tab-pane" id="tab_9">
                @include('admin.evaluasi.partial')
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->



				</div>
		 		<!-- /.panel-body -->
 			</div>
 			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection

@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::script('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js', array('type' => 'text/javascript')) !!}
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->

	{!! Html::script('plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js', array('type' => 'text/javascript')) !!}

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
			$('#tabel-provinsi').DataTable({
					responsive: true,
					bSort : false,
					sSearch : false;
			});
	});
	</script>

	<!-- Delete Data JavaScript - Jeffry Wayy -->
	{!! Html::script('js/laravel.js', array('type' => 'text/javascript')) !!}

@stop
