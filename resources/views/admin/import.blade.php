@extends('admin_index')

@section('content-header')
	<h1>Import Data Siswa</h1>
@endsection

@section('content')

<div class="row">
  <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12">
    <div class="box box-primary">
      {!! Form::open(['url' => '/admin/siswa/import','method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
      <!-- <form class="form-horizontal" files="true" role="form" method="POST" action="{{ url('/admin/importpesantren') }}"> -->
        <div class="box-body">
          <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('template', 'Template', ['class'=>'col-md-2 col-sm-2 control-label']) !!}
            <div class="col-md-4">
              <a class="btn btn-success" href="{!! URL::to('/admin/templateimportsiswa') !!}"><i class=\
              "fa fa-cloud-download"></i> Download Template</a>
            </div>
          </div>
        </div>
        <hr />
        <div class="panel-body">
            <p><b>Upload Data Siswa</b></p>
            <br />
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('excel') ? ' has-error' : '' }}">
              {!! Form::label('excel', 'Pilih file', ['class'=>'col-md-2 col-sm-2 control-label']) !!}
              <div class="col-md-3 col-sm-3">
                {!! Form::file('excel') !!}
                {!! $errors->first('excel', '<p class="help-block">:message</p>') !!}
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2">
                {!! Form::submit('Upload Siswa', ['class'=>'btn btn-primary']) !!}
              </div>
            </div>
						<br />
						<div class="left7">
							<span style="color :red">*  Data Siswa yang tidak valid akan didownload, perbaiki dan import ulang.</span>
						</div>
            <!-- <span style="color :red">* Pastikan data didalam file excel tidak ada yang kosong. Field wajib : NSPP dan kabupaten_id_kabupaten</span> -->
          </div>
        <!-- </form> -->
        {!! Form::close() !!}
      </div>
    </div>
 </div>
@endsection
