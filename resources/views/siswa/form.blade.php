<div class="form-group">
	{!! Form::label('nama_siswa', 'Nama Siswa :') !!}
	{!! Form::text('nama_siswa',null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('nis', 'NIS Siswa :') !!}
	{!! Form::text('nis', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('nisn', 'NISN Siswa :') !!}
	{!! Form::text('nisn', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('kelas_id', 'Kelas :') !!}
	{!! Form::select('kelas_id',$kelas,1,['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('guru_asuh_id', 'Guru Asuh :') !!}
	{!! Form::select('guru_asuh_id',$guru,1,['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('asrama_kamar', 'Nama Asrama dan Kamar :') !!}
	{!! Form::text('asrama_kamar', null, ['class' => 'form-control']) !!}
	<p class="help-block">Contoh : Putra 1 - 203, Putra 2 - 102, Putri 1 - 105, Putri 2 - 206</p>
</div>

<div class="form-group col-md-4 col-md-offset-4">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
