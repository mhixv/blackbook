@extends('login_index')

@section('content')
<div class="container" style="margin-top:175px">
  @if (Session::has('token_error'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      {{ Session::get('token_error') }}
    </div>
  @endif
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">LOGIN</h3>
                </div>
                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <strong>Whoops!</strong> Ada permasalahan dengan masukan yang anda ketik.<br><br>
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                <!-- /.box-header -->
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="box-body">
                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                              <div class="col-md-6">
                                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="password" class="col-md-4 control-label">Password</label>

                              <div class="col-md-6">
                                  <input id="password" type="password" class="form-control" name="password">

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="col-md-6 col-md-offset-4">
                                  <div class="checkbox">
                                      <label>
                                          <input type="checkbox" name="remember"> Ingat Saya
                                      </label>
                                  </div>
                              </div>
                          </div>
                        </div>

                        <div class="box-footer">
                          <button type="reset" class="btn btn-default">Reset</button>
                          <button type="submit" class="btn btn-info pull-right">Masuk</button>
                          <a class="btn btn-default" href="{{ url('/') }}">Beranda</a>
                          <a class="btn btn-link" href="{{ url('/password/reset') }}">Lupa Kata Sandi?</a>

                        </div>
                        <!-- /.box-footer -->
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
