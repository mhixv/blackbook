@extends('login_index')

@section('content')
<div class="container" style="margin-top:175px">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
              <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">LOGIN</h3>
                    </div>
                    @if (count($errors) > 0)
        							<div class="alert alert-danger">
        								<strong>Whoops!</strong> Ada permasalahan dengan masukan yang anda ketik.<br><br>
        								<ul>
        									@foreach ($errors->all() as $error)
        										<li>{{ $error }}</li>
        									@endforeach
        								</ul>
        							</div>
        						@endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal"  role="form" method="POST" action="{{ url('/login') }}">
                      {{ csrf_field() }}

                      <div class="box-body">
                        <div class="form-group">
                          <label for="email" class="col-sm-2 control-label">Email</label>

                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="password" class="col-sm-2 control-label">Password</label>

                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" placeholder="Password">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Ingat Saya
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Sign in</button>

                        <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>

                      </div>
                      <!-- /.box-footer -->
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
