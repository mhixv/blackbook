<div class="form-group">
	{!! Form::label('nama_kelas', 'Nama Kelas :') !!}
	{!! Form::text('nama_kelas',null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('wali_kelas_id', 'Wali Kelas :') !!}
	{!! Form::select('wali_kelas_id',$guru,1,['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-4 col-md-offset-4">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
