<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mencari Data Pelanggaran Siswa | MAN Insan Cendekia Siak</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-green.min.css') }}">

  <!-- Custom PIP CSS -->
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

  <link href="{{ asset('img/favicon.ico') }}" rel="shortcut icon">

	<!-- DataTables CSS -->
	<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
	<!-- DataTables Responsive CSS -->
	<link rel="stylesheet" href="{{ asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">

  <style>
      body {
          /*font-family: 'Lato';*/
          background-color: #444;
          background: url("{{asset('/img/background.jpg')}}") no-repeat center center fixed;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
      }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="skin-green">

  <!-- <div class="wrapper"> -->

    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="{!! URL::to('/') !!}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
	      <span class="logo-mini"><b>IC</b>S</span>
	      <!-- logo for regular state and mobile devices -->
	      <span class="logo-lg"><b>ICSIAK</b> - Black Books</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <a href="#" class="navbar-custom-menu-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
					<ul class="nav navbar-nav">
            <li><a href="{{ URL::to('/login') }}"><i class="fa fa-user"></i> LOGIN</a></li>
            <li><a href="{{ URL::to('/lihat') }}"><i class="fa fa-file-text-o"></i> CEK PELANGGARAN</a></li>

          </ul>
        </div>
      </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <!-- <div class="content-wrapper"> -->

      <!-- Main content -->
      <section class="content">
					<div class="row">
							<div class="col-lg-12">
									<div class="box box-primary">
										<div class="box-header with-border">
											<h3 class="box-title">Hasil Pencarian Data Pelanggaran Siswa</h3>
										</div>
											<div class="box-body">
												<div class="dataTable_wrapper">
														<table class="table table-striped table-bordered table-hover" id="tabel1">
															<thead>
																<tr>
																	<th>No</th>
																	<th>Nama Lengkap</th>
																	<th>NISN</th>
																	<th>Kelas</th>
																	<th>Aksi</th>
																</tr>
															</thead>
															<tbody>
																@foreach ($hasil as $data)
																<tr>
																	<td class="center">
																		{{ $row++ }}
																	</td>
																	<td class="center">
																		{{ $data->nama_siswa }}
																	</td>
																	<td class="center">
																		{{ $data->nisn }}
																	</td>
																	<td class="center">
																		{{ $data->kelas->nama_kelas }}
																	</td>
																	<td class="center">
																			<a class="btn btn-xs btn-success" href="{{ URL::to('/lihat/'.$data->id) }}"><i class="fa fa-eye fa-fw"></i> Lihat</a>
																	</td>
																</tr>
																@endforeach
															</tbody>
													 </table>
										 </div>
										 </div>
					 					<!-- /.panel-body -->
					 			</div>
					 			<!-- /.panel -->
					 	</div>
					 	<!-- /.col-lg-12 -->
					</div>
					 <!-- /.row -->
			 </section>


	 <!-- jQuery 2.2.3 -->
 	<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
 	<!-- Bootstrap 3.3.6 -->
 	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('dist/js/app.min.js') }}"></script>

	<!-- DataTables JavaScript -->
	<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
    $('#tabel1').DataTable({
						responsive: true,
						scrollX: true,
						bSort : false,
						language: {
							"sProcessing":   "Sedang memproses...",
							"sLengthMenu":   "Tampilkan _MENU_ entri",
							"sZeroRecords":  "Tidak ditemukan Data.",
							"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
							"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
							"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
							"sInfoPostFix":  "",
							"sSearch":       "Cari:",
							"sUrl":          "",
							"oPaginate": {
									"sFirst":    "Pertama",
									"sPrevious": "Sebelumnya",
									"sNext":     "Selanjutnya",
									"sLast":     "Terakhir"
							}
						},
						"columnDefs": [
							{ "orderable": false, "targets": 0 },
							{ width: 20, targets: 0 }
						]
		});
	} );
	</script>
</body>
</html>
