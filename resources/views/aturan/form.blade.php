<div class="form-group">
	{!! Form::label('nama_aturan', 'Nama Aturan :') !!}
	{!! Form::textarea('nama_aturan',null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('poin', 'Maksimal Poin :') !!}
	{!! Form::text('poin',null,['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-4 col-md-offset-4">
	{!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
