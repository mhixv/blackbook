@extends('operator_index')


@section('css')
	<!-- DataTables CSS -->
	{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
	<!-- DataTables Responsive CSS -->
	{!! Html::style('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}

	<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">

@stop

@section('content-header')
	      <h1>Daftar Pelanggaran dan Hukuman Siswa</h1>
@endsection

@section('content')
					<div class="row">
							<div class="col-lg-12">
									<div class="box box-primary">
										<div class="box-header with-border">
											<h1 class="box-title">Lihat Data Pelanggaran Siswa</h1>
										</div>
											<div class="box-body">


												<div class="dataTable_wrapper">
														<table class="table table-responsive table-bordered table-hover" id="tabel2">

															<thead>
																<tr>
																	<th style="width:1%;">No</th>
                                  									<th style="width:12%;">Nama</th>
																	<th style="width:10%;">Tanggal</th>
																	<th>Pelanggaran</th>
																	<th>Keterangan</th>
																	<th style="width:7%;">Poin</th>
																</tr>
															</thead>
															<tbody>
																@foreach ($pelanggaran as $data)
																@if($data->status == 1)
																<tr style="color: #ff0000;">
																@else
																<tr>
																@endif
																	<td>
																		{{ $row++ }}
																	</td>
                                  									<td>
																		{{ $data->siswa->nama_siswa }}
																	</td>
																	<td>
																		{{ $data->tanggal }}
																	</td>
																	<td>
																		{{ $data->aturan->nama_aturan }}
																	</td>
																	<td>
																		{{ $data->keterangan }}
																	</td>
																	<td>
																		{{ $data->aturan->poin }} Poin
																	</td>
																</tr>
																@endforeach
															</tbody>
															<tfoot>
												        <tr>
												          <th></th>
												          <th></th>
												          <th></th>
                                  <th></th>
												          <th>Jumlah Poin</th>
												          <th>-</th>
												        </tr>
												      </tfoot>
													 </table>
										 </div>
										 </div>
					 					<!-- /.panel-body -->
					 			</div>
					 			<!-- /.panel -->
					 	</div>
					 	<!-- /.col-lg-12 -->
					</div>
					 <!-- /.row -->
					 <div class="row">
							<div class="col-lg-12">
									<div class="box box-primary">
										<div class="box-header with-border">
											<h1 class="box-title">Lihat Data Hukuman Siswa</h1>
										</div>
											<div class="box-body">


												<div class="dataTable_wrapper">
														<table class="table table-responsive table-striped table-bordered table-hover" id="tabel3">

															<thead>
																<tr>
																	<th style="width:1%;">No</th>
                                  									<th style="width:15%;">Nama</th>
																	<th style="width:10%;">Tanggal</th>
																	<th style="width:12%;">Kategori Sangsi</th>
																	<th>Keterangan</th>
																	<th style="width:7%;">Kompensasi</th>
																</tr>
															</thead>
															<tbody>
																@foreach ($hukuman as $data)
																<tr>
																	<td>
																		{{ $row1++ }}
																	</td>
                                  									<td>
																		{{ $data->siswa->nama_siswa }}
																	</td>
																	<td>
																		{{ $data->tanggal }}
																	</td>
																	<td>
																		{{ $data->sangsi->nama_sangsi }}
																	</td>
																	<td>
																		{{ $data->keterangan }}
																	</td>
																	<td>
																		{{ $data->sangsi->kompensasi }}
																	</td>
																</tr>
																@endforeach
															</tbody>
															<tfoot>
												        <tr>
												          <th></th>
												          <th></th>
												          <th></th>
                                  <th></th>
												          <th>Jumlah Poin</th>
												          <th>-</th>
												        </tr>
												      </tfoot>
													 </table>
										 </div>
										 </div>
					 					<!-- /.panel-body -->
					 			</div>
					 			<!-- /.panel -->
					 	</div>
					 	<!-- /.col-lg-12 -->
					</div>
					 <!-- /.row -->
@stop


@section('script')
	<!-- DataTables JavaScript -->
	{!! Html::script('plugins/datatables/jquery.dataTables.min.js', array('type' => 'text/javascript')) !!}
	{!! Html::script('plugins/datatables/dataTables.bootstrap.min.js', array('type' => 'text/javascript')) !!}

	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script> -->
	<script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script> -->
	<script src="{{ asset('js/buttons.flash.min.js') }}"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> -->
	<script src="{{ asset('js/jszip.min.js') }}"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script> -->
	<script src="{{ asset('js/pdfmake.min.js') }}"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> -->
	<script src="{{ asset('js/vfs_fonts.js') }}"></script>
	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script> -->
	<script src="{{ asset('js/buttons.html5.min.js') }}"></script>
	<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script> -->
	<script src="{{ asset('js/buttons.print.min.js') }}"></script>

	<!-- <script src="https://cdn.datatables.net/rowreorder/1.2.3/js/dataTables.rowReorder.min.js"></script> -->
	<!-- <script src="https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script> -->
	<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function() {
    $('#tabel2').DataTable({
						"footerCallback": function (row, data, start, end, display) {
								var jumFormat = $.fn.dataTable.render.number( '\.', '.', 0, '' ).display;
								var api = this.api(),
								intVal = function (i) {
											return typeof i === 'string' ?
													 i.replace(/[\Poin]/g, '')*1 :
													 typeof i === 'number' ?
													 i : 0;
								},
								 total5 = api
										.column(5)
										.data()
										.reduce(function (a, b) {
												return intVal(a) + intVal(b);
										}, 0);

								 $(api.column(5).footer()).html(
										'' + jumFormat(total5) + ' Poin'
								 );
						},
						// responsive: true,
						iDisplayLength: 50,
						scrollX: true,
						bSort : false,
						language: {
							"sProcessing":   "Sedang memproses...",
							"sLengthMenu":   "Tampilkan _MENU_ entri",
							"sZeroRecords":  "Tidak ditemukan Data.",
							"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
							"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
							"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
							"sInfoPostFix":  "",
							"sSearch":       "Cari:",
							"sUrl":          "",
							"oPaginate": {
									"sFirst":    "Pertama",
									"sPrevious": "Sebelumnya",
									"sNext":     "Selanjutnya",
									"sLast":     "Terakhir"
							}
						},
						"columnDefs": [
							{ "orderable": false, "targets": 0 },
							{ width: 20, targets: 0 }
						],
						dom: 'Bfrtip',
		        buttons: [
		            'excel', 'pdf', 'print',{
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'A4'
            }
		        ]
		});

    $('#tabel3').DataTable({						
						responsive: true,
						bSort : false,
						iDisplayLength: 50,
						scrollX: true,
						language: {
							"sProcessing":   "Sedang memproses...",
							"sLengthMenu":   "Tampilkan _MENU_ entri",
							"sZeroRecords":  "Tidak ditemukan Data.",
							"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
							"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
							"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
							"sInfoPostFix":  "",
							"sSearch":       "Cari:",
							"sUrl":          "",
							"oPaginate": {
									"sFirst":    "Pertama",
									"sPrevious": "Sebelumnya",
									"sNext":     "Selanjutnya",
									"sLast":     "Terakhir"
							}
						},
						"columnDefs": [
							{ "orderable": false, "targets": 0 },
							{ width: 20, targets: 0 }
						],
						dom: 'Bfrtip',
		        buttons: [
		            'excel', 'pdf', 'print',{
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'A4'
            }
		        ]
		});
	} );
	</script>

@stop