<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'HomeController@index');
Route::get('/lihat', 'HomeController@lihat');
Route::post('/lihat', 'HomeController@lihat_data');
Route::get('/lihat/{id_data}', 'HomeController@lihat_detil');

Route::group(array('prefix' => 'operator','middleware' => ['operator']), function(){
  Route::get('/', 'OperatorController@index');
  Route::get('/pelanggaran', 'OperatorController@pelanggaran');
  Route::get('/pelanggaran/create', 'OperatorController@pelanggaran_create');
  Route::post('/pelanggaran', 'OperatorController@pelanggaran_simpan');
  Route::get('/pelanggaran/{id}/edit', 'OperatorController@pelanggaran_edit');
  Route::PATCH('/pelanggaran/{id}', 'OperatorController@pelanggaran_update');
  Route::delete('pelanggaran/delete/{id}','OperatorController@pelanggaran_delete');

  Route::get('/hukuman', 'OperatorController@hukuman');
  Route::get('/hukuman/create', 'OperatorController@hukuman_create');
  Route::post('/hukuman', 'OperatorController@hukuman_simpan');
  Route::get('/hukuman/{id}/edit', 'OperatorController@hukuman_edit');
  Route::PATCH('/hukuman/{id}', 'OperatorController@hukuman_update');
  Route::delete('hukuman/delete/{id}','OperatorController@hukuman_delete');
  // hukuman dari dashboard operator
  Route::get('/hukuman/{id}', 'OperatorController@hukuman_hukum');
  Route::post('/hukuman/hukum', 'OperatorController@hukuman_hukum2');
  // lihat pelanggaran dan hukuman siswa $id
  Route::get('/lihat/{id_data}', 'OperatorController@lihat_detil');

  // laporan pelanggaran dan hukuman
  Route::get('/laporan_pelanggaran', 'OperatorController@laporan_pelanggaran');
  Route::post('/laporan_pelanggaran', 'OperatorController@laporan_pelanggaran2');
  Route::get('/laporan_hukuman', 'OperatorController@laporan_hukuman');
  Route::post('/laporan_hukuman', 'OperatorController@laporan_hukuman2');
});

Route::group(array('prefix' => 'admin','middleware' => ['admin']), function(){

  Route::get('/', 'AdminController@index');

  Route::get('/pelanggaran', 'AdminController@pelanggaran');
  Route::get('/hukuman', 'AdminController@hukuman');
  Route::get('/jenis_pelanggaran', 'AdminController@jenis_pelanggaran');

  Route::get('/cetak_laporan','AdminController@cetak_laporan');
  Route::post('/cetak_laporan','AdminController@cetak_laporan2');

  Route::get('/tahun_aktif', 'AdminController@tahun_aktif');
  Route::patch('/tahun_aktif/{id}', 'AdminController@tahun_aktif_update');

  Route::resource('/guru','GuruController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('guru/delete/{guru}','GuruController@destroy');

  Route::resource('/siswa','SiswaController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('siswa/delete/{siswa}','SiswaController@destroy');
  Route::get('/siswa/import','SiswaController@import');
  Route::post('/siswa/import','SiswaController@importSiswa');
  Route::get('/templateimportsiswa', 'SiswaController@download_template');

  Route::resource('/kamar','KamarController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('kamar/delete/{kamar}','KamarController@destroy');

  Route::resource('/kelas','KelasController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('kelas/delete/{kelas}','KelasController@destroy');

  Route::resource('/aturan','AturanController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('aturan/delete/{kelas}','AturanController@destroy');

  Route::resource('/sangsi','SangsiController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('sangsi/delete/{sangsi}','SangsiController@destroy');

  Route::resource('/tahunajaran','TahunAjaranController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('tahunajaran/delete/{tahunajaran}','TahunAjaranController@destroy');

  Route::resource('/pengguna','PenggunaController',['except' => ['show']]); // seharusnya method 'show' tidak ada
  Route::delete('pengguna/delete/{user}','PenggunaController@destroy');

  // reset password pengguna
  Route::get('changepassworduser/{id}', 'Auth\ChangePasswordsController@edit2');
  Route::post('changepassworduser/{id}', 'Auth\ChangePasswordsController@update2');

  // lihat pelanggaran dan hukuman siswa $id
  Route::get('/lihat/{id_data}', 'AdminController@lihat_detil');

  // al: ajax put akses id
	// Route::get('/listspesifikakses/{id}','PenggunaController@getAccess');

});
